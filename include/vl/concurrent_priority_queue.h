#pragma once

#include <queue>
#include <mutex>
#include <condition_variable>
#include <chrono>

namespace vl {

/* A simple blocking producer-consumer priority queue
 */
template<typename T, typename Cmp = std::less<T>>
class ConcurrentPriorityQueue {
	std::priority_queue<T, std::vector<T>, Cmp> queue;
	std::mutex mutex;
	std::condition_variable condvar;

public:
	ConcurrentPriorityQueue() {}
	ConcurrentPriorityQueue(const ConcurrentPriorityQueue&) = delete;
	ConcurrentPriorityQueue& operator=(const ConcurrentPriorityQueue&) = delete;
	void push(const T &t);
	void push_all(const std::vector<T> &elems);
	// Block to pop an item from the queue
	T pop();
	/* Try to pop an item from the queue. Returns false if
	 * the queue was empty (nothing to pop).
	 */
	bool try_pop(T &t);
	/* Try to pop an item from the queue. If the queue is empty
	 * will wait for some timeout, if the timeout expires and
	 * there's no item in the queue, returns false.
	 */
	bool try_pop_for(T &t, const std::chrono::nanoseconds &time_out);
};

template<typename T, typename Cmp>
void ConcurrentPriorityQueue<T, Cmp>::push(const T &t) {
	{
		std::lock_guard<std::mutex> lock(mutex);
		queue.push(t);
	}
	condvar.notify_all();
}
template<typename T, typename Cmp>
void ConcurrentPriorityQueue<T, Cmp>::push_all(const std::vector<T> &elems) {
	{
		std::lock_guard<std::mutex> lock(mutex);
		for (auto &t : elems) {
			queue.push(t);
		}
	}
	condvar.notify_all();
}
template<typename T, typename Cmp>
T ConcurrentPriorityQueue<T, Cmp>::pop() {
	std::unique_lock<std::mutex> lock(mutex);
	condvar.wait(lock, [&](){ return !queue.empty(); });
	T t = queue.top();
	queue.pop();
	return t;
}
template<typename T, typename Cmp>
bool ConcurrentPriorityQueue<T, Cmp>::try_pop(T &t) {
	std::unique_lock<std::mutex> lock(mutex);
	if (condvar.wait_for(lock, std::chrono::nanoseconds(0), [&](){ return !queue.empty(); })) {
		t = queue.top();
		queue.pop();
		return true;
	}
	return false;
}
template<typename T, typename Cmp>
bool ConcurrentPriorityQueue<T, Cmp>::try_pop_for(T &t, const std::chrono::nanoseconds &time_out) {
	std::unique_lock<std::mutex> lock(mutex);
	if (condvar.wait_for(lock, time_out, [&](){ return !queue.empty(); })) {
		t = queue.top();
		queue.pop();
		return true;
	}
	return false;
}

}

