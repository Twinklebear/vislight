#ifndef GLT_FRAMEBUFFER_H
#define GLT_FRAMEBUFFER_H

#include "vl/vislight_api.h"
#include "gl_core_4_5.h"

namespace glt {
/*
 * Checks if the passed framebuffer is complete, returns true if it is
 */
VISLIGHT_API bool check_framebuffer(GLuint fbo);
}

#endif

