#ifndef VISLIGHT_PLUGIN_H
#define VISLIGHT_PLUGIN_H

#include <iostream>
#include <string>
#include <vector>
#include <memory>

#include <SDL.h>
#include <imgui.h>
#include <glm/glm.hpp>

#ifdef _WIN32
#include <windows.h>
#else
#include <dlfcn.h>
#endif

#include "glt/buffer_allocator.h"
#include "vislight_api.h"
#include "file_name.h"
#include "plugin_message.h"

#if defined(_WIN32) || defined(__CYGWIN__)
	#ifndef BUILDING_VISLIGHT
		#define PLUGIN_LOAD_FN(PLUGIN) \
		extern "C" __declspec(dllexport) vl::PluginFunctionTable::InitFn vislight_plugin_##PLUGIN##_load(){ \
			std::cout << "DEBUG: Plugin '" << #PLUGIN << "' is loading\n"; \
			return vislight_plugin_##PLUGIN##_init; \
		}
		#define PLUGIN_UNLOAD_FN(PLUGIN, FCN) \
		extern "C" __declspec(dllexport) void vislight_plugin_##PLUGIN##_unload(){ \
			std::cout << "DEBUG: Plugin '" << #PLUGIN << "' is unloading\n"; \
			FCN(); \
		}
	#endif
#else
	#if __GNUC__ >= 4
		#ifndef BUILDING_VISLIGHT
			#define PLUGIN_LOAD_FN(PLUGIN) \
			extern "C" __attribute__((visibility("default"))) vl::PluginFunctionTable::InitFn \
			vislight_plugin_##PLUGIN##_load(){ \
				std::cout << "DEBUG: Plugin '" << #PLUGIN << "' is loading\n"; \
				return vislight_plugin_##PLUGIN##_init; \
			}
			#define PLUGIN_UNLOAD_FN(PLUGIN, FCN) \
			extern "C" __attribute__((visibility("default"))) void vislight_plugin_##PLUGIN##_unload(){ \
				std::cout << "DEBUG: Plugin '" << #PLUGIN << "' is unloading\n"; \
				FCN(); \
			}
		#endif
	#endif
#endif

namespace vl {

/* Various types of plugins that can be created. These
 * flags can be OR'd together as well if a plugin provides
 * a combination of functionality
 */
enum PluginType {
	INVALID = 0,
	LOADER = 1,
	RENDER = 1 << 1,
	UI_ELEMENT = 1 << 2
};

struct VISLIGHT_API PluginFunctionTable {
	// TODO: This will need to take some pointer or something
	// to a plugin registry or something so the plugin can set itself up
	using InitFn = bool (*)(const std::vector<std::string>&, PluginFunctionTable&,
			MessageDispatcher &dispatch);
	using UnloadFn = void (*)();
	// The LoadFn is used to bootstrap up to C++ calling since we can
	// only find extern "C" functions in the library initially due
	// to name mangling. This function tells us who to really call to init
	using LoadFn = InitFn (*)();
	using LoaderFn = bool (*)(const vl::FileName&);
	// TODO: This is a poor way to get good rendering. It would need to
	// be abstracted in a way that we can manage allocations and rendering
	// internally and do things like merge draws and buffers
	using RenderFn = void (*)(std::shared_ptr<glt::BufferAllocator> &buf_allocator,
		const glm::mat4 &view, const glm::mat4 &projection, const float elapsed);
	using UiFn = void (*)();
	using EventFn = void (*)(const SDL_Event&);
	// It's a bit annoying that we can't merge motion, wheel and click
	// events for mice. Perhaps we can wrap the SDL events (this is what we should do)
	using MessageFn = void (*)(const std::shared_ptr<PluginMessage> &);

	int plugin_type = INVALID;
	/* File extensions handled by this plugin if it's a loader, should be
	 * comma separated list of file extensions, e.g. "raw,png,jpg"
	 */
	std::string file_extensions;

	InitFn init_fn = nullptr;
	UnloadFn unload_fn = nullptr;
	LoadFn load_fn = nullptr;

	LoaderFn loader_fn = nullptr;
	RenderFn render_fn = nullptr;
	UiFn ui_fn = nullptr;
	EventFn event_fn = nullptr;
	MessageFn message_fn = nullptr;

	// Check that we've found functions to call for all
	// functionality the plugin is claiming to implement
	bool validate() const;
};

/* Manages loading, initialization calling and unloading of plugins
 */
class VISLIGHT_API Plugin {
#ifdef _WIN32
	HMODULE plugin;
#else
	void *plugin;
#endif

	std::string name;

public:
	Plugin(const std::string name, std::vector<std::string> args, MessageDispatcher &dispatcher);
	// We'd need some ref-counting internally to not release
	// the plugin too early so copying is disabled for now
	Plugin(const Plugin &) = delete;
	Plugin& operator=(const Plugin &) = delete;
	~Plugin();
	int plugin_type() const;
	bool loads_file(const vl::FileName &file) const;
	const std::string& get_name() const;
	// Send a message to this plugin
	// TODO: Instead of returning a bool should this function
	// be able to return a std::unique_ptr<PluginMessage> ? Then
	// plugins can send more informative error messages back
	bool send_message(const std::shared_ptr<PluginMessage> &msg);
	// TODO: Properly wrap calls to the functions for the plugin
	PluginFunctionTable fcn_table;

private:
	// Load a function pointer from the shared library. Note that T
	// must be a pointer to function type.
	template<typename T>
	T get_fn(const std::string &fcn_name){
#ifdef _WIN32
		FARPROC fn = GetProcAddress(plugin, fcn_name.c_str());
#else
		void *fn = dlsym(plugin, fcn_name.c_str());
#endif
		if (fn == NULL){
			std::cout << "no such function '" << fcn_name << "'\n";
			return nullptr;
		} else {
			return (T)fn;
		}
	}
};

}

#endif

