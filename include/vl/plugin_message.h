#ifndef VISLIGHT_PLUGIN_MESSAGE_H
#define VISLIGHT_PLUGIN_MESSAGE_H

#include <string>
#include <memory>
#include <queue>

#include "vislight_api.h"

namespace vl {

class Plugin;

/* Base structure for communicating messages between plugins
 */
struct VISLIGHT_API PluginMessage {
	virtual ~PluginMessage();
	// The name of the plugin who sent this message
	virtual const std::string& from_plugin() const = 0;
	// The name of the plugin this message is going too
	virtual const std::string& to_plugin() const = 0;
	/* The name of this message so the plugins sending/receiving
	 * can identify it
	 */
	virtual const std::string& name() const = 0;
};

/* Message dispatcher for plugins, create a message
 * and call 'send' to dispatch it to the receiving plugin
 * TODO: Need some wildcard message filters to send to multiple
 * plugins or to all
 */
class VISLIGHT_API MessageDispatcher {
	std::queue<std::shared_ptr<PluginMessage>> messages;

public:
	// Dispatch messages in the queue to the target plugins
	void dispatch(std::vector<std::unique_ptr<Plugin>> &plugins);
	// Send a message to some other plugin through the dispatcher
	void send(std::shared_ptr<PluginMessage> msg);
};

}

#endif

