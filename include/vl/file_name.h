#ifndef FILE_NAME_H
#define FILE_NAME_H

#include <string>
#include <ostream>

#include "vislight_api.h"

namespace vl {

struct VISLIGHT_API FileName {
	std::string file_name;

	// Construct a new, normalized file path
	FileName(const std::string &file_name);
	FileName path() const;
	std::string extension() const;
	FileName join(const FileName &other) const;
	// Normalize the string path, replacing all '\' with '/'
	void normalize();
	// Get a pointer to the underlying characters
	const char* c_str() const;
	bool empty() const;
	size_t size() const;
};
}

VISLIGHT_API std::ostream& operator<<(std::ostream &os, const vl::FileName &f);

#endif

