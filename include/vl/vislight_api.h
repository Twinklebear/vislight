#ifndef VISLIGHT_API_H
#define VISLIGHT_API_H

#if defined(_WIN32) || defined(__CYGWIN__)
	#ifndef BUILDING_VISLIGHT
		#define VISLIGHT_API __declspec(dllimport)
	#else 
		#define VISLIGHT_API __declspec(dllexport)
	#endif
#else
	#if __GNUC__ >= 4
		#define VISLIGHT_API
	#endif
#endif

#endif

