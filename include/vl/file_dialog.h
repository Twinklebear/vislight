#ifndef VISLIGHT_FILE_DIALOG_H
#define VISLIGHT_FILE_DIALOG_H

#include <string>

#include "vl/vislight_api.h"
#include "vl/file_name.h"

namespace vl {

enum class FileDialogStatus {
	FILE_OKAY,
	FILE_CANCELLED,
	FILE_ERROR
};

/* Result returned from prompting the user to open
 * a file through the file dialog.
 * if status == FILE_OKAY then result contains the selected file name
 * if status == FILE_ERROR then result contains the error string
 * if status == FILE_CANCELLED the user cancelled choosing a file
 */
struct VISLIGHT_API FileResult {
	FileDialogStatus status;
	vl::FileName result = vl::FileName{""};
};

class VISLIGHT_API FileDialog {
	std::string filters, default_path;

public:
	/* Construct a file dialog that when opened will prompt the
	 * users for files matching the filters starting at the
	 * default path passed.
	 * The filters are specified by a list of comma and semicolon
	 * separated values. A comma adds a separate type to a filter
	 * while a semicolon starts a new filter, e.g.
	 * 'png,jpg;psd'
	 * The default filter is for png and jpg files, while a second
	 * is available for psd
	 */
	FileDialog(const std::string &filters = "", const std::string &default_path = "");
	// TODO: Should it be possible have different filters/default path for
	// open & save? Should this FileDialog class really just be a function?
	FileResult open_file() const;
	FileResult save_file() const;
};

}

#endif

