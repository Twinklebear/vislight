#ifndef VISLIGHT_DISPLAY_TARGET_H
#define VISLIGHT_DISPLAY_TARGET_H

#include "vl/vislight_api.h"

namespace vl {

/* A target the is displayed to the user in some way, could be
 * a single screen or a HMD.
 */
class VISLIGHT_API DisplayTarget {
public:
	virtual ~DisplayTarget(){}
	/* Perform some per-frame setup required by the display target
	 * e.g. get eye & head tracking positions, etc.
	 */
	virtual void begin_frame() = 0;
	/* Determine how many times the render loop should be called,
	 * e.g. twice for an HMD to render to each eye
	 */
	virtual size_t render_count() = 0;
	/* Setup render targets to be drawn to for this render loop iteration,
	 * returns back the view and projection matrices
	 */
	virtual void begin_render(const size_t iteration, glm::mat4 &view, glm::mat4 &proj) = 0; 
	// Finish rendering for this iteration
	virtual void end_render(const size_t iteration) = 0;
	// Display the rendering results to the user
	virtual void display() = 0;
};

}

#endif

