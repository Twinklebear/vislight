#ifndef VISLIGHT_MONITOR_DISPLAY_H
#define VISLIGHT_MONITOR_DISPLAY_H

#include <SDL.h>

#include "display_target.h"

namespace vl {

// Display for rendering to a single framebuffer on a monitor
class VISLIGHT_API MonitorDisplay : public DisplayTarget {
	SDL_Window *window = nullptr;

public:
	MonitorDisplay(SDL_Window *win);
	~MonitorDisplay() override;
	/* Perform some per-frame setup required by the display target
	 * e.g. get eye & head tracking positions, etc.
	 */
	void begin_frame() override;
	/* Determine how many times the render loop should be called,
	 * e.g. twice for an HMD to render to each eye
	 */
	size_t render_count() override;
	/* Setup render targets to be drawn to for this render loop iteration,
	 * returns back the view and projection matrices
	 */
	void begin_render(const size_t iteration, glm::mat4 &view, glm::mat4 &proj) override;
	// Finish rendering for this iteration
	void end_render(const size_t iteration) override;
	// Display the rendering results to the user
	void display() override;
};
}

#endif

