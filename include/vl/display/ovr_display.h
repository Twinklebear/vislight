#ifndef VISLIGHT_OVR_DISPLAY_H
#define VISLIGHT_OVR_DISPLAY_H

#if VR_ENABLED

#include <array>

#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <OVR_CAPI_GL.h>

#include "vl/glt/gl_core_4_5.h"
#include "display_target.h"

namespace vl {

// Display for rendering data to an Oculus VR HMD
class VISLIGHT_API OVRDisplay : public DisplayTarget {
	SDL_Window *window = nullptr;
	// Our Oculus session info
	ovrSession session;
	ovrHmdDesc hmd_desc;

	// Left, Right eye FBOs
	std::array<GLuint, 2> eye_fbos;
	// Left, Right depth textures, the color buffers are managed by OVR
	std::array<GLuint, 2> eye_depth_tex;
	// Left, Right texture swap chains
	std::array<ovrTextureSwapChain, 2> swap_chains;
	// Setup eye swap chains
	std::array<ovrSizei, 2> eye_texture_size;
	// Mirror texture for displaying on the monitor as well
	GLuint mirror_fbo = 0;
	GLuint mirror_tex_id = 0;
	ovrMirrorTexture mirror_tex = nullptr;
	// Postprocessing fade out shader for when tracking is lost
	GLuint postprocess_fade_shader = 0;
	float fade_alpha = 0.0;
	bool had_position_tracking = false;
	GLuint dummy_vao = 0;
	// Rendering poses for each eye
	ovrPosef eye_render_pos[2];
	size_t frame_index = 0;
	double sensor_sample_time = 0;

public:
	OVRDisplay(SDL_Window *win);
	~OVRDisplay() override;
	/* Perform some per-frame setup required by the display target
	 * e.g. get eye & head tracking positions, etc.
	 */
	void begin_frame() override;
	/* Determine how many times the render loop should be called,
	 * e.g. twice for an HMD to render to each eye
	 */
	size_t render_count() override;
	/* Setup render targets to be drawn to for this render loop iteration,
	 * returns back the view and projection matrices
	 */
	void begin_render(const size_t iteration, glm::mat4 &view, glm::mat4 &proj) override;
	// Finish rendering for this iteration
	void end_render(const size_t iteration) override;
	// Display the rendering results to the user
	void display() override;
	// Get the OVR session
	ovrSession get_session();
};
}
#endif

#endif

