#pragma once

#include <utility>
#include <vector>
#include <cstddef>
#include <unordered_map>

#include "vislight_api.h"

namespace vl {

/* A disjoint set data structure which can compute the connected components
 * of some graph structure.
 */
struct VISLIGHT_API DisjointSet {
	using ConnectedComponents = std::unordered_map<size_t, std::vector<size_t>>;

	// map of node ids to parent ids
	std::unordered_map<size_t, size_t> sets;

	void make_set(const size_t i);
	// Union the sets containing i and j together.
	void set_union(const size_t i, const size_t j);
	// Return the connected components of the disjoint set structure
	ConnectedComponents connected_components();

private:
	size_t find(const size_t n);
};

}

