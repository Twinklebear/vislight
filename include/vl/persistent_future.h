#pragma once

#include <cassert>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <chrono>

namespace vl {

/* A persistent future value. Similar to std::future, however
 * get can be called multiple times to return the same value that
 * was set for the result.
 */
template<typename T>
class PersistentFuture {
	struct FutureState {
		T val;
		bool is_set;
		std::mutex mutex;
		std::condition_variable condvar;

		FutureState() : is_set(false) {}
	};
	std::shared_ptr<FutureState> state;

public:
	PersistentFuture() : state(std::make_shared<FutureState>()) {}
	// Set the value of the future
	void set(T &t);
	// Wait for the value to be set and retrieve it
	T& get() const;
	/* Try to get the value, returns false immediately if it's
	 * not set
	 */
	bool try_get(T &t) const;
	/* Try to get the value and wait for some timeout, returns
	 * false immediately if it's not set
	 */
	bool try_get_for(T &t, const std::chrono::nanoseconds &time_out) const;
	// Check if the future is set
	bool is_set() const;
};
template<typename T>
void PersistentFuture<T>::set(T &t) {
	{
		std::lock_guard<std::mutex> lock(state->mutex);
		assert(!state->is_set);
		state->val = t;
		state->is_set = true;
	}
	state->condvar.notify_all();
}
template<typename T>
T& PersistentFuture<T>::get() const {
	std::unique_lock<std::mutex> lock(state->mutex);
	state->condvar.wait(lock, [&](){ return state->is_set; });
	return state->val;
}
template<typename T>
bool PersistentFuture<T>::try_get(T &t) const {
	std::unique_lock<std::mutex> lock(state->mutex);
	if (state->condvar.wait_for(lock, std::chrono::nanoseconds(0), [&](){ return state->is_set; })) {
		t = state->val;
		return true;
	}
	return false;
}
template<typename T>
bool PersistentFuture<T>::try_get_for(T &t, const std::chrono::nanoseconds &time_out) const {
	std::unique_lock<std::mutex> lock(state->mutex);
	if (state->condvar.wait_for(lock, time_out, [&](){ return state->is_set; })) {
		t = state->val;
		return true;
	}
	return false;
}
template<typename T>
bool PersistentFuture<T>::is_set() const {
	std::lock_guard<std::mutex> lock(state->mutex);
	return state->is_set;
}

}

