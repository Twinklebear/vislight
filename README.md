# vislight

Lightweight visualization

## Building

[SDL2](https://www.libsdl.org/index.php) and [GLM](http://glm.g-truc.net/0.9.7/index.html) are required,
If SDL2 and/or GLM aren't installed in the standard location (or you're on windows) you can pass `-DSDL2=<path>`
and/or `-DGLM=<path>` to specify the root directories for SDL2 and GLM to find the includes and libraries.
[Hana](https://bitbucket.org/hoangthaiduong/hana) is also required, the search path for Hana can be
optionally specified with `-DHANA=<path>`.

## Volume Rendering

To render a .raw or .idx file drag it and drop it onto the vislight window. For raw files you must
also specify the volume dimensions and data type before it can be rendered, the IDX file will default
to displaying the first field/timestep at the lowest quality level.

## Loading Plugins

To specify a plugin to load (e.g. the provided `test` plugin) run vislight with the `--plugin` command
followed by the plugin name and any arguments for it:

```
./vislight --plugin test some_args --plugin my_plugin other args
```

