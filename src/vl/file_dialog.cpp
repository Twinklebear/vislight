#include <nfd.h>

#include "vl/file_dialog.h"

vl::FileDialog::FileDialog(const std::string &filters, const std::string &default_path)
	: filters(filters), default_path(default_path)
{}
vl::FileResult vl::FileDialog::open_file() const {
	nfdchar_t *out_path = nullptr;
	const nfdchar_t *filter_ptr = nullptr;
	const nfdchar_t *path_ptr = nullptr;
	if (!filters.empty()){
		filter_ptr = filters.c_str();
	}
	if (!default_path.empty()){
		path_ptr = default_path.c_str();
	}

	nfdresult_t nfd_result = NFD_OpenDialog(filter_ptr, path_ptr, &out_path);
	vl::FileResult result;
	if (nfd_result == NFD_OKAY){
		result.status = vl::FileDialogStatus::FILE_OKAY;
		result.result = std::string{out_path};
		free(out_path);
	} else if (nfd_result == NFD_CANCEL){
		result.status = vl::FileDialogStatus::FILE_CANCELLED;
	} else {
		result.status = vl::FileDialogStatus::FILE_ERROR;
		result.result = std::string{NFD_GetError()};
	}
	return result;
}
vl::FileResult vl::FileDialog::save_file() const {
	nfdchar_t *out_path = nullptr;
	const nfdchar_t *filter_ptr = nullptr;
	const nfdchar_t *path_ptr = nullptr;
	if (!filters.empty()){
		filter_ptr = filters.c_str();
	}
	if (!default_path.empty()){
		path_ptr = default_path.c_str();
	}

	nfdresult_t nfd_result = NFD_SaveDialog(filter_ptr, path_ptr, &out_path);
	vl::FileResult result;
	if (nfd_result == NFD_OKAY){
		result.status = vl::FileDialogStatus::FILE_OKAY;
		result.result = std::string{out_path};
		free(out_path);
	} else if (nfd_result == NFD_CANCEL){
		result.status = vl::FileDialogStatus::FILE_CANCELLED;
	} else {
		result.status = vl::FileDialogStatus::FILE_ERROR;
		result.result = std::string{NFD_GetError()};
	}
	return result;
}

