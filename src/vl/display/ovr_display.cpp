#include <iostream>
#include <stdexcept>

#include <SDL.h>

#include "vl/glt/util.h"
#include "vl/display/ovr_display.h"

vl::OVRDisplay::OVRDisplay(SDL_Window *win) : window(win) {
	// Setup Oculus lib and get the HMD
	ovrResult result = ovr_Initialize(nullptr);
	if (!OVR_SUCCESS(result)){
		std::cout << "Error initializing OVR: " << result << "\n";
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Oculus VR Error", "Failed to initialize OVR", win);
		throw std::runtime_error("Failed to init OVR");
	}
	std::cout << "Oculus VR Initialized, LibOVR Version: " << ovr_GetVersionString() << "\n";
	ovrGraphicsLuid luid;
	result = ovr_Create(&session, &luid);
	if (!OVR_SUCCESS(result)){
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Oculus VR Error", "Failed to create session", win);
		throw std::runtime_error("Failed to create OVR session");
	}
	std::cout << "Got Oculus VR session\n";

	hmd_desc = ovr_GetHmdDesc(session);
	std::cout << "Got Oculus VR HMD:\n\tManufacturer: " << hmd_desc.Manufacturer
		<< "\n\tSerial Number: " << hmd_desc.SerialNumber << "\n";
	SDL_SetWindowSize(win, hmd_desc.Resolution.w / 2, hmd_desc.Resolution.h / 2);

	// Setup textures for the eyes
	glGenFramebuffers(eye_fbos.size(), eye_fbos.data());
	// Left, Right depth textures, the color buffers are managed by OVR
	glGenTextures(eye_depth_tex.size(), eye_depth_tex.data());
	// Setup eye swap chains
	for (size_t i = 0; i < 2; ++i){
		eye_texture_size[i] = ovr_GetFovTextureSize(session, ovrEyeType(i), hmd_desc.DefaultEyeFov[i], 1);
		std::cout << "Texture for eye " << i << " dims {" << eye_texture_size[i].w
			<< ", " << eye_texture_size[i].h << "}\n";
		ovrTextureSwapChainDesc desc = {};
		desc.Type = ovrTexture_2D;
		desc.ArraySize = 1;
		desc.Width = eye_texture_size[i].w;
		desc.Height = eye_texture_size[i].h;
		desc.MipLevels = 1;
		desc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
		desc.SampleCount = 1;
		desc.StaticImage = ovrFalse;

		ovrResult result = ovr_CreateTextureSwapChainGL(session, &desc, &swap_chains[i]);
		if (!OVR_SUCCESS(result)){
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Oculus VR Error", "Failed to set up texture swap chains", win);
			throw std::runtime_error("Failed to setup OVR texture swap chains");
		}
		int length = 0;
		ovr_GetTextureSwapChainLength(session, swap_chains[i], &length);
		std::cout << "swap chain for eye " << i << " is " << length << " long\n";
		// Setup filtering parameters for the textures in the swap chain
		for (int j = 0; j < length; ++j){
			GLuint tex_id;
			ovr_GetTextureSwapChainBufferGL(session, swap_chains[i], j, &tex_id);
			glBindTexture(GL_TEXTURE_2D, tex_id);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		}
		// Setup depth buffer
		glBindTexture(GL_TEXTURE_2D, eye_depth_tex[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, eye_texture_size[i].w, eye_texture_size[i].h,
			0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	}

	// Setup the mirror texture
	ovrMirrorTextureDesc desc;
	desc.Width = hmd_desc.Resolution.w / 2;
	desc.Height = hmd_desc.Resolution.h / 2;
	desc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
	desc.MiscFlags = 0;
	result = ovr_CreateMirrorTextureGL(session, &desc, &mirror_tex);
	if (!OVR_SUCCESS(result)){
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Oculus VR Error", "Failed to set up mirror texture", win);
		throw std::runtime_error("Failed to setup OVR mirror texture");
	}
	// Setup mirror FBO
	ovr_GetMirrorTextureBufferGL(session, mirror_tex, &mirror_tex_id);
	glGenFramebuffers(1, &mirror_fbo);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, mirror_fbo);
	glFramebufferTexture2D(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mirror_tex_id, 0);
	glFramebufferRenderbuffer(GL_READ_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

	// The sample shows setting swap interval to 0 to let the compositor do its "magic"
	// What magic is done?
	SDL_GL_SetSwapInterval(0);

	// Track such that the floor height is 0
	ovr_SetTrackingOriginType(session, ovrTrackingOrigin_FloorLevel);

	// Setup shader to do postprocessing
	const std::string res_path = glt::get_resource_path();
	postprocess_fade_shader = glt::load_program({std::make_pair(GL_VERTEX_SHADER, res_path + "postprocess_vert.glsl"),
		std::make_pair(GL_FRAGMENT_SHADER, res_path + "fade_frag.glsl")});
	glGenVertexArrays(1, &dummy_vao);
}
vl::OVRDisplay::~OVRDisplay() {
	ovr_DestroyMirrorTexture(session, mirror_tex);
	for (auto &c : swap_chains){
		ovr_DestroyTextureSwapChain(session, c);
	}
	glDeleteFramebuffers(1, &mirror_fbo);
	glDeleteTextures(1, &mirror_tex_id);
	glDeleteProgram(postprocess_fade_shader);
	glDeleteFramebuffers(eye_fbos.size(), eye_fbos.data());
	glDeleteTextures(eye_depth_tex.size(), eye_depth_tex.data());
	glDeleteVertexArrays(1, &dummy_vao);
	ovr_Destroy(session);
	ovr_Shutdown();
}
void vl::OVRDisplay::begin_frame() {
	// Get the eye position information
	std::array<ovrEyeRenderDesc, 2> eye_render_desc = {
		ovr_GetRenderDesc(session, ovrEye_Left, hmd_desc.DefaultEyeFov[0]),
		ovr_GetRenderDesc(session, ovrEye_Right, hmd_desc.DefaultEyeFov[1])
	};
	ovrVector3f hmd_to_eye_offset[2] = {
		eye_render_desc[0].HmdToEyeOffset,
		eye_render_desc[1].HmdToEyeOffset
	};
	ovr_GetEyePoses(session, frame_index, ovrTrue, hmd_to_eye_offset, eye_render_pos, &sensor_sample_time);

	ovrTrackingState tracking_state = ovr_GetTrackingState(session, sensor_sample_time, ovrFalse);
	if (!(tracking_state.StatusFlags & ovrStatus_PositionTracked) && had_position_tracking){
		std::cout << "Lost position tracking, fading....\n";
	}
	else if ((tracking_state.StatusFlags & ovrStatus_PositionTracked) && !had_position_tracking){
		std::cout << "Re-gained position tracking\n";
	}
	had_position_tracking = tracking_state.StatusFlags & ovrStatus_PositionTracked;

	if (had_position_tracking){
		fade_alpha = std::max(fade_alpha - 0.01f, 0.0f);
	} else {
		fade_alpha = std::min(fade_alpha + 0.01f, 0.9f);
	}
}
size_t vl::OVRDisplay::render_count() {
	return 2;
}
void vl::OVRDisplay::begin_render(const size_t iteration, glm::mat4 &view, glm::mat4 &proj) {
	GLuint cur_eye_tex = 0;
	int cur_idx;
	ovr_GetTextureSwapChainCurrentIndex(session, swap_chains[iteration], &cur_idx);
	ovr_GetTextureSwapChainBufferGL(session, swap_chains[iteration], cur_idx, &cur_eye_tex);
	glBindFramebuffer(GL_FRAMEBUFFER, eye_fbos[iteration]);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, cur_eye_tex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, eye_depth_tex[iteration], 0);

	glViewport(0, 0, eye_texture_size[iteration].w, eye_texture_size[iteration].h);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Compute eye transform
	glm::quat eye_orientation(eye_render_pos[iteration].Orientation.w, eye_render_pos[iteration].Orientation.x,
		eye_render_pos[iteration].Orientation.y, eye_render_pos[iteration].Orientation.z);
	glm::vec3 up = eye_orientation * glm::vec3(0, 1, 0);
	glm::vec3 forward = eye_orientation * glm::vec3(0, 0, -1);
	glm::vec3 eye_pos = glm::vec3(eye_render_pos[iteration].Position.x,
		eye_render_pos[iteration].Position.y, eye_render_pos[iteration].Position.z);
	// Use Oculus's projection matrix calculation, it seems glm doesn't quite match it
	ovrMatrix4f ovrproj = ovrMatrix4f_Projection(hmd_desc.DefaultEyeFov[iteration], 0.2f, 1000.0f, ovrProjection_None);

	// Return the view and projection matrices to the caller
	// Oculus stores the matrix row-major, so transpose when writing to the GL matrix
	for (size_t r = 0; r < 4; ++r){
		for (size_t c = 0; c < 4; ++c){
			proj[c][r] = ovrproj.M[r][c];
		}
	}
	view = glm::lookAt(eye_pos, eye_pos + forward, up);
}
void vl::OVRDisplay::end_render(const size_t iteration) {
	// fade-in/out the screen if position tracking was lost/gained
	// TODO: consider using separate layer for fade-out?
	if (fade_alpha > 0.0f){
		glDisable(GL_DEPTH_TEST);
		glUseProgram(postprocess_fade_shader);
		glProgramUniform1f(postprocess_fade_shader, 0, fade_alpha);
		glBindVertexArray(dummy_vao);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glEnable(GL_DEPTH_TEST);
	}

	// Unattach the swap chain texture from the framebuffer
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
	// TODO: The sample detachs the depth as well, but that's managed by us right? So I don't
	// think we need to detach it
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0);

	// Tell Oculus we're done rendering
	ovr_CommitTextureSwapChain(session, swap_chains[iteration]);
}
void vl::OVRDisplay::display() {
	// Do distortion rendering and submit the frame to the headset
	ovrLayerEyeFov layer_desc;
	layer_desc.Header.Type = ovrLayerType_EyeFov;
	layer_desc.Header.Flags = ovrLayerFlag_TextureOriginAtBottomLeft;
	for (size_t i = 0; i < 2; ++i){
		layer_desc.ColorTexture[i] = swap_chains[i];
		ovrRecti viewport;
		ovrVector2i pos;
		pos.x = 0;
		pos.y = 0;
		layer_desc.Viewport[i].Pos = pos;
		layer_desc.Viewport[i].Size = eye_texture_size[i];
		layer_desc.Fov[i] = hmd_desc.DefaultEyeFov[i];
		layer_desc.RenderPose[i] = eye_render_pos[i];
		layer_desc.SensorSampleTime = sensor_sample_time;
	}

	// Submit the frame to the headset
	ovrLayerHeader *layers = &layer_desc.Header;
	ovrResult result = ovr_SubmitFrame(session, frame_index, nullptr, &layers, 1);
	if (!OVR_SUCCESS(result)){
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Oculus VR Error", "Failed to submit frame to HMD", window);
		throw std::runtime_error("OVR error: failed to submit to HMD");
	}

	// Blit the mirror texture
	glBindFramebuffer(GL_READ_FRAMEBUFFER, mirror_fbo);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBlitFramebuffer(0, hmd_desc.Resolution.h / 2, hmd_desc.Resolution.w / 2, 0,
		0, 0, hmd_desc.Resolution.w / 2, hmd_desc.Resolution.h / 2, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
	++frame_index;
}
ovrSession vl::OVRDisplay::get_session() {
	return session;
}

