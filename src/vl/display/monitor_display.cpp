#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include "vl/glt/util.h"
#include "vl/display/monitor_display.h"

vl::MonitorDisplay::MonitorDisplay(SDL_Window *win) : window(win){
	SDL_SetWindowSize(window, 1280, 720);
}
vl::MonitorDisplay::~MonitorDisplay(){}
void vl::MonitorDisplay::begin_frame(){}
size_t vl::MonitorDisplay::render_count(){
	return 1;
}
void vl::MonitorDisplay::begin_render(const size_t, glm::mat4 &view, glm::mat4 &proj){
	// TODO: How to put back in the arcball camera controls in this sytem? Some abstraction
	// for the VR vs. keyboard & mouse camera interactions would be needed and might be a bit
	// of a pain. Or maybe not actually that bad.
	view = glm::lookAt(glm::vec3{0.4f, 1.1f, 0.6f}, glm::vec3{0.4f, 1.14f, 0.f}, glm::vec3{0.f, 1.f, 0.f});
	proj = glm::perspective(glt::to_radians(70), 1280.f / 720.f, 0.1f, 600.f);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
void vl::MonitorDisplay::end_render(const size_t){}
void vl::MonitorDisplay::display(){}

