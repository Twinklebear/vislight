/* TODO: format error codes */

#include <cinttypes>
#include <cstdio>
#include <iostream>
#include <stdexcept>

#include "vl/display/openvr_display.h"


static glm::mat4 m34_to_mat4(const vr::HmdMatrix34_t &t) {
	return glm::mat4(
		t.m[0][0], t.m[1][0], t.m[2][0], 0.0f,
		t.m[0][1], t.m[1][1], t.m[2][1], 0.0f,
		t.m[0][2], t.m[1][2], t.m[2][2], 0.0f,
		t.m[0][3], t.m[1][3], t.m[2][3], 1.0f
	);
}


static glm::mat4 m44_to_mat4(const vr::HmdMatrix44_t &t) {
	return glm::mat4(
		t.m[0][0], t.m[1][0], t.m[2][0], t.m[3][0],
		t.m[0][1], t.m[1][1], t.m[2][1], t.m[3][1],
		t.m[0][2], t.m[1][2], t.m[2][2], t.m[3][2],
		t.m[0][3], t.m[1][3], t.m[2][3], t.m[3][3]
	);
}


vl::OpenVRDisplay::OpenVRDisplay(SDL_Window *window) : window(window) {
	vr::EVRInitError error;
	system = vr::VR_Init(&error, vr::VRApplication_Scene);
	if (error != vr::VRInitError_None) {
		fprintf(stderr, "OpenVR: Initialization error %d\n", error);
		throw std::runtime_error("Failed to initialize OpenVR");
	}

	const vr::TrackedDeviceIndex_t hmd_index = 0;
	if (!system->IsTrackedDeviceConnected(vr::k_unTrackedDeviceIndex_Hmd + hmd_index)) {
		fprintf(stderr, "OpenVR: Tracking device %" PRIu32 " not connected\n", hmd_index);
		throw std::runtime_error("Failed to initialize OpenVR");
	}

	vr_compositor = (vr::IVRCompositor *)vr::VR_GetGenericInterface(vr::IVRCompositor_Version, &error);
	if (error != vr::VRInitError_None) {
		fprintf(stderr, "OpenVR: Compositor initialization error\n");
		throw std::runtime_error("Failed to initialize OpenVR");
	}

	system->GetRecommendedRenderTargetSize(&render_dims[0], &render_dims[1]);

	for (int eye = 0; eye < 2; ++eye) {
		glCreateFramebuffers(1, &fb_descs[eye].render_fb);

		glCreateTextures(GL_TEXTURE_2D, 1, &fb_descs[eye].render_texture);
		glTextureStorage2D(fb_descs[eye].render_texture, 1, GL_SRGB8_ALPHA8, render_dims[0], render_dims[1]);

		glCreateTextures(GL_TEXTURE_2D, 1, &fb_descs[eye].depth_texture);
		glTextureStorage2D(fb_descs[eye].depth_texture, 1, GL_DEPTH_COMPONENT32F, render_dims[0], render_dims[1]);

		glNamedFramebufferTexture(fb_descs[eye].render_fb, GL_COLOR_ATTACHMENT0, fb_descs[eye].render_texture, 0);
		glNamedFramebufferTexture(fb_descs[eye].render_fb, GL_DEPTH_ATTACHMENT, fb_descs[eye].depth_texture, 0);
		GLenum status = glCheckNamedFramebufferStatus(fb_descs[eye].render_fb, GL_DRAW_FRAMEBUFFER);
		if (status != GL_FRAMEBUFFER_COMPLETE) {
			fprintf(stderr, "GL: Render framebuffer incomplete\n");
			throw std::runtime_error("Failed to initialize OpenVR");
		}

		/* rendered data gets blit into this texture (i.e. this is eye texture), not multisampled */
		glCreateFramebuffers(1, &fb_descs[eye].resolve_fb);

		glCreateTextures(GL_TEXTURE_2D, 1, &fb_descs[eye].resolve_texture);
		glTextureStorage2D(fb_descs[eye].resolve_texture, 1, GL_SRGB8_ALPHA8, render_dims[0], render_dims[1]);

		glNamedFramebufferTexture(fb_descs[eye].resolve_fb, GL_COLOR_ATTACHMENT0, fb_descs[eye].resolve_texture, 0);
		status = glCheckNamedFramebufferStatus(fb_descs[eye].resolve_fb, GL_DRAW_FRAMEBUFFER);
		if (status != GL_FRAMEBUFFER_COMPLETE) {
			fprintf(stderr, "GL: Eye framebuffer incomplete\n");
			throw std::runtime_error("Failed to initialize OpenVR");
		}
	}

	/* eye to head transform and eye projection matrices */
	const float near_clip = 0.1f;
	const float far_clip  = 100.0f;

	matrices.projection_eyes[0] = m44_to_mat4(system->GetProjectionMatrix(vr::Eye_Left, near_clip, far_clip, vr::API_OpenGL));
	matrices.head_to_eyes[0]    = glm::inverse(m34_to_mat4(system->GetEyeToHeadTransform(vr::Eye_Left)));

	matrices.projection_eyes[1] = m44_to_mat4(system->GetProjectionMatrix(vr::Eye_Right, near_clip, far_clip, vr::API_OpenGL));
	matrices.head_to_eyes[1] = glm::inverse(m34_to_mat4(system->GetEyeToHeadTransform(vr::Eye_Right)));
}


vl::OpenVRDisplay::~OpenVRDisplay() {
	for (int eye = 0; eye < 2; ++eye) {
		glDeleteTextures(1, &fb_descs[eye].depth_texture);
		glDeleteTextures(1, &fb_descs[eye].resolve_texture);
		glDeleteFramebuffers(1, &fb_descs[eye].resolve_fb);

		glDeleteTextures(1, &fb_descs[eye].render_texture);
		glDeleteFramebuffers(1, &fb_descs[eye].render_fb);
	}

	vr::VR_Shutdown();
}


void vl::OpenVRDisplay::begin_frame() {
}


size_t vl::OpenVRDisplay::render_count() {
	return 2;
}


void vl::OpenVRDisplay::begin_render(const size_t iteration, glm::mat4 &view, glm::mat4 &projection) {
	glBindFramebuffer(GL_FRAMEBUFFER, fb_descs[iteration].render_fb);

	glViewport(0, 0, render_dims[0], render_dims[1]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	view       = matrices.head_to_eyes[iteration] * matrices.absolute_to_device;
	projection = matrices.projection_eyes[iteration];
}


void vl::OpenVRDisplay::end_render(const size_t iteration) {
	glBlitNamedFramebuffer(fb_descs[iteration].render_fb, fb_descs[iteration].resolve_fb,
	                       0, 0, render_dims[0], render_dims[1],
	                       0, 0, render_dims[0], render_dims[1],
	                       GL_COLOR_BUFFER_BIT, GL_LINEAR);
}


void vl::OpenVRDisplay::display() {
	/* TODO: this is constant and can be cached */
	vr::Texture_t left_eye = {};
	left_eye.handle      = (void *)fb_descs[0].resolve_texture;
	left_eye.eType       = vr::API_OpenGL;
	left_eye.eColorSpace = vr::ColorSpace_Gamma;

	vr::Texture_t right_eye = {};
	right_eye.handle = (void *)fb_descs[1].resolve_texture;
	right_eye.eType = vr::API_OpenGL;
	right_eye.eColorSpace = vr::ColorSpace_Gamma;
	vr_compositor->Submit(vr::Eye_Left, &left_eye, NULL, vr::Submit_Default);

	vr_compositor->Submit(vr::Eye_Right, &right_eye, NULL, vr::Submit_Default);

	/* this is necessary for rendering to work properly (without it there is strange flicker) */
	vr::TrackedDevicePose_t tracked_device_pose;
	vr_compositor->WaitGetPoses(&tracked_device_pose, 1, NULL, 0);


	/* update device position matrix (device -> world, so we need to take inverse) */
	/* TODO: maybe move into begin_frame */
	matrices.absolute_to_device = glm::inverse(m34_to_mat4(tracked_device_pose.mDeviceToAbsoluteTracking));

	/* blit left eye */
	int window_dims[2];
	SDL_GetWindowSize(window, &window_dims[0], &window_dims[1]);
	glBlitNamedFramebuffer(fb_descs[0].resolve_fb, 0,
	                       0, 0, render_dims[0], render_dims[1],
	                       0, 0, window_dims[0], window_dims[1],
	                       GL_COLOR_BUFFER_BIT, GL_NEAREST);
}