#include <stdexcept>

#include "config.h"
#include "vl/plugin.h"

bool vl::PluginFunctionTable::validate() const {
	using namespace vl;
	if (plugin_type == PluginType::INVALID){
		std::cerr << "Plugin validity check failed: Plugin did not specify a type\n";
		return false;
	}
	if ((plugin_type & PluginType::LOADER) && (!loader_fn || file_extensions.empty())){
		std::cerr << "Plugin validity check failed: plugin claims to be a loader"
			<< " plugin but did not provide one of: a loader function or file extensions\n";
		return false;
	}
	if ((plugin_type & PluginType::RENDER) && !render_fn){
		std::cerr << "Plugin validity check failed: plugin claims to be a render"
			<< " plugin but did not provide a render function\n";
		return false;
	}
	if ((plugin_type & PluginType::UI_ELEMENT) && !ui_fn){
		std::cerr << "Plugin validity check failed: plugin claims to be a UI element"
			<< " plugin but did not provide a UI function or ImGui loader\n";
		return false;
	}
	return true;
}

vl::Plugin::Plugin(const std::string name, std::vector<std::string> args, vl::MessageDispatcher &dispatcher)
	: plugin(nullptr), name(name)
{
	std::string so_name = PLUGIN_PREFIX "vislight_plugin_" + name + PLUGIN_SUFFIX;
	std::cout << "loading plugin " << so_name << "\n";
#ifdef _WIN32
	plugin = LoadLibraryEx(so_name.c_str(), NULL,
			LOAD_LIBRARY_SEARCH_APPLICATION_DIR | LOAD_LIBRARY_SEARCH_SYSTEM32);
#else
	plugin = dlopen(so_name.c_str(), RTLD_LAZY);
#endif
	if (!plugin){
#if _WIN32
		auto err = GetLastError();
		std::cout << "error code: " << err << std::endl;
#else
		std::cout << "error: " << dlerror() << std::endl;
#endif
		std::cout << "failed to load plugin\n";

		// TODO: Get the error from the OS api (GetLastError on win and dlerror on posix)
		throw std::runtime_error("Error: Failed to load plugin " + name);
	}
	// Now find the init function and call it
	fcn_table.load_fn = get_fn<PluginFunctionTable::LoadFn>("vislight_plugin_" + name + "_load");
	fcn_table.unload_fn = get_fn<PluginFunctionTable::UnloadFn>("vislight_plugin_" + name + "_unload");
	if (!fcn_table.load_fn || !fcn_table.unload_fn){
		std::cout << "failed to find init or unload fn\n";
		throw std::runtime_error("Error: Failed to find init or unload function for plugin " + name);
	}
	fcn_table.init_fn = fcn_table.load_fn();
	args[0] = so_name;
	if (!fcn_table.init_fn(args, fcn_table, dispatcher)){
		std::cout << "failed to init\n";
		throw std::runtime_error("Error: Failed to initialize plugin " + name);
	}
	if (!fcn_table.validate()){
		throw std::runtime_error("Error: Plugin function table failed validity check");
	}
}
vl::Plugin::~Plugin(){
	fcn_table.unload_fn();
#ifdef _WIN32
	FreeLibrary(plugin);
#else
	dlclose(plugin);
#endif
}
int vl::Plugin::plugin_type() const {
	return fcn_table.plugin_type;
}
bool vl::Plugin::loads_file(const vl::FileName &file) const {
	return (fcn_table.plugin_type & vl::PluginType::LOADER)
		&& fcn_table.file_extensions.find(file.extension()) != std::string::npos;
}
const std::string& vl::Plugin::get_name() const {
	return name;
}
bool vl::Plugin::send_message(const std::shared_ptr<PluginMessage> &msg){
	// TODO: yea this is not such a robust error handling mechanism
	if (fcn_table.message_fn){
		fcn_table.message_fn(msg);
		return true;
	} else {
		return false;
	}
}

