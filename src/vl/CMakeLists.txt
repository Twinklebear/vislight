add_definitions(-DBUILDING_VISLIGHT)

add_subdirectory(external)
add_subdirectory(glt)
add_subdirectory(display)
add_library(vl SHARED file_name.cpp plugin.cpp imgui_impl.cpp
	volume.cpp file_dialog.cpp plugin_message.cpp disjoint_set.cpp
	$<TARGET_OBJECTS:display> $<TARGET_OBJECTS:glt>
	$<TARGET_OBJECTS:nfd> $<TARGET_OBJECTS:imgui>)
set_target_properties(vl PROPERTIES POSITION_INDEPENDENT_CODE ON)

# ImGUI needs the imm32 library on windows
if (${WIN32})
	set(LIBS ${LIBS} imm32)
endif()

get_target_property(NFD_LIBS nfd NFD_LIBS)
target_link_libraries(vl hana ${NFD_LIBS} ${LIBS} ${CMAKE_DL_LIBS})
install(TARGETS vl DESTINATION bin)

