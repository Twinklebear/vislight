#include <limits>
#include <cassert>
#include <iostream>

#include "vl/disjoint_set.h"

void vl::DisjointSet::make_set(const size_t i) {
	sets[i] = i;
}
void vl::DisjointSet::set_union(const size_t i, const size_t j) {
	const size_t parent_i = find(i);
	const size_t parent_j = find(j);
	sets[parent_i] = parent_j;
}
vl::DisjointSet::ConnectedComponents vl::DisjointSet::connected_components() {
	ConnectedComponents components;
	for (const auto &n : sets) {
		components[find(n.first)].push_back(n.first);
	}
	return components;
}
size_t vl::DisjointSet::find(const size_t n) {
	auto fnd = sets.find(n);
	if (fnd != sets.end()) {
		if (fnd->second != n) {
			fnd->second = find(fnd->second);
		}
		return fnd->second;
	}
	throw std::runtime_error("Find called for non-existing set!");
}

