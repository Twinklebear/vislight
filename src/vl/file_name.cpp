#include "vl/file_name.h"

vl::FileName::FileName(const std::string &file_name) : file_name(file_name){
	// We really just need to make sure it's normalized on Windows, Linux will
	// always give us unix-style paths
#ifdef _WIN32
	normalize();
#endif
}
vl::FileName vl::FileName::path() const {
	size_t fnd = file_name.rfind("/");
	if (fnd != std::string::npos){
		return vl::FileName{file_name.substr(0, fnd + 1)};
	}
	return vl::FileName{""};
}
std::string vl::FileName::extension() const {
	size_t fnd = file_name.rfind(".");
	if (fnd != std::string::npos){
		return file_name.substr(fnd + 1);
	}
	return "";
}
vl::FileName vl::FileName::join(const vl::FileName &other) const {
	return vl::FileName{file_name + "/" + other.file_name};
}
void vl::FileName::normalize(){
	for (auto &c : file_name){
		if (c == '\\'){
			c = '/';
		}
	}
}
const char* vl::FileName::c_str() const {
	return file_name.c_str();
}
bool vl::FileName::empty() const {
	return file_name.empty();
}
size_t vl::FileName::size() const {
	return file_name.size();
}

std::ostream& operator<<(std::ostream &os, const vl::FileName &f){
	os << f.file_name;
	return os;
}

