#include <algorithm>

#include "vl/plugin_message.h"
#include "vl/plugin.h"

vl::PluginMessage::~PluginMessage(){}

void vl::MessageDispatcher::dispatch(std::vector<std::unique_ptr<vl::Plugin>> &plugins){
	for (; !messages.empty(); messages.pop()){
		auto msg = messages.front();
		auto target = std::find_if(plugins.begin(), plugins.end(),
			[&](const std::unique_ptr<vl::Plugin> &p){
				return p->get_name() == msg->to_plugin();
			});
		// If there is no such plugin or some error occured sending the message
		// return an error message to the sender
		if (target == plugins.end() || !(*target)->send_message(msg)){
			std::cout << "error dispatching " << msg->name() << " to "
				<< msg->to_plugin() << "\n";
			// TODO: send back an error
		}
	}
}
void vl::MessageDispatcher::send(std::shared_ptr<PluginMessage> msg){
	// TODO: Eventually this would probably need some locking when
	// the dispatcher is run on a separate thread
	messages.push(msg);
}

