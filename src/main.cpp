#include "config.h"

#include <iostream>
#include <memory>
#include <limits>
#include <algorithm>
#include <vector>
#include <string>

#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <imgui.h>

#include "vl/glt/gl_core_4_5.h"
#include "vl/glt/debug.h"
#include "vl/glt/util.h"
#include "vl/glt/buffer_allocator.h"
#include "vl/glt/arcball_camera.h"
#include "vl/imgui_impl.h"
#include "vl/file_name.h"
#include "vl/file_dialog.h"
#include "vl/plugin.h"
#include "vl/plugin_message.h"

static int WIN_WIDTH = 1280;
static int WIN_HEIGHT = 720;

int app_loop(SDL_Window *win, std::vector<std::string> &argv);

int main(int argc, char **argv){
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
		std::cerr << "Failed to initialize SDL\n";
		return 1;
	}
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 0);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
#ifndef NDEBUG
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

	SDL_Window *win = SDL_CreateWindow("vislight", SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED, WIN_WIDTH, WIN_HEIGHT, SDL_WINDOW_OPENGL);
	if (!win){
		std::cout << "Failed to open SDL window: " << SDL_GetError() << "\n";
		return 1;
	}
	SDL_GLContext ctx = SDL_GL_CreateContext(win);
	if (!ctx){
		std::cout << "Failed to get OpenGL context: " << SDL_GetError() << "\n";
		return 1;
	}
	if (ogl_LoadFunctions() == ogl_LOAD_FAILED){
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Failed to Load OpenGL Functions",
				"Could not load OpenGL functions for 4.4, OpenGL 4.4 or higher is required",
				NULL);
		std::cout << "ogl load failed" << std::endl;
		SDL_GL_DeleteContext(ctx);
		SDL_DestroyWindow(win);
		SDL_Quit();
		return 1;
	}
	SDL_GL_SetSwapInterval(1);
#ifndef NDEBUG
	glt::dbg::register_debug_callback();
	glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_MARKER,
			0, GL_DEBUG_SEVERITY_NOTIFICATION, 16, "DEBUG LOG START");
#endif

	std::vector<std::string> args{argv, argv + argc};
	int rc = app_loop(win, args);

	SDL_GL_DeleteContext(ctx);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return rc;
}
int app_loop(SDL_Window *win, std::vector<std::string> &argv){
	assert(win);
	glClearColor(0.1f, 0.1f, 0.1f, 1.f);
	glClearDepth(1.f);
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_FRAMEBUFFER_SRGB);

	// Loop through and load any plugins
	std::vector<std::unique_ptr<vl::Plugin>> plugins;
	vl::MessageDispatcher msg_dispatch;
	for (auto it = argv.begin(); it != argv.end(); ++it){
		if (*it == "--plugin"){
			auto end_plugin_args = std::find_if(++it, argv.end(),
					[](const std::string &x){ return x == "--plugin"; });
			if (it == end_plugin_args){
				std::cerr << "Error parsing '--plugin': No plugin name found!\n";
				return 1;
			}
			std::vector<std::string> plugin_args{ it, end_plugin_args };
			plugins.push_back(std::make_unique<vl::Plugin>(plugin_args[0], plugin_args, msg_dispatch));
		}
	}
	// Build a list of all file types we can open, given our loaded plugins
	std::string supported_file_types;
	for (const auto &p : plugins){
		if (p->plugin_type() & vl::PluginType::LOADER){
			supported_file_types += p->fcn_table.file_extensions + ";";
		}
	}
	// Pop the last stray semicolon we pushed on if we have file loading support
	if (!supported_file_types.empty()){
		supported_file_types.pop_back();
	}

	// Get the possibly changed window size after setting up the display
	SDL_GetWindowSize(win, &WIN_WIDTH, &WIN_HEIGHT);

	ImGui_ImplSdlGL3_Init(win);
	ImGuiIO& io = ImGui::GetIO();

	std::shared_ptr<glt::BufferAllocator> allocator = std::make_shared<glt::BufferAllocator>(static_cast<size_t>(64e6));

	// Note the vec3 is padded to a vec4 size, so we need a bit more room
	auto viewing_buf = allocator->alloc(2 * sizeof(glm::mat4) + sizeof(glm::vec4), glt::BufAlignment::UNIFORM_BUFFER);
	glBindBuffer(GL_UNIFORM_BUFFER, viewing_buf.buffer);
	glBindBufferRange(GL_UNIFORM_BUFFER, 0, viewing_buf.buffer, viewing_buf.offset, viewing_buf.size);

	glm::mat4 proj_mat = glm::perspective(glt::to_radians(65), static_cast<float>(WIN_WIDTH) / WIN_HEIGHT, 0.1f, 500.f);
	glm::mat4 start_cam = glm::lookAt(glm::vec3{0, 0, 2}, glm::vec3{0, 0, 0}, glm::vec3{0, 1, 0});
	auto camera = glt::ArcBallCamera{start_cam, 400.f, 75.f, {1.f / WIN_WIDTH, 1.f / WIN_HEIGHT}};
	{
		char *buf = static_cast<char*>(viewing_buf.map(GL_UNIFORM_BUFFER,
					GL_MAP_INVALIDATE_RANGE_BIT | GL_MAP_WRITE_BIT));
		glm::mat4 *mats = reinterpret_cast<glm::mat4*>(buf);
		// While it says vec3 in the buffer with std140 layout they'll be padded to vec4's
		glm::vec4 *vecs = reinterpret_cast<glm::vec4*>(buf + 2 * sizeof(glm::mat4));
		mats[0] = proj_mat;
		mats[1] = start_cam;
		vecs[0] = glm::vec4{camera.eye_pos(), 1};

		glBindBufferRange(GL_UNIFORM_BUFFER, 0, viewing_buf.buffer, viewing_buf.offset, viewing_buf.size);
		viewing_buf.unmap(GL_UNIFORM_BUFFER);
	}

	bool quit = false;
	bool ui_hovered = false;
	bool camera_updated = false;
	bool window_resized = false;
	uint32_t prev_time = SDL_GetTicks();
	uint32_t cur_time = prev_time;
	while (!quit){
		cur_time = SDL_GetTicks();
		float elapsed = (cur_time - prev_time) / 1000.f;
		prev_time = cur_time;
		SDL_Event e;
		while (SDL_PollEvent(&e)){
			if (e.type == SDL_QUIT || (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE)){
				quit = true;
				break;
			}
			// Dropfile is handled as a file load event and we don't send the raw SDL event
			// to the plugins
			if (e.type != SDL_DROPFILE){
				if (e.type == SDL_WINDOWEVENT && e.window.event == SDL_WINDOWEVENT_RESIZED) {
					window_resized = true;
					SDL_GetWindowSize(win, &WIN_WIDTH, &WIN_HEIGHT);
				}
				if (!ui_hovered) {
					if (e.type == SDL_KEYDOWN) {
						camera_updated |= camera.keypress(e.key);
					} else if (e.type == SDL_MOUSEMOTION) {
						camera_updated |= camera.mouse_motion(e.motion, elapsed);
					} else if (e.type == SDL_MOUSEWHEEL) {
						camera_updated |= camera.mouse_scroll(e.wheel, elapsed);
					}
				}

				for (auto &p : plugins){
					if (p->fcn_table.event_fn){
						p->fcn_table.event_fn(e);
					}
				}
			}
			else {
				vl::FileName dropped_file(e.drop.file);
				for (auto &p : plugins){
					if ((p->plugin_type() & vl::PluginType::LOADER) && p->loads_file(dropped_file)){
						if (!p->fcn_table.loader_fn(dropped_file)){
							std::cout << "Plugin " << p->get_name() << " failed to load "
								<< dropped_file << "\n";
						}
					}
				}
				SDL_free(e.drop.file);
			}
			ImGui_ImplSdlGL3_ProcessEvent(&e);
		}
		msg_dispatch.dispatch(plugins);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClearColor(0.1f, 0.1f, 0.1f, 1.f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glm::mat4 view_mat = camera.transform();
		if (camera_updated || window_resized){
			char *buf = static_cast<char*>(viewing_buf.map(GL_UNIFORM_BUFFER, GL_MAP_WRITE_BIT));
			glm::mat4 *mats = reinterpret_cast<glm::mat4*>(buf);
			glm::vec3 *eye_pos = reinterpret_cast<glm::vec3*>(buf + 2 * sizeof(glm::mat4));
			if (window_resized){
				proj_mat = glm::perspective(glt::to_radians(65), static_cast<float>(WIN_WIDTH) / WIN_HEIGHT, 0.1f, 500.f);
				camera.update_screen(WIN_WIDTH, WIN_HEIGHT);
				mats[0] = proj_mat;
				window_resized = false;
			}
			mats[1] = view_mat;
			*eye_pos = camera.eye_pos();

			viewing_buf.unmap(GL_UNIFORM_BUFFER);
			camera_updated = false;
		}

		for (auto &p : plugins){
			if (p->plugin_type() & vl::PluginType::RENDER){
				p->fcn_table.render_fn(allocator, view_mat, proj_mat, elapsed);
			}
		}

		ImGui_ImplSdlGL3_NewFrame(win);
		ui_hovered = io.WantCaptureMouse || io.WantCaptureKeyboard;

		if (ImGui::BeginMainMenuBar()){
			if (ImGui::BeginMenu("File")){
				// Open a file and dispatch it to the appropriate plugins
				if (ImGui::MenuItem("Open")){
					vl::FileDialog dialog{ supported_file_types };
					vl::FileResult fresult = dialog.open_file();
					if (fresult.status != vl::FileDialogStatus::FILE_OKAY){
						if (fresult.status == vl::FileDialogStatus::FILE_ERROR){
							std::cout << "Dialog error: " << fresult.result << "\n";
						}
					}
					else {
						for (auto &p : plugins){
							if ((p->plugin_type() & vl::PluginType::LOADER) && p->loads_file(fresult.result)){
								if (!p->fcn_table.loader_fn(fresult.result)){
									std::cout << "Plugin " << p->get_name() << " failed to load "
										<< fresult.result << "\n";
								}
							}
						}
					}
				}
				ImGui::EndMenu();
			}
			ImGui::EndMainMenuBar();
		}
		ImGui::Text("Average %.3f ms/frame (%.1f FPS)", 1000.f / io.Framerate, io.Framerate);

		for (auto &p : plugins){
			if (p->plugin_type() & vl::PluginType::UI_ELEMENT){
				p->fcn_table.ui_fn();
			}
		}
		ImGui::Render();

		SDL_GL_SwapWindow(win);
	}

	ImGui_ImplSdlGL3_Shutdown();
	return 0;
}

