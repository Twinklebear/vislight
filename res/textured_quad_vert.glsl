#version 430 core

#include "view_info.glsl"

const vec2 quad_verts[4] = vec2[](
	vec2(-1, 1), vec2(-1, -1), vec2(1, 1), vec2(1, -1)
);
const vec2 quad_uvs[4] = vec2[](
	vec2(0, 1), vec2(0, 0), vec2(1, 1), vec2(1, 0)
);

// TODO: Maybe allow for instancing?
uniform mat4 model_mat;

out vec2 uv;

void main(void){
	gl_Position = proj * view * model_mat * vec4(quad_verts[gl_VertexID], 0, 1);
	uv = quad_uvs[gl_VertexID];
}

