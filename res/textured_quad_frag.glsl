#version 430 core

uniform sampler2D tex;

out vec4 color;

in vec2 uv;

void main(void){
	color = texture(tex, uv);
}

