#version 430 core

out vec4 color;

void main(void){
	if (gl_PointCoord.s <= 0.2 || gl_PointCoord.s >= 0.8
			|| gl_PointCoord.t <= 0.2 || gl_PointCoord.t >= 0.8)
	{
		color = vec4(0.1, 0.1, 0.1, 1);
	} else {
		color = vec4(1, 1, 0, 1);
	}
}

