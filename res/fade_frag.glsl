#version 430

layout(location = 0) uniform float alpha;

out vec4 color;

void main(void){
	color = vec4(vec3(0.0), alpha);
}