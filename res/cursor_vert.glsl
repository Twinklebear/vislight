#version 430 core

uniform mat4 proj;
uniform vec2 pos;

void main(void){
	gl_Position = proj * vec4(pos, 0, 1);
	gl_PointSize = 8;
}

