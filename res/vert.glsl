#version 440 core

layout(std140, binding = 0) uniform Viewing {
	mat4 proj, view;
};

layout(location = 0) in vec3 pos;

uniform samplerBuffer color_buffer;

out vec4 vcolor;

void main(void){
	gl_Position = proj * view * vec4(pos, 1);
	gl_PointSize = 2;
	vcolor = texelFetch(color_buffer, gl_VertexID);
}

