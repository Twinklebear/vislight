#include <iostream>
#include <vector>

#include <SDL.h>
#include <imgui.h>

#include <vl/glt/gl_core_4_5.h>
#include <vl/glt/buffer_allocator.h>
#include <vl/plugin.h>
#include <vl/file_name.h>

#include "../msg_test/msg_test.h"

static std::string mouse_event_str;
static std::string key_event_str;
static std::string msg_name;
static std::vector<std::string> plugin_args;
static float clear_color[3] = {0.3, 0.3, 0.3};
static vl::MessageDispatcher *msg_dispatch = nullptr;

static void ui_fn(){
	if (ImGui::Begin("Test Plugin UI")){
		ImGui::Text("This is the test plugin");

		ImGui::ColorEdit3("glClearColor", clear_color);
		ImGui::Text(mouse_event_str.c_str());
		mouse_event_str = "Mouse Event? No";

		ImGui::Text(key_event_str.c_str());
		key_event_str = "Key Event? No";

		if (!msg_name.empty()){
			ImGui::Text(msg_name.c_str());
		}

		ImGui::Text("Arguments:");
		for (auto &a : plugin_args){
			ImGui::Text("%s", a.c_str());
		}
	}
	ImGui::End();
}
static void render_fn(std::shared_ptr<glt::BufferAllocator> &allocator, const glm::mat4 &view,
	   const glm::mat4 &proj, const float elapsed)
{
	glClearColor(clear_color[0], clear_color[1], clear_color[2], 1.f);
}
static void event_fn(const SDL_Event &e){
	if (e.type == SDL_MOUSEMOTION || e.type == SDL_MOUSEBUTTONUP
		|| e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEWHEEL)
	{
		mouse_event_str = "Mouse Event? Yes!";
	} else if (e.type == SDL_KEYDOWN){
		key_event_str = "Key Event? Yes!";
	}
}
static void test_msg_recv(const std::shared_ptr<vl::PluginMessage> &msg){
	const msg_test::TestMessage *tmsg = dynamic_cast<const msg_test::TestMessage*>(msg.get());
	if (tmsg){
		std::cout << "got message from " << tmsg->from_plugin() << "\n";
		msg_name = "Got Message: " + tmsg->msg_content;
		// Now send a message back
		if (msg_dispatch){
			msg_dispatch->send(std::make_shared<msg_test::TestMessage>(tmsg->from_plugin(),
						"Hello plugin " + tmsg->from_plugin() + "!"));
		}
	}
}
static bool vislight_plugin_test_init(const std::vector<std::string> &args, vl::PluginFunctionTable &fcns,
		vl::MessageDispatcher &dispatcher)
{
	std::cout << "The real init\n";
	vl::FileName fname{"hello.cpp"};
	std::cout << "fname extension = " << fname.extension() << "\n";
	std::cout << "args = {\n";
	for (const auto &a : args){
		std::cout << "\t" << a << "\n";
	}
	std::cout << "}\n";
	plugin_args = args;
	msg_dispatch = &dispatcher;

	fcns.plugin_type = vl::PluginType::UI_ELEMENT | vl::PluginType::RENDER;
	fcns.ui_fn = ui_fn;
	fcns.render_fn = render_fn;
	fcns.event_fn = event_fn;
	fcns.message_fn = test_msg_recv;
	return true;
}
static void unload_fn(){}

// Setup externally callable loaders so the plugin loader can find our stuff
PLUGIN_LOAD_FN(test)
PLUGIN_UNLOAD_FN(test, unload_fn)

