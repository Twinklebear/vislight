#include <memory>
#include <iostream>
#include <vector>

#include <imgui.h>

#include <vl/glt/buffer_allocator.h>
#include <vl/plugin.h>

#include "msg_test.h"

using namespace msg_test;

static std::string msg_name;
static std::vector<std::string> plugin_args;

static void test_ui(){
	if (ImGui::Begin("Message Test Plugin UI")){
		ImGui::Text("This is the msg test plugin");

		if (!msg_name.empty()){
			ImGui::Text(msg_name.c_str());
		}

		ImGui::Text("Arguments:");
		for (auto &a : plugin_args){
			ImGui::Text("%s", a.c_str());
		}
	}
	ImGui::End();
}
static void test_msg_recv(const std::shared_ptr<vl::PluginMessage> &msg){
	const msg_test::TestMessage *tmsg = dynamic_cast<const msg_test::TestMessage*>(msg.get());
	if (tmsg){
		std::cout << "got message from " << tmsg->from_plugin() << "\n";
		msg_name = "Got Message: " + tmsg->msg_content;
	}
}
static bool vislight_plugin_msg_test_init(const std::vector<std::string> &args, vl::PluginFunctionTable &fcns,
		vl::MessageDispatcher &dispatcher)
{
	std::cout << "args = {\n";
	for (const auto &a : args){
		std::cout << "\t" << a << "\n";
	}
	std::cout << "}\n";
	plugin_args = args;
	dispatcher.send(std::make_shared<TestMessage>("test", "Hello plugin test!"));

	fcns.plugin_type = vl::PluginType::UI_ELEMENT;
	fcns.ui_fn = test_ui;
	fcns.message_fn = test_msg_recv;
	return true;
}
static void unload_fn(){}

// Setup externally callable loaders so the plugin loader can find our stuff
PLUGIN_LOAD_FN(msg_test)
PLUGIN_UNLOAD_FN(msg_test, unload_fn)

