#ifndef MSG_TEST_H
#define MSG_TEST_H

#include <string>

#include <vl/plugin_message.h>

namespace msg_test {

struct TestMessage : vl::PluginMessage {
	std::string from, to, type_name;
	std::string msg_content;

	TestMessage(const std::string &to, const std::string &msg_content);
	const std::string& from_plugin() const override;
	const std::string& to_plugin() const override;
	const std::string& name() const override;
};
TestMessage::TestMessage(const std::string &to, const std::string &msg_content)
	: from("msg_test"), to(to), type_name("TestMessage"), msg_content(msg_content)
{}
const std::string& TestMessage::from_plugin() const {
	return from;
}
const std::string& TestMessage::to_plugin() const {
	return to;
}
const std::string& TestMessage::name() const {
	return type_name;
}

}

#endif

