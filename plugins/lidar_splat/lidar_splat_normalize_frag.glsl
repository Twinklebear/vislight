#version 430 core

layout(binding=0) uniform sampler2D splats;
layout(binding=2) uniform sampler1D tfcn;
uniform float max_density;

in vec2 uv;

out vec4 color;

void main(void){
	float val = texture(splats, uv).r;
	if (val > 0.0) {
		color = texture(tfcn, val / max_density);
	} else {
		color = vec4(0);
	}
}

