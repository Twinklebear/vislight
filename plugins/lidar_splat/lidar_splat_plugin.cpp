#include <limits>
#include <fstream>
#include <unordered_map>
#include <cstring>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <imgui.h>

#include <vl/glt/buffer_allocator.h>
#include <vl/plugin.h>
#include <vl/glt/util.h>
#include <vl/glt/framebuffer.h>

#include "splat_scene.h"
#ifdef LIDAR_SPLAT_ENABLE_GENERICIO
#include <mpi.h>
#endif

struct UiVals {
	GLuint max_density_unif = 0;
	float max_density = 10.f;
	bool max_density_changed = false;

	GLuint splat_radius_unif = 0;
	float splat_radius = 1.f;
	bool splat_radius_changed = false;
};

static GLuint splat_shader = 0;
static GLuint normalization_shader = 0;
static GLuint fbo = 0;
static GLuint splat_target = 0;
static std::vector<SplatScene> splat_scenes;
static UiVals ui_vals;

static void ui_fn() {
	if (ImGui::Begin("Splatter Test")) {
		ImGui::Text("# of particles %llu", splat_scenes[0].num_points);
		ui_vals.max_density_changed = ImGui::SliderFloat("Max Density", &ui_vals.max_density, 0.f, 400.f);
		ui_vals.splat_radius_changed = ImGui::SliderFloat("Splat Radius", &ui_vals.splat_radius, 0.5f, 64.f);
	}
	ImGui::End();
}
static void render_fn(std::shared_ptr<glt::BufferAllocator> &allocator, const glm::mat4 &view,
		const glm::mat4 &proj, const float elapsed)
{
	const std::string res_path = glt::get_resource_path("lidar_splat");
	if (splat_shader == 0) {
		splat_shader = glt::load_program({std::make_pair(GL_VERTEX_SHADER, res_path + "lidar_splat_vert.glsl"),
				std::make_pair(GL_FRAGMENT_SHADER, res_path + "lidar_splat_frag.glsl")});
		ui_vals.splat_radius_unif = glGetUniformLocation(splat_shader, "splat_radius");
		glUseProgram(splat_shader);
		glUniform1f(ui_vals.splat_radius_unif, ui_vals.splat_radius);
	}
	if (normalization_shader == 0) {
		normalization_shader = glt::load_program({
			std::make_pair(GL_VERTEX_SHADER, res_path + "lidar_splat_normalize_vert.glsl"),
			std::make_pair(GL_FRAGMENT_SHADER, res_path + "lidar_splat_normalize_frag.glsl")
		});
		ui_vals.max_density_unif = glGetUniformLocation(normalization_shader, "max_density");
		glUseProgram(normalization_shader);
		glUniform1f(ui_vals.max_density_unif, ui_vals.max_density);
	}
	if (fbo == 0) {
		glGenFramebuffers(1, &fbo);
		glGenTextures(1, &splat_target);
		glBindTexture(GL_TEXTURE_2D, splat_target);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 1280, 720, 0, GL_RED, GL_FLOAT, nullptr);
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, splat_target, 0);
		if (!glt::check_framebuffer(fbo)) {
			throw std::runtime_error("lidar_splat: FBO invalid!");
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	glEnable(GL_PROGRAM_POINT_SIZE);
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	glBlendFunc(GL_ONE, GL_ONE);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(splat_shader);
	if (ui_vals.splat_radius_changed) {
		glUniform1f(ui_vals.splat_radius_unif, ui_vals.splat_radius);
		ui_vals.splat_radius_changed = false;
	}
	for (auto &scene : splat_scenes) {
		if (scene.vao == 0) {
			glGenVertexArrays(1, &scene.vao);
			glBindVertexArray(scene.vao);
			scene.vbo = allocator->alloc(scene.data.size() * sizeof(glm::vec3));
			{
				glm::vec3 *v = static_cast<glm::vec3*>(scene.vbo.map(GL_ARRAY_BUFFER, GL_MAP_WRITE_BIT));
				std::memcpy(v, scene.data.data(), scene.vbo.size);
				scene.vbo.unmap(GL_ARRAY_BUFFER);
			}
			glEnableVertexAttribArray(0);
			//glEnableVertexAttribArray(1);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)scene.vbo.offset);
			//glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(glm::vec3),
			//		(void*)(scene.vbo.offset + sizeof(glm::vec3)));
		}
		glBindVertexArray(scene.vao);
		glDrawArrays(GL_POINTS, 0, scene.num_points);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, splat_target);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glUseProgram(normalization_shader);
	if (ui_vals.max_density_changed) {
		ui_vals.max_density_changed = false;
		glUniform1f(ui_vals.max_density_unif, ui_vals.max_density);
	}
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
}
static bool loader_fn(const vl::FileName &file_name){
	std::cout << "splat plugin loading file: " << file_name << "\n";
#ifdef LIDAR_SPLAT_ENABLE_LAS
	if (file_name.extension() == "las" || file_name.extension() == "laz") {
		splat_scenes.push_back(SplatScene::import_lidar(file_name));
		return true;
	}
#endif
	if (file_name.extension() == "xyz") {
		splat_scenes.push_back(SplatScene::import_xyz(file_name));
		return true;
	}
#ifdef LIDAR_SPLAT_ENABLE_GENERICIO
	const bool is_hacc = file_name.file_name.find(std::string("mpicosmo")) != std::string::npos;
	if (file_name.extension() == "gio" || is_hacc) {
		splat_scenes.push_back(SplatScene::import_genericio(file_name));
		return true;
	}
#endif
	return false;
}
static bool vislight_plugin_lidar_splat_init(const std::vector<std::string> &args,
		vl::PluginFunctionTable &fcns, vl::MessageDispatcher &dispatcher)
{
#ifdef LIDAR_SPLAT_ENABLE_GENERICIO
	int flag = 0;
	MPI_Initialized(&flag);
	if (!flag) {
		// Fake some args for MPI
		int mpi_argc = 0;
		MPI_Init(&mpi_argc, nullptr);
	}
#endif

	std::cout << "splat plugin loaded, args = {\n";
	for (const auto &a : args){
		std::cout << "\t" << a << "\n";
		vl::FileName fname = a;
		if (!fname.extension().empty()) {
			loader_fn(fname);
		}
	}
	std::cout << "}\n";

#ifdef LIDAR_SPLAT_ENABLE_GENERICIO
	MPI_Finalize();
#endif

	fcns.plugin_type = vl::PluginType::UI_ELEMENT | vl::PluginType::RENDER | vl::PluginType::LOADER;
	fcns.ui_fn = ui_fn;
	fcns.render_fn = render_fn;
	fcns.loader_fn = loader_fn;
	fcns.file_extensions = "las,laz,xyz";

	return true;
}
static void unload_fn(){}

PLUGIN_LOAD_FN(lidar_splat)
PLUGIN_UNLOAD_FN(lidar_splat, unload_fn)

