# Find the GenericIO library
#
# This module defines:
# GENERICIO_FOUND, if false do not try to link against GenericIO
# GENERICIO_LIBRARY, the name of the GenericIO library to link against
# GENERICIO_INCLUDE_DIR, the GenericIO include directory
#
# You can also specify the environment variable GENERICIO_DIR or define it with
# -DGENERICIO_DIR=... to hint at the module where to search for GenercIO

find_path(GENERICIO_INCLUDE_DIR GenericIO.h
	HINTS
	$ENV{GENERICIO_DIR}
	${GENERICIO_DIR}
	PATH_SUFFIXES include/
	PATHS
	~/Library/Frameworks
	/Library/Frameworks
	/usr/local/include/
	/usr/include/
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
)

find_library(GENERICIO_LIBRARY_TMP NAMES GenericIO
	HINTS
	$ENV{GENERICIO_DIR}
	${GENERICIO_DIR}
	PATH_SUFFIXES lib/
	PATHS
	/sw
	/opt/local
	/opt/csw
	/opt
)

set(GENERICIO_FOUND FALSE)
if (GENERICIO_LIBRARY_TMP AND GENERICIO_INCLUDE_DIR)
  set(GENERICIO_LIBRARY ${GENERICIO_LIBRARY_TMP} CACHE STRING "Which GenericIO library to link against")
	set(GENERICIO_LIBRARY_TMP ${GENERICIO_LIBRARY_TMP} CACHE INTERNAL "")
	set(GENERICIO_FOUND TRUE)
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GenericIO REQUIRED_VARS GENERICIO_LIBRARY GENERICIO_INCLUDE_DIR)

