#version 430 core

#include <view_info.glsl>

uniform float splat_radius;

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 color;

out vec3 vcolor;

void main(void) {
	vcolor = color;
	gl_Position = proj * view * vec4(pos, 1);
	gl_PointSize = splat_radius * 100.0 / gl_Position.z;
}

