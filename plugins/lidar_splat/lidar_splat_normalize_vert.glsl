#version 430 core

const vec2 quad_verts[4] = vec2[](
	vec2(-1, 1), vec2(-1, -1), vec2(1, 1), vec2(1, -1)
);
const vec2 quad_uvs[4] = vec2[](
	vec2(0, 1), vec2(0, 0), vec2(1, 1), vec2(1, 0)
);

out vec2 uv;

void main(void){
	gl_Position = vec4(quad_verts[gl_VertexID], 0.5, 1);
	uv = quad_uvs[gl_VertexID];
}


