#version 430 core

in vec3 vcolor;

out float color;

void main(void) {
	vec2 coord = 2.0 * (gl_PointCoord - 0.5);
	color = clamp(1.0 - length(coord), 0.0, 1.0);
}

