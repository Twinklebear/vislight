#include <fstream>
#include <memory>
#include <unordered_map>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#ifdef LIDAR_SPLAT_ENABLE_LAS
#include <lasreader.hpp>
#include <lastransform.hpp>
#endif

#ifdef LIDAR_SPLAT_ENABLE_GENERICIO
#include <GenericIODefinitions.hpp>
#include <GenericIOReader.h>
#include <GenericIO.h>
#include <GenericIOUtilities.h>
#include <GenericIOPosixReader.h>
#include <mpi.h>
#endif

#include "splat_scene.h"

#ifdef LIDAR_SPLAT_ENABLE_LAS
SplatScene SplatScene::import_lidar(const vl::FileName &file_name) {
	LASreadOpener read_opener;
	read_opener.set_file_name(file_name.c_str());
	LASreader *reader = read_opener.open();
	std::cout << "LiDAR file '" << file_name
		<< "' contains " << reader->npoints << " points\n"
		<< "min: ( " << reader->get_min_x()
		<< ", " << reader->get_min_y()
		<< ", " << reader->get_min_z() << " )\n"
		<< "max: ( " << reader->get_max_x()
		<< ", " << reader->get_max_y()
		<< ", " << reader->get_max_z() << " )\n"
		<< "Re-centered bounds: ( 0, 0, 0 ) to "
		<< "( " << reader->get_max_x() - reader->get_min_x()
		<< ", " << reader->get_max_y() - reader->get_min_y()
		<< ", " << reader->get_max_z() - reader->get_min_z()
		<< " )\n";

	SplatScene scene;
	scene.num_points = reader->npoints;

	glm::vec3 lidar_dim = glm::vec3{reader->get_max_x() - reader->get_min_x(),
		reader->get_max_y() - reader->get_min_y(), reader->get_max_z() - reader->get_min_z()};
	scene.data.reserve(reader->npoints * 1);

	const float uint16_max = static_cast<float>(std::numeric_limits<uint16_t>::max());
	for (size_t i = 0; reader->read_point(); ++i) {
		reader->point.compute_coordinates();
		reader->point.coordinates[0] -= reader->get_min_x() + lidar_dim.x / 2.0;
		reader->point.coordinates[1] -= reader->get_min_y() + lidar_dim.y / 2.0;
		reader->point.coordinates[2] -= reader->get_min_z() + lidar_dim.z / 2.0;
		scene.data.push_back(glm::vec3{reader->point.coordinates[0], reader->point.coordinates[1],
				reader->point.coordinates[2]});
		const uint16_t *rgb = reader->point.get_rgb();
		//scene.data.push_back(glm::vec3{rgb[0] / uint16_max, rgb[1] / uint16_max, rgb[2] / uint16_max});
	}
	reader->close();
	delete reader;
	return scene;
}
#endif
SplatScene SplatScene::import_xyz(const vl::FileName &file_name) {
	std::ifstream file{file_name.file_name.c_str()};

	SplatScene scene;
	file >> scene.num_points;
	scene.data.reserve(scene.num_points * 2);

	std::string description;
	file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::getline(file, description);

	std::cout << "XYZ File '" << file_name << "'\nContains " << scene.num_points
		<< " atoms\nDescription: " << description << "\n";

	int next_atom_id = 0;
	std::unordered_map<std::string, int> atom_type_id;
	size_t atoms_read = 0;
	std::string atom_name;
	glm::vec3 pos;
	while (file >> atom_name >> pos.x >> pos.y >> pos.z){
		if (atom_type_id.find(atom_name) == atom_type_id.end()){
			atom_type_id[atom_name] = next_atom_id++;
		}
		scene.data.push_back(pos);
		++atoms_read;
	}
	if (atoms_read != scene.num_points){
		std::cout << "Error reading XYZ file, got " << atoms_read << " atoms but expected "
			<< scene.num_points << "\n";
	}
	for (const auto &t : atom_type_id){
		std::cout << "Atom type '" << t.first << "' id = " << t.second << "\n";
	}
	return scene;
}
#ifdef LIDAR_SPLAT_ENABLE_GENERICIO
SplatScene SplatScene::import_genericio(const vl::FileName &file_name) {
	std::unique_ptr<gio::GenericIOReader> reader
		= std::make_unique<gio::GenericIOPosixReader>();
	reader->SetFileName(file_name.file_name);
	reader->SetCommunicator(MPI_COMM_WORLD);
	// We'd want to use this strategy but I think the HACC GIO Silvio sent is
	// a single block per file?
	//reader->SetBlockAssignmentStrategy(gio::RCB_BLOCK_ASSIGNMENT);
	reader->OpenAndReadHeader();

	const size_t n_elems = reader->GetNumberOfElements();
	std::cout << "# of elems in the file: " << n_elems << std::endl;

	const size_t show_count = 27'000'000;

	SplatScene scene;
	scene.data.reserve(show_count);
	// Read each block until we reach the desired number of elements
	size_t num_read = 0;
	std::vector<int> assigned_blocks = *reader->GetAssignedBlocks();
	for (const auto &block : assigned_blocks) {
		reader->ClearVariables();
		const gio::RankHeader block_header = reader->GetBlockHeader(block);
		const size_t block_elems = block_header.NElems;
		std::cout << "# of elems in block " << block << " = " << block_elems << "\n";

		void *x_arr = nullptr;
		void *y_arr = nullptr;
		void *z_arr = nullptr;
		for (size_t i = 0; i < reader->GetNumberOfVariablesInFile(); ++i) {
			auto var = reader->GetFileVariableInfo(i);
			std::cout << "var[" << i << "] = " << var.Name << std::endl;
			if (var.Name == "x") {
				x_arr = gio::GenericIOUtilities::AllocateVariableArray(var, block_elems);
				reader->AddVariable(var, x_arr, gio::GenericIOBase::ValueHasExtraSpace);
				assert(var.IsFloat && var.Size == 4);
			} else if (var.Name == "y") {
				y_arr = gio::GenericIOUtilities::AllocateVariableArray(var, block_elems);
				reader->AddVariable(var, y_arr, gio::GenericIOBase::ValueHasExtraSpace);
				assert(var.IsFloat && var.Size == 4);
			} else if (var.Name == "z") {
				z_arr = gio::GenericIOUtilities::AllocateVariableArray(var, block_elems);
				reader->AddVariable(var, z_arr, gio::GenericIOBase::ValueHasExtraSpace);
				assert(var.IsFloat && var.Size == 4);
			}
		}

		double box_min[3], box_max[3];
		reader->GetBlockBounds(block, box_min, box_max);

		reader->ReadBlock(block);
		const float *x = static_cast<const float*>(x_arr);
		const float *y = static_cast<const float*>(y_arr);
		const float *z = static_cast<const float*>(z_arr);

		for (size_t i = 0; i < block_elems; ++i) {
			glm::vec3 pos(*x++, *y++, *z++);
#if 0
			// For a stress test, re-scale everything to be in a -64,64 unit cube
			for (size_t c = 0; c < 3; ++c) {
				pos[c] = 128.0 * (pos[c] - box_min[c]) / (box_max[c] - box_min[c]) - 64.0;
			}
#endif
			scene.data.emplace_back(pos);
			++num_read;
			if (num_read >= show_count) {
				break;
			}
		}

		// Go through and de-allocate the vars. TODO: allocating this stuff ourself
		// might be better like in the tutorial.
		for (size_t i = 0; i < reader->GetNumberOfVariablesInFile(); ++i) {
			auto var = reader->GetFileVariableInfo(i);
			if (var.Name == "x") {
				gio::GenericIOUtilities::DeallocateVariableArray(var, x_arr);
			} else if (var.Name == "y") {
				gio::GenericIOUtilities::DeallocateVariableArray(var, y_arr);
			} else if (var.Name == "z") {
				gio::GenericIOUtilities::DeallocateVariableArray(var, z_arr);
			}
		}
		if (num_read >= show_count) {
			break;
		}
	}
	scene.num_points = num_read;
	return scene;
}
#endif

