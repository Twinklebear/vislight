#pragma once

#include <vector>
#include <vl/file_name.h>
#include <vl/glt/buffer_allocator.h>

struct SplatScene {
	GLuint vao;
	size_t num_points;
	glt::SubBuffer vbo;
	// The cpu-side (and gpu-side) data is pos, color both as vec3fs
	std::vector<glm::vec3> data;

private:
	SplatScene() : vao(0), num_points(0) {}

public:
#ifdef LIDAR_SPLAT_ENABLE_LAS
	static SplatScene import_lidar(const vl::FileName &file_name);
#endif
	static SplatScene import_xyz(const vl::FileName &file_name);
#ifdef LIDAR_SPLAT_ENABLE_GENERICIO
	static SplatScene import_genericio(const vl::FileName &file_name);
#endif
};

