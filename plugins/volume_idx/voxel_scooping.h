#pragma once

#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <atomic>
#include <array>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include "sparse_idx_volume.h"

// The scooping page tracks which voxels in an IDX page we've visited so far
struct ScoopingPage {
	std::shared_ptr<const VolumePage> idx;
	// Tracks which voxels we've visited
	std::vector<bool> visited;

	ScoopingPage();
	ScoopingPage(std::shared_ptr<const VolumePage> &idx);
	// Check if a voxel is visited in this page, the voxel coords should
	// be in the page's space
	bool is_visited(const glm::uvec3 &voxel) const;
	// Mark a voxel as visited in this page. The voxel coords should be
	// in the page's space
	void visit(const glm::uvec3 &voxel);
};

// A vertex on the tracing graph
struct Vertex {
	glm::vec3 position;
	// The scooping distance of the cluster that made this node
	float scooping_distance;

	Vertex(const glm::vec3 pos, float scoop_dist);
};

// Mirroring the annotation edge and vertex structure
struct Edge {
	size_t id;

	std::vector<glm::vec3> edge_points;

	// TODO: Need to track the length of the edge and its attachment
	// points so we can do the short branch pruning. We also need to
	// know the scooping distance at the attachment points, so need
	// the vertices of the graph, like the annotation structure.
	float length;

	// The ids of the endpoints of the edge in the vertices list, where the
	// scooping distance is also stored.
	size_t v1, v2;
	// Edges connected to v1 and v2
	std::unordered_set<size_t> v1_edges, v2_edges;

	Edge(size_t id);
	// Append a point to this edge, updating the length
	void add_point(const glm::vec3 &p);
};

// The tracing produced by the voxel scooping process
struct NeuronTree {
	size_t next_edge_id;
	size_t next_vertex_id;

	// Map all the things...
	std::unordered_map<size_t, Edge> edges;
	std::unordered_map<size_t, Vertex> vertices;

	NeuronTree();
	size_t make_vertex(const glm::vec3 &pos, const float scoop_dist);
	Vertex& get_vertex(const size_t id);
	// This tries to mirror Pavol's Annotation API closely
	// Make a new edge starting at the vertex with id v1
	Edge& make_edge(const size_t v1);
	// End the edge at some vertex in the vertices list. Ending an edge will add
	// the endpoint to the edges list of points
	void end_edge(const size_t eid, const size_t v2);
	// Get the edge with the passed id, throws if no such edge found
	Edge& get_edge(const size_t id);
	const Edge& get_edge(const size_t id) const;
	/* Get the max length of the subtree starting at the edge
	 * the max subtree length is the distance from the edge's starting
	 * point to the furthest point away on any of its child branches.
	 */
	float total_edge_length(const size_t eid) const;
};

// A connected component of voxels in the volume we're scooping
struct Cluster {
	// The voxel coords are in the global volume space
	std::vector<glm::uvec3> voxels;
	// The node point for this cluster, for building the center line
	// and its center of mass
	glm::vec3 node, center_of_mass;
	// The length of the diagonal of the cluster's AABB
	float diagonal_length;
	// The scooping distance from the node for this cluster
	float scooping_distance;
	// The parent is the index in the vector in the stack before
	// this one that created this cluster.
	// Or if we do DFS the parent is always the top of the preceeding stack
	std::shared_ptr<Cluster> parent;
	// The edge being grown by this cluster
	size_t edge_id;

	Cluster(std::shared_ptr<Cluster> parent, size_t edge_id);
	// Compute the AABB diagonal length of the cluster and
	// its center of mass and its node point
	void compute_node();
};

struct PageIdHash {
	glm::uvec3 num_pages;

	PageIdHash(const glm::uvec3 &num_pages);
	size_t operator()(const glm::uvec3 &v) const;
};

class VoxelScooping {
	std::shared_ptr<SparseIdxVolume> volume;
	float threshold, length_scoop_ratio, min_length;
	// TODO: I would like this to be more stateful, so each tracing thread would have
	// an instance of this class. This would then let us easily store
	// and query the state and progress of the tracing
	// Voxel scooping status pages
	std::unordered_map<glm::uvec3, ScoopingPage, PageIdHash> pages;
	std::queue<std::shared_ptr<Cluster>> clusters;

	std::thread tracing_thread;
	std::atomic<bool> cancelled, complete;
	// The neuron centerline tree computed by the algorithm
	NeuronTree tree;

	std::atomic<bool> progress_tree_hazard;
	// A partial copy maintained for rendering the progress of the algorithm,
	// separate copy kept from the one the thread works on
	NeuronTree progress_tree;

public:
	VoxelScooping(std::shared_ptr<SparseIdxVolume> &volume, float threshold,
			float length_scoop_ratio = 1.5, float min_length = 20);
	~VoxelScooping();
	// Start a new voxel scooping tracing from the seed point
	void trace(const glm::uvec3 &seed);
	// Get the read side copy of the neuron tree for observing the partial results
	// TODO: return a shared_ptr then we can dump the progress tree and not copy
	// when we've finished and can just share the original tree struct.
	NeuronTree get_tree();
	// Check if the tracing process is completed
	bool tracing_complete() const;
	// Cancel the tracing
	void cancel_tracing();

private:
	// Function run by the thread to compute the tracing
	void thread_trace();
	// Get the x, y, and z voxel ranges of the neighbors for this voxel
	inline std::array<size_t, 6> get_neighbor_range(const glm::uvec3 &v) const {
		const glm::uvec3 dims = volume->get_logical_dims();
		return std::array<size_t, 6>{
			v.x > 0 ? v.x - 1 : 0,
			v.x + 1 < dims.x ? v.x + 2 : dims.x,
			v.y > 0 ? v.y - 1 : 0,
			v.y + 1 < dims.y ? v.y + 2 : dims.y,
			v.z > 0 ? v.z - 1 : 0,
			v.z + 1 < dims.z ? v.z + 2 : dims.z
		};
	}
	// Get the x, y, and z voxel ranges of the neighbors for this voxel within some radius
	std::array<size_t, 6> get_neighbor_range(const glm::uvec3 &v, const float radius) const;
	// Collect the unvisited neighbors of this voxel into the disjoint set
	void collect_unvisited_neighbors(const glm::uvec3 &v, vl::DisjointSet &disjoint);
	// Perform the scooping part of the voxel scooping process,
	// find the furthest distance to a voxel in this cluster
	// from the node and find all unvisited object voxels in this distance
	// and add them to the cluster.
	void scoop_cluster(std::shared_ptr<Cluster> &cluster);
	// Prune short branches, this operates on the finished neuron tree
	void prune_short_branches();
	// Get a voxel from the page at some location
	float get_voxel_value(const glm::uvec3 &v, std::shared_ptr<const VolumePage> &idx);
	// Union all neighbor voxels with each other
	void union_neighbors(vl::DisjointSet &disjoint) const;
	// Get or create and return the scooping page containing the voxel
	ScoopingPage& get_scooping_page(const glm::uvec3 &voxel);
};

