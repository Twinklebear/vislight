#pragma once

#include <queue>
#include <atomic>
#include <thread>
#include <mutex>
#include <map>
#include <memory>
#include <future>
#include <unordered_map>
#include <tiffio.h>

#include <glm/glm.hpp>
#include <vl/concurrent_priority_queue.h>
#include <vl/persistent_future.h>
#include "sparse_volume_cache.h"

struct TiffImage {
	TIFF *tiff;

	TiffImage(const std::string &file);
	~TiffImage();
	TiffImage(const TiffImage &) = delete;
	TiffImage& operator=(const TiffImage &) = delete;
};

class CachedTiffImage {
	std::string filename;
	size_t tiff_dir;

public:
	CachedTiffImage(const std::string &file);
	/* We expose the same interface to TIFF directories for ease of use,
	 * where the different directories can be opened as separate "images"
	 */
	CachedTiffImage(const std::string &file, size_t tiff_dir);
	std::unique_ptr<TiffImage> open();
};

struct CachedTiffPage {
	glm::uvec3 id;
	size_t last_used;
	FuturePage val;

	CachedTiffPage(const glm::uvec3 &id, const size_t time);
};

/* The SparseTiffVolume manages paging and caching from the TIFF files on
 * disk using libtiff. The desired page size can be set to mach whatever is needed,
 * e.g. GL sparse texture page size or something else. You then request pages
 * from the cache using "get_page".
 *
 * TODO: Must handle the case where threads are requesting the same pages at the
 * same time. A page can be in 3 states: requested, loading on a worker and loaded.
 */
class SparseTiffVolume : public SparseVolumeCache {
	glm::uvec3 volume_dims;
	uint32_t rows_per_strip;
	size_t tiff_strip_size;
	VolumeDType data_type;

	std::mutex slices_mutex;
	std::atomic<size_t> open_files;
	std::vector<CachedTiffImage> zslices;

	vl::ConcurrentPriorityQueue<VolumePageRequest, std::less<VolumePageRequest>> page_requests;

	std::vector<std::thread> loader_threads;
	std::atomic<bool> quit_workers;

	// Map of page ids to the page in the cache
	std::unordered_map<size_t, CachedTiffPage> cache;
	std::mutex cache_mutex;
	std::atomic<size_t> current_time;
	size_t max_cache_size, max_files_open;

	/* Delegating constructor to setup shared state between both modes.
	 * Opens on TIFF file to read the data type and X/Y dimensions
	 */
	SparseTiffVolume(TIFF *tiff, const glm::uvec3 &page_size,
			const size_t max_cache_mb, const size_t n_threads);

public:
	/* Open a list of TIFF files for the volume.
	 * max_cache_mb specifies the max MB of cached data you want to store, the default is 2048MB
	 */
	SparseTiffVolume(const std::vector<std::string> &z_slices, const glm::uvec3 &page_size,
			const size_t max_cache_mb = 2048, const size_t n_threads = 12);
	/* Open a TIFF directory for the volume.
	 * max_cache_mb specifies the max MB of cached data you want to store, the default is 2048MB
	 */
	SparseTiffVolume(const std::string &tiff_file, const glm::uvec3 &page_size,
			const size_t max_cache_mb = 2048, const size_t n_threads = 12);
	SparseTiffVolume(const SparseTiffVolume&) = delete;
	SparseTiffVolume& operator=(const SparseTiffVolume&) = delete;
	~SparseTiffVolume();
	/* Get the page. If the page is ready the shared future will have its state set,
	 * otherwise the request to load the page will be enqueue'd.
	 */
	FuturePage get_page(const VolumePageRequest &page) override;
	/* Get the page containing the voxel passed. Will throw if the voxel is out
	 * of bounds of the volume
	 */
	FuturePage get_containing_page(const glm::uvec3 &voxel,
			const float priority = 0) override;
	glm::uvec3 get_logical_dims() const override;

private:
	void worker_thread(const size_t tid);
	/* Read a box of data from the set of TIFF files, returns a pointer to the
	 * data read and will read temp data into the read buf passed.
	 */
	uint8_t* read_box(const glm::uvec3 &start, const glm::uvec3 &size, tdata_t read_buf);
	/* Look for pages in the cache we can clear out and remove them.
	 * cache lock must be held before calling this method
	 */
	void remove_old_pages();
};

