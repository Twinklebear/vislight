#include <cassert>
#include <stack>
#include <cmath>
#include <chrono>
#include <algorithm>
#include <iostream>

#include <vl/disjoint_set.h>

#include "voxel_scooping.h"

ScoopingPage::ScoopingPage() : idx(nullptr) {}
ScoopingPage::ScoopingPage(std::shared_ptr<const VolumePage> &idx)
	: idx(idx), visited(idx->actual_size.x * idx->actual_size.y * idx->actual_size.z, false)
{}
bool ScoopingPage::is_visited(const glm::uvec3 &voxel) const {
	assert(voxel.x < idx->actual_size.x && voxel.y < idx->actual_size.y && voxel.z < idx->actual_size.z);
	return visited[voxel.x + idx->actual_size.x * (voxel.y + idx->actual_size.y * voxel.z)];
}
void ScoopingPage::visit(const glm::uvec3 &voxel) {
	assert(voxel.x < idx->actual_size.x && voxel.y < idx->actual_size.y && voxel.z < idx->actual_size.z);
	visited[voxel.x + idx->actual_size.x * (voxel.y + idx->actual_size.y * voxel.z)] = true;
}

Vertex::Vertex(const glm::vec3 pos, float scoop_dist) : position(pos), scooping_distance(scoop_dist) {}

Edge::Edge(size_t id) : id(id), length(0) {}
void Edge::add_point(const glm::vec3 &p) {
	if (!edge_points.empty()) {
		length += glm::distance(p, edge_points.back());
	}
	edge_points.push_back(p);
}

NeuronTree::NeuronTree() : next_edge_id(0), next_vertex_id(0) {}
size_t NeuronTree::make_vertex(const glm::vec3 &pos, const float scoop_dist) {
	const size_t vid = next_vertex_id++;
	vertices.emplace(vid, Vertex(pos, scoop_dist));
	return vid;
}
Vertex& NeuronTree::get_vertex(const size_t id) {
	auto fnd = vertices.find(id);
#ifndef _NDEBUG
	if (fnd == vertices.end()) {
		throw std::runtime_error("Invalid vertex id requested in find");
	}
#endif
	return fnd->second;
}
Edge& NeuronTree::make_edge(const size_t v1) {
	const size_t eid = next_edge_id++;
	auto e = edges.emplace(eid, eid);
	e.first->second.add_point(get_vertex(v1).position);
	e.first->second.v1 = v1;
	return e.first->second;
}
void NeuronTree::end_edge(const size_t eid, const size_t v2) {
	Edge &e = get_edge(eid);
	e.add_point(get_vertex(v2).position);
	e.v2 = v2;
}
Edge& NeuronTree::get_edge(const size_t id) {
	auto fnd = edges.find(id);
#ifndef _NDEBUG
	if (fnd == edges.end()) {
		throw std::runtime_error("Invalid edge id requested in find");
	}
#endif
	return fnd->second;
}
const Edge& NeuronTree::get_edge(const size_t id) const {
	auto fnd = edges.find(id);
#ifndef _NDEBUG
	if (fnd == edges.end()) {
		throw std::runtime_error("Invalid edge id requested in find");
	}
#endif
	return fnd->second;
}
float NeuronTree::total_edge_length(const size_t eid) const {
	const Edge &edge = get_edge(eid);
	// Go through all subtrees leaving this edge and find the longest one
	float max_len = 0;
	for (const auto &e : edge.v2_edges) {
		max_len = std::max(max_len, total_edge_length(e));
	}
	return max_len + edge.length;
}

Cluster::Cluster(std::shared_ptr<Cluster> parent, size_t edge_id)
	: diagonal_length(0), scooping_distance(0), parent(parent), edge_id(edge_id)
{}
void Cluster::compute_node() {
	// Find the AABB of this cluster and its center of mass
	const size_t sz = voxels.size();
	glm::uvec3 min, max;
	center_of_mass = glm::vec3(0);
	for (size_t i = 0; i < sz; ++i) {
		min = glm::min(voxels[i], min);
		max = glm::max(voxels[i], max);
		center_of_mass += voxels[i];
	}
	center_of_mass /= sz;
	diagonal_length = glm::distance(glm::vec3(min), glm::vec3(max));

	const float exponent = diagonal_length <= parent->diagonal_length ?
		diagonal_length / parent->diagonal_length
		: parent->diagonal_length / diagonal_length;
	node = parent->node + std::pow(0.5f, exponent) * (center_of_mass - parent->node);

	// Compute this node's scooping distance
	scooping_distance = -1;
	for (const auto &v : voxels) {
		scooping_distance = std::max(glm::distance(node, glm::vec3(v)), scooping_distance);
	}
}

PageIdHash::PageIdHash(const glm::uvec3 &num_pages) : num_pages(num_pages) {}
size_t PageIdHash::operator()(const glm::uvec3 &v) const {
	return v.x + num_pages.x * (v.y + num_pages.y * v.z);
}

VoxelScooping::VoxelScooping(std::shared_ptr<SparseIdxVolume> &v, float threshold, float length_scoop_ratio,
		float min_length)
	: volume(v), threshold(threshold), length_scoop_ratio(length_scoop_ratio), min_length(min_length),
	pages(v->get_num_pages().x * v->get_num_pages().y * v->get_num_pages().z / 10,
			PageIdHash(v->get_num_pages())),
	cancelled(false), complete(false), progress_tree_hazard(false)
{}
VoxelScooping::~VoxelScooping() {
	cancelled = true;
	tracing_thread.join();
}
void VoxelScooping::trace(const glm::uvec3 &seed) {
	if (tracing_thread.get_id() != std::thread::id()) {
		throw std::runtime_error("VoxelScooping can only trace on seed at a time");
	}
	// The first cluster is our seed point
	clusters.push(std::make_shared<Cluster>(nullptr, tree.make_edge(tree.make_vertex(glm::vec3(seed), 0.5f)).id));
	{
		auto &seed_cluster = clusters.front();
		seed_cluster->node = seed;
		seed_cluster->center_of_mass = seed;
		seed_cluster->voxels.push_back(seed);

		pages.emplace(volume->get_containing_page_id(seed), volume->get_containing_page(seed).get());

		// Mark the start point as visited
		ScoopingPage &seed_page = pages[volume->get_containing_page_id(seed)];
		seed_page.visit(seed_page.idx->to_page(seed));
	}
	tracing_thread = std::thread([&](){ thread_trace(); });
}
NeuronTree VoxelScooping::get_tree() {
	// Spin lock if someone is getting a copy of the progress tree
	bool expected = false;
	while (!progress_tree_hazard.compare_exchange_weak(expected, true)) {
		expected = false;
	}
	// Update the partial progress tree for display
	NeuronTree copy = progress_tree;
	progress_tree_hazard = false;

	return copy;
}
bool VoxelScooping::tracing_complete() const {
	return complete.load();
}
void VoxelScooping::cancel_tracing() {
	cancelled = true;
}
void VoxelScooping::thread_trace() {
	const glm::uvec3 dims = volume->get_logical_dims();

	auto start = std::chrono::high_resolution_clock::now();
	size_t iter = 0;
	for (iter = 0; !clusters.empty() && !cancelled; ++iter) {
		const std::shared_ptr<Cluster> current = clusters.front();
		clusters.pop();

		vl::DisjointSet disjoint;
		for (const auto &v : current->voxels) {
			collect_unvisited_neighbors(v, disjoint);
		}
		// If there aren't any unvisited neighbors (disjoint is empty) then this edge
		// is ending so finish it and put it on our list.
		if (disjoint.sets.empty()) {
			// TODO: This would call end edge on the annotation.
			const size_t vid = tree.make_vertex(current->node, current->scooping_distance);
			tree.end_edge(current->edge_id, vid);
			continue;
		}

		// Union all neighboring voxels we've found together to find connected components
		union_neighbors(disjoint);

		// Now we've got the connected components for this wavefront
		auto components = disjoint.connected_components();
		const bool branching = components.size() > 1;
		const size_t branch_factor = components.size();
		std::vector<size_t> branch_edges;
		// If there are more than one component this edge is ending and we're creating new branches
		// coming off of it, so end this edge and spawn the new branches
		if (branching) {
			const size_t end_vert = tree.make_vertex(current->node, current->scooping_distance);

			tree.end_edge(current->edge_id, end_vert);
			branch_edges.reserve(branch_factor);
			for (size_t b = 0; b < branch_factor; ++b) {
				Edge &starting = tree.make_edge(end_vert);
				branch_edges.push_back(starting.id);
				starting.v1_edges.insert(current->edge_id);
			}

			Edge &ending = tree.get_edge(current->edge_id);
			for (const auto &bid : branch_edges) {
				ending.v2_edges.insert(bid);
			}
		} else {
			// Otherwise we're simply continuing this edge, so add the current cluster's
			// node to the edge.
			tree.get_edge(current->edge_id).add_point(current->node);
		}

		size_t comp_id = 0;
		for (const auto &c : components) {
			std::shared_ptr<Cluster> cluster;
			// If we're branching at this point set up the new incident edges and update the incident/exident
			// edge ids for the current cluster's edge and the newly made edge.
			if (branching) {
				cluster = std::make_shared<Cluster>(current, branch_edges[comp_id]);
				Edge &e = tree.get_edge(branch_edges[comp_id]);
				for (const auto &id : branch_edges) {
					if (id != e.id) {
						e.v1_edges.insert(id);
					}
				}
			} else {
				// If there's only one component this child is continuing the edge, otherwise
				// this edge is ending and the children are continuing new edges from the starting point
				cluster = std::make_shared<Cluster>(current, current->edge_id);
			}

			cluster->voxels.reserve(c.second.size());
			for (const auto &v : c.second) {
				const glm::uvec3 voxel(v % dims.x, (v / dims.x) % dims.y, v / (dims.x * dims.y));
				cluster->voxels.push_back(voxel);
			}
			cluster->compute_node();
			scoop_cluster(cluster);
			clusters.push(cluster);
			++comp_id;
		}

		// TODO: Better setup for sharing this progress tree with the reader. This does hurt
		// our performance somewhat, making this copy each iteration.

		// Spin lock if someone is getting a copy of the progress tree
		bool expected = false;
		while (!progress_tree_hazard.compare_exchange_weak(expected, true)) {
			expected = false;
		}
		// Update the partial progress tree for display
		progress_tree = tree;
		progress_tree_hazard = false;
	}
	auto end = std::chrono::high_resolution_clock::now();
	std::cout << "Voxel scooping took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
		<< "ms and " << iter << " iterations\n";

	start = std::chrono::high_resolution_clock::now();
	prune_short_branches();
	end = std::chrono::high_resolution_clock::now();
	std::cout << "Pruning short branches took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
		<< "ms\n";

	// Spin lock if someone is getting a copy of the progress tree
	bool expected = false;
	while (!progress_tree_hazard.compare_exchange_weak(expected, true)) {
		expected = false;
	}
	// Update the partial progress tree for display
	progress_tree = tree;
	progress_tree_hazard = false;
	complete = true;
}
std::array<size_t, 6> VoxelScooping::get_neighbor_range(const glm::uvec3 &v, const float radius) const {
	const glm::uvec3 dims = volume->get_logical_dims();
	std::array<size_t, 6> ranges;
	for (size_t i = 0; i < 3; ++i) {
		const float min = std::floor(v[i] - radius);
		if (min > 0) {
			ranges[i * 2] = min;
		} else {
			ranges[i * 2] = 0;
		}

		const float max = std::ceil(v[i] + radius);
		if (max < dims[i]) {
			ranges[i * 2 + 1] = max + 1;
		} else {
			ranges[i * 2 + 1] = dims[i];
		}
	}
	return ranges;
}
void VoxelScooping::collect_unvisited_neighbors(const glm::uvec3 &v, vl::DisjointSet &disjoint) {
	const glm::uvec3 dims = volume->get_logical_dims();
	// Go over the 3^3 neighborhood of voxels and collect any unvisited neuron ones,
	// based on our threshold. TODO: Configurable threshold? How to specify?
	const std::array<size_t, 6> ranges = get_neighbor_range(v);
	for (size_t z = ranges[4]; z < ranges[5]; ++z) {
		for (size_t y = ranges[2]; y < ranges[3]; ++y) {
			for (size_t x = ranges[0]; x < ranges[1]; ++x) {
				// Don't bother testing the voxel we're spawning from..
				if (v.x == x && v.y == y && v.z == z) {
					continue;
				}

				// Find the page containing the voxel we're looking up
				const glm::uvec3 va(x, y, z);
				ScoopingPage &page = get_scooping_page(va);

				// Check if that voxel is visited or not, if it's not been visited
				// see if it meets our threshold
				const glm::uvec3 va_page = page.idx->to_page(va);
				if (!page.is_visited(va_page)) {
					page.visit(va_page);
					const float val = get_voxel_value(va_page, page.idx);
					if (val > threshold) {
						const size_t vid = va.x + dims.x * (va.y + dims.y * va.z);
						disjoint.make_set(vid);
					}
				}
			}
		}
	}
}
void VoxelScooping::scoop_cluster(std::shared_ptr<Cluster> &cluster) {
	const glm::uvec3 dims = volume->get_logical_dims();
	// Go over the scooping neighborhood of voxels and collect any unvisited neuron ones,
	// based on our threshold.
	const std::array<size_t, 6> ranges = get_neighbor_range(glm::uvec3(cluster->node), cluster->scooping_distance);
	vl::DisjointSet disjoint;
	for (size_t z = ranges[4]; z < ranges[5]; ++z) {
		for (size_t y = ranges[2]; y < ranges[3]; ++y) {
			for (size_t x = ranges[0]; x < ranges[1]; ++x) {
				// Find the page containing the voxel we're looking up
				const glm::uvec3 va(x, y, z);
				ScoopingPage &page = get_scooping_page(va);

				// Check if that voxel is visited or not, if it's not been visited
				// see if it meets our threshold
				const glm::uvec3 va_page = page.idx->to_page(va);
				if (!page.is_visited(va_page)) {
					// Check this voxel is actually in the scooping distance, b/c we go over the box
					// with a half length of the radius.
					if (glm::distance(glm::vec3(va), cluster->node) <= cluster->scooping_distance) {
						const float val = get_voxel_value(va_page, page.idx);
						if (val > threshold) {
							const size_t vid = va.x + dims.x * (va.y + dims.y * va.z);
							disjoint.make_set(vid);
						}
					}
				}
			}
		}
	}

	// Union all neighboring voxels we've found together to find connected components
	union_neighbors(disjoint);

	// Add the cluster's existing voxels to the set so we can find which items are connected to
	// the cluster itself
	for (const auto &v : cluster->voxels) {
		const size_t vid = v.x + dims.x * (v.y + dims.y * v.z);
		disjoint.make_set(vid);
		const std::array<size_t, 6> ranges = get_neighbor_range(v);

		// Union with the scooped ones such that the cluster's voxels will be the parent of the tree
		for (size_t z = ranges[4]; z < ranges[5]; ++z) {
			for (size_t y = ranges[2]; y < ranges[3]; ++y) {
				for (size_t x = ranges[0]; x < ranges[1]; ++x) {
					// Don't bother unioning us with ourself
					if (v.x == x && v.y == y && v.z == z) {
						continue;
					}
					// Compute neighbor ID and see if they're in the object voxels set for
					// this wavefront, if so union the trees.
					const size_t neighbor_id = x + dims.x * (y + dims.y * z);
					auto fnd = disjoint.sets.find(neighbor_id);
					if (fnd != disjoint.sets.end()) {
						disjoint.set_union(fnd->first, vid);
					}
				}
			}
		}
	}
	// The connected components we can add with the scooping shouuld have one
	// of our initial voxels as a parent
	auto components = disjoint.connected_components();
	for (const auto &c : components) {
		auto fnd = std::find_if(cluster->voxels.begin(), cluster->voxels.end(),
				[&](const glm::uvec3 &v) {
					const size_t vid = v.x + dims.x * (v.y + dims.y * v.z);
					return vid == c.first;
				});
		if (fnd != cluster->voxels.end()) {
			for (const auto &v : c.second) {
				const glm::uvec3 voxel(v % dims.x, (v / dims.x) % dims.y, v / (dims.x * dims.y));
				ScoopingPage &page = get_scooping_page(voxel);

				// We're adding this voxel to our cluster so mark it as visited
				const glm::uvec3 v_page = page.idx->to_page(voxel);
				page.visit(v_page);

				cluster->voxels.push_back(voxel);
			}
		}
	}
}
void VoxelScooping::prune_short_branches() {
	std::unordered_set<size_t> to_remove;
	// TODO: picking an arbitrary min length is super nonsense. What if we computed which
	// branches were outliers in their length being much shorter than some percentile
	// and pruned those?
	for (const auto &e : tree.edges) {
		const float len = tree.total_edge_length(e.first);
		const Vertex &branch_start = tree.get_vertex(e.second.v1);
		if (len < min_length || len / branch_start.scooping_distance < length_scoop_ratio) {
			to_remove.insert(e.first);
			// We also need to remove all edges that come off this edge
			std::stack<size_t> next_child;
			next_child.push(e.first);
			while (!next_child.empty()) {
				const size_t n = next_child.top();
				next_child.pop();
				const Edge &c_edge = tree.get_edge(n);
				for (const auto &c : c_edge.v2_edges) {
					next_child.push(c);
				}
				to_remove.insert(n);
			}
		}
	}

	std::cout << "Found " << to_remove.size() << " branches to be pruned\n";
	for (const auto &id : to_remove) {
		const Edge &edge = tree.get_edge(id);
		std::cout << "Removing branch " << id << " with length "
			<< edge.length << "\n";
		// Erase the endpoint for this edge
		auto fnd = tree.vertices.find(edge.v2);
		if (fnd != tree.vertices.end()) {
			tree.vertices.erase(fnd);
		}
		tree.edges.erase(id);
	}
}
float VoxelScooping::get_voxel_value(const glm::uvec3 &v, std::shared_ptr<const VolumePage> &idx) {
	if (idx->data_type == VolumeDType::Float32) {
		const float *f = reinterpret_cast<const float*>(idx->data);
		return f[v.x + idx->actual_size.x * (v.y + idx->actual_size.y * v.z)];
	} else {
		throw std::runtime_error("Unhandled IDX DType!");
	}
}
void VoxelScooping::union_neighbors(vl::DisjointSet &disjoint) const {
	const glm::uvec3 dims = volume->get_logical_dims();
	for (const auto &elem : disjoint.sets) {
		const glm::uvec3 v(elem.first % dims.x, (elem.first / dims.x) % dims.y,
				elem.first / (dims.x * dims.y));
		const std::array<size_t, 6> ranges = get_neighbor_range(v);
		for (size_t z = ranges[4]; z < ranges[5]; ++z) {
			for (size_t y = ranges[2]; y < ranges[3]; ++y) {
				for (size_t x = ranges[0]; x < ranges[1]; ++x) {
					// Don't bother unioning us with ourself
					if (v.x == x && v.y == y && v.z == z) {
						continue;
					}
					// Compute neighbor ID and see if they're in the object voxels set for
					// this wavefront, if so union the trees.
					const size_t neighbor_id = x + dims.x * (y + dims.y * z);
					auto fnd = disjoint.sets.find(neighbor_id);
					if (fnd != disjoint.sets.end()) {
						disjoint.set_union(elem.first, fnd->first);
					}
				}
			}
		}
	}
}
ScoopingPage& VoxelScooping::get_scooping_page(const glm::uvec3 &voxel) {
	const glm::uvec3 pid = volume->get_containing_page_id(voxel);
	auto fnd = pages.find(pid);
	if (fnd == pages.end()) {
		fnd = pages.emplace(pid, volume->get_page(VolumePageRequest(pid)).get()).first;
	}
	return fnd->second;
}

