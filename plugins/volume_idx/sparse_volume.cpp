#include <iostream>
#include <cstring>
#include <algorithm>
#include <limits>

#include <glm/glm.hpp>

#include "vl/glt/util.h"
#include "vl/imgui_impl.h"

#include "sparse_volume.h"

static const std::array<float, 42> CUBE_STRIP = {
	1, 1, 0,
	0, 1, 0,
	1, 1, 1,
	0, 1, 1,
	0, 0, 1,
	0, 1, 0,
	0, 0, 0,
	1, 1, 0,
	1, 0, 0,
	1, 1, 1,
	1, 0, 1,
	0, 0, 1,
	1, 0, 0,
	0, 0, 0
};

PBOMapping::PBOMapping(size_t page_bytes) : buffer(0), page_bytes(page_bytes), mapping(nullptr), sync(0) {
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer);
	glBufferStorage(GL_PIXEL_UNPACK_BUFFER, page_bytes, NULL, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
	mapping = glMapBufferRange(GL_PIXEL_UNPACK_BUFFER, 0, page_bytes,
			GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
}
PBOMapping::~PBOMapping() {
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer);
	glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
	glDeleteBuffers(1, &buffer);
	if (sync != 0){
		glDeleteSync(sync);
	}
}

SparseVolume::SparseVolume(GLenum gl_format, std::shared_ptr<SparseVolumeCache> sparse_volume)
	: sparse_volume(sparse_volume), internal_format(gl_format), shader(0), vao(0), texture(0),
	transform_dirty(true), translation(0), scaling(1), dims(sparse_volume->get_logical_dims()),
	texture_dims(dims), pbo_workers_quit(false), next_page(0)
{
	vol_min = std::numeric_limits<float>::infinity();
	vol_max = -vol_min;
	// Find the max side of the volume and set this as 1, then find scaling for the other sides
	float max_axis = static_cast<float>(std::max(dims[0], std::max(dims[1], dims[2])));
	for (size_t i = 0; i < 3; ++i){
		vol_render_size[i] = dims[i] / max_axis;
	}
}
SparseVolume::~SparseVolume(){
	if (allocator){
		allocator->free(cube_buf);
		allocator->free(vol_props);
		glDeleteVertexArrays(1, &vao);
		glDeleteTextures(1, &texture);
		// TODO: Why does GL crash on invalid program value, when this is
		// definitely a valid fucking program?
		//glDeleteProgram(shader);
		pbo_workers_quit = true;
		for (auto &t : pbo_workers) {
			t.join();
		}
	}
}
void SparseVolume::translate(const glm::vec3 &v){
	translation += v;
	transform_dirty = true;
}
void SparseVolume::scale(const glm::vec3 &v){
	scaling *= v;
	transform_dirty = true;
}
void SparseVolume::rotate(const glm::quat &r){
	rotation = r * rotation;
	transform_dirty = true;
}
void SparseVolume::set_base_matrix(const glm::mat4 &m){
	base_matrix = m;
	transform_dirty = true;
}
void SparseVolume::render(std::shared_ptr<glt::BufferAllocator> &buf_allocator, const glm::mat4 &view_mat){
	// We need to apply the inverse volume transform to the eye to get it in the volume's space
	glm::mat4 vol_transform = glm::translate(translation) * glm::mat4_cast(rotation)
		* glm::scale(scaling * vol_render_size) * base_matrix;
	// Setup shaders, vao and volume texture
	if (!allocator){
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		allocator = buf_allocator;
		// Setup our cube tri strip to draw the bounds of the volume to raycast against
		cube_buf = buf_allocator->alloc(sizeof(float) * CUBE_STRIP.size());
		{
			float *buf = reinterpret_cast<float*>(cube_buf.map(GL_ARRAY_BUFFER,
						GL_MAP_INVALIDATE_RANGE_BIT | GL_MAP_WRITE_BIT));
			for (size_t i = 0; i < CUBE_STRIP.size(); ++i){
				buf[i] = CUBE_STRIP[i];
			}
			cube_buf.unmap(GL_ARRAY_BUFFER);
		}
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)cube_buf.offset);

		vol_props = buf_allocator->alloc(2 * sizeof(glm::mat4) + sizeof(glm::vec4) + sizeof(glm::vec2),
			glt::BufAlignment::UNIFORM_BUFFER);
		{
			char *buf = reinterpret_cast<char*>(vol_props.map(GL_UNIFORM_BUFFER,
						GL_MAP_INVALIDATE_RANGE_BIT | GL_MAP_WRITE_BIT));
			glm::mat4 *mats = reinterpret_cast<glm::mat4*>(buf);
			glm::vec4 *vecs = reinterpret_cast<glm::vec4*>(buf + 2 * sizeof(glm::mat4));
			glm::vec2 *scale_bias = reinterpret_cast<glm::vec2*>(buf + 2 * sizeof(glm::mat4) + sizeof(glm::vec4));
			mats[0] = vol_transform;
			mats[1] = glm::inverse(mats[0]);
			vecs[0] = glm::vec4{static_cast<float>(dims[0]), static_cast<float>(dims[1]),
				static_cast<float>(dims[2]), 0};
			// Set scaling and bias to scale the volume values
			*scale_bias = glm::vec2{1.f / (vol_max - vol_min), -vol_min};

			// TODO: Again how will this interact with multiple folks doing this?
			glBindBufferRange(GL_UNIFORM_BUFFER, 1, vol_props.buffer, vol_props.offset, vol_props.size);
			vol_props.unmap(GL_UNIFORM_BUFFER);
			transform_dirty = false;
		}

		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_3D, texture);

		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_SPARSE_ARB, GL_TRUE);
		glTexStorage3D(GL_TEXTURE_3D, 1, internal_format, dims[0], dims[1], dims[2]);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);

		// TODO: If drawing multiple volumes they can all share the same program
		const std::string resource_path = glt::get_resource_path();
		shader = glt::load_program({
				std::make_pair(GL_VERTEX_SHADER, resource_path + "vol_vert.glsl"),
				std::make_pair(GL_FRAGMENT_SHADER, resource_path + "vol_frag.glsl")});
		glUseProgram(shader);
		// TODO: how does this interact with having multiple volumes? should we just
		// have GL4.5 as a hard requirement for DSA? Can I get 4.5 on my laptop?
		glUniform1i(glGetUniformLocation(shader, "volume"), 1);
		glUniform1i(glGetUniformLocation(shader, "palette"), 2);

		// Spawn the PBO upload threads
		const glm::uvec3 page_size = sparse_volume->get_page_size();
		const size_t page_bytes = sizeof(float) * page_size.x * page_size.y * page_size.z;

		std::vector<std::shared_ptr<PBOMapping>> pbos;
		const size_t num_pbo_threads = 4;
		for (size_t i = 0; i < num_pbo_threads * 4; ++i) {
			pbos.push_back(std::make_shared<PBOMapping>(page_bytes));
		}
		// Enqueue all the initial pbos into the available queue to get started
		available.enqueue_bulk(pbos.data(), pbos.size());

		for (size_t i = 0; i < num_pbo_threads; ++i) {
			pbo_workers.emplace_back([&](){ pbo_worker(i); });
			glt::set_thread_name(pbo_workers.back(), "sparse_volume_pbo_worker");
		}
	}

	// See if any of the GPU-side copies have finished and thus the PBOs used are available again
	if (!copying.empty()) {
		// Partition pbos so that the completed ones are at the end.
		auto done = std::partition(copying.begin(), copying.end(),
			[](const std::shared_ptr<PBOMapping> &pbo) {
				GLenum res = glClientWaitSync(pbo->sync, GL_SYNC_FLUSH_COMMANDS_BIT, 0);
				if (res == GL_WAIT_FAILED) {
					throw std::runtime_error("PBO Upload Sync failure!");
				}
				// Check if the sync has not finished yet
				return res == GL_TIMEOUT_EXPIRED;
			});
		if (done != copying.end()) {
			// Delete the sync objects and re-enqueue the pbos as available.
			for (auto it = done; it != copying.end(); ++it) {
				glDeleteSync((*it)->sync);
				(*it)->sync = 0;
				available.enqueue(*it);
			}
			copying.erase(done, copying.end());
		}
	}

	// The PBO is written to and ready to be copied to the texture
	if (uploading.size_approx() > 0) {
		std::shared_ptr<PBOMapping> pbo = nullptr;
		if (uploading.try_dequeue(pbo)) {
			// Update min/max if this page changed them
			const float min = std::min(pbo->min, vol_min);
			const float max = std::max(pbo->max, vol_max);
			if (min != vol_min || max != vol_max) {
				vol_min = min;
				vol_max = max;
				std::cout << "Updating scale/bias to new range {" << vol_min << ", " << vol_max << "}\n";
				char *buf = reinterpret_cast<char*>(vol_props.map(GL_UNIFORM_BUFFER, GL_MAP_WRITE_BIT));
				glm::vec2 *scale_bias = reinterpret_cast<glm::vec2*>(buf + 2 * sizeof(glm::mat4) + sizeof(glm::vec4));
				*scale_bias = glm::vec2{1.f / (vol_max - vol_min), -vol_min};
				vol_props.unmap(GL_UNIFORM_BUFFER);
			}

			const glm::uvec3 page_size = sparse_volume->get_page_size();
			const glm::uvec3 offset = pbo->page_id * page_size;

			glBindTexture(GL_TEXTURE_3D, texture);
			glTexPageCommitmentARB(GL_TEXTURE_3D, 0, offset.x, offset.y, offset.z,
					pbo->actual_size.x, pbo->actual_size.y, pbo->actual_size.z, GL_TRUE);

			glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo->buffer);
			glTexSubImage3D(GL_TEXTURE_3D, 0, offset.x, offset.y, offset.z,
					pbo->actual_size.x, pbo->actual_size.y, pbo->actual_size.z,
					GL_RED, GL_FLOAT, nullptr);
			glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

			// Add this PBO to our list of ones that are being copied on the GPU to wait on it later
			pbo->sync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
			copying.push_back(pbo);
		}
	}

	const glm::uvec3 num_pages = sparse_volume->get_num_pages();
	if (next_page != num_pages) {
		// Request pages in chunks for testing
		for (size_t i = 0; i < 2 && next_page != num_pages; ++i) {
			page_requests.enqueue(sparse_volume->get_page(VolumePageRequest(next_page)));

			++next_page.x;
			if (next_page.x == num_pages.x) {
				next_page.x = 0;
				++next_page.y;
			}
			if (next_page.y == num_pages.y) {
				next_page.y = 0;
				++next_page.z;
			}
			if (next_page.z == num_pages.z) {
				std::cout << "all pages requested\n";
				next_page = num_pages;
			}
		}
	}

	if (transform_dirty){
		char *buf = reinterpret_cast<char*>(vol_props.map(GL_UNIFORM_BUFFER, GL_MAP_WRITE_BIT));
		glm::mat4 *mats = reinterpret_cast<glm::mat4*>(buf);
		mats[0] = vol_transform;
		mats[1] = glm::inverse(mats[0]);
		vol_props.unmap(GL_UNIFORM_BUFFER);
		transform_dirty = false;
	}

	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	glBindBufferRange(GL_UNIFORM_BUFFER, 1, vol_props.buffer, vol_props.offset, vol_props.size);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_3D, texture);
	glUseProgram(shader);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, CUBE_STRIP.size() / 3);

	glCullFace(GL_BACK);
	glDisable(GL_CULL_FACE);
}
void SparseVolume::pbo_worker(const size_t tid) {
	while (!pbo_workers_quit.load()) {
		// Try to get a page to upload
		FuturePage future_page;
		while (!page_requests.wait_dequeue_timed(future_page, std::chrono::nanoseconds(150))) {
			if (pbo_workers_quit.load()) {
				return;
			}
		}

		// Wait for this page to be loaded from disk
		std::shared_ptr<const VolumePage> available_page = nullptr;
		while (!future_page.try_get_for(available_page, std::chrono::nanoseconds(250))) {
			if (pbo_workers_quit.load()) {
				return;
			}
		}

		// If there's no PBO we can write too, wait for one to be available
		std::shared_ptr<PBOMapping> pbo = nullptr;
		while (!available.wait_dequeue_timed(pbo, std::chrono::nanoseconds(150))) {
			if (pbo_workers_quit.load()) {
				return;
			}
		}

		pbo->page_id = available_page->id;
		pbo->actual_size = available_page->actual_size;
		const size_t page_elems = pbo->actual_size.x * pbo->actual_size.y * pbo->actual_size.z;
		const size_t page_bytes = sizeof(float) * page_elems;

		// Copy the page from Hana to the PBO
		if (available_page->data_type == VolumeDType::Float32) {
			std::memcpy(pbo->mapping, available_page->data, page_bytes);
		} else if (available_page->data_type == VolumeDType::UInt8) {
			float *f = static_cast<float*>(pbo->mapping);
			for (size_t i = 0; i < page_elems; ++i) {
				f[i] = static_cast<float>(available_page->data[i] / 255.0);
			}
		} else if (available_page->data_type == VolumeDType::UInt16) {
			float *f = static_cast<float*>(pbo->mapping);
			uint16_t *ui = reinterpret_cast<uint16_t*>(available_page->data);
			for (size_t i = 0; i < page_elems; ++i) {
				f[i] = static_cast<float>(ui[i]) / std::numeric_limits<uint16_t>::max();
			}
		} else if (available_page->data_type == VolumeDType::Float64) {
			float *f = static_cast<float*>(pbo->mapping);
			double *d = reinterpret_cast<double*>(available_page->data);
			for (size_t i = 0; i < page_elems; ++i) {
				f[i] = static_cast<float>(d[i]);
			}
		} else {
			throw std::runtime_error("Unhandled IDX DType!");
		}

		// Compute the min/max for this page so we can update if the overall data range has changed
		float *data = static_cast<float*>(pbo->mapping);
		auto minmax = std::minmax_element(data, data + page_elems);
		pbo->min = *minmax.first;
		pbo->max = *minmax.second;
		uploading.enqueue(pbo);
	}
}

