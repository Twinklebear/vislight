#include "sparse_volume_cache.h"

VolumePage::VolumePage(const glm::uvec3 &id, const glm::uvec3 &page_size, const glm::uvec3 &actual_size,
		uint8_t *data, VolumeDType data_type)
	: id(id), page_size(page_size), actual_size(actual_size), data(data), data_type(data_type)
{}
VolumePage::~VolumePage() {
	delete[] data;
}
glm::uvec3 VolumePage::to_page(const glm::uvec3 &v) const {
	return v - id * page_size;
}
glm::uvec3 VolumePage::from_page(const glm::uvec3 &v) const {
	return v + id * page_size;
}

VolumePageRequest::VolumePageRequest() : id(glm::uvec3(0, 0, 0)), priority(-1) {}
VolumePageRequest::VolumePageRequest(const glm::uvec3 &id, const float priority) : id(id), priority(priority) {}
bool operator<(const VolumePageRequest &a, const VolumePageRequest &b) {
	return a.priority < b.priority;
}
bool operator>(const VolumePageRequest &a, const VolumePageRequest &b) {
	return a.priority > b.priority;
}

SparseVolumeCache::SparseVolumeCache(const glm::uvec3 &page_size) : page_size(page_size) {}
SparseVolumeCache::~SparseVolumeCache() {}
glm::uvec3 SparseVolumeCache::get_containing_page_id(const glm::uvec3 &voxel) const {
	return voxel / page_size;
}
glm::uvec3 SparseVolumeCache::get_num_pages() const {
	return num_pages;
}
size_t SparseVolumeCache::get_total_num_pages() const {
	const glm::uvec3 npages = get_num_pages();
	return npages.x * npages.y * npages.z;
}
glm::uvec3 SparseVolumeCache::get_page_size() const {
	return page_size;
}

bool PageIdLess::operator()(const glm::uvec3 &a, const glm::uvec3 &b) {
	return a.x < b.x || (a.x == b.x && a.y < b.y)
		|| (a.x == b.x && a.y == b.y && a.z < b.z);
}

