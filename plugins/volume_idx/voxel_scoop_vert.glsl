#version 430 core

#include "vol_global.glsl"

layout(location = 0) in vec3 pos;

void main(void){
	gl_Position = proj * view * vol_transform * vec4(pos / vol_dim, 1);
	gl_PointSize = 4;
}

