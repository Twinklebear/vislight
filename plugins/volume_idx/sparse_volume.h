#pragma once

#include <array>
#include <memory>
#include <vector>
#include <fstream>
#include <thread>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <vl/glt/gl_core_4_5.h>
#include <vl/glt/buffer_allocator.h>
#include <vl/external/moodycamel/blockingconcurrentqueue.h>

#include "sparse_volume_cache.h"

struct PBOMapping {
	GLuint buffer;
	size_t page_bytes;
	void *mapping;
	float min, max;
	// The page size is the base page size that we page in with,
	// the actual size is the actual data size of the page. These can
	// differ in the case that the volume size is not a multiple of the
	// page size.
	glm::uvec3 page_id, actual_size;
	GLsync sync;

	PBOMapping();
	PBOMapping(size_t page_bytes);
	~PBOMapping();
};

class SparseVolume {
	std::shared_ptr<SparseVolumeCache> sparse_volume;
	GLenum internal_format;

	// GL stuff
	GLuint shader, vao, texture;
	std::shared_ptr<glt::BufferAllocator> allocator;
	glt::SubBuffer cube_buf, vol_props;
	bool transform_dirty;
	// Base transformation matrix, e.g. the IDX logical to physical transform
	glm::mat4 base_matrix;
	glm::vec3 translation, scaling, vol_render_size;
	glm::quat rotation;
	glm::uvec3 dims, texture_dims;

	using PageRequestQueue = moodycamel::BlockingConcurrentQueue<FuturePage>;
	PageRequestQueue page_requests;

	using PBOQueue = moodycamel::BlockingConcurrentQueue<std::shared_ptr<PBOMapping>>;
	// The available PBO queue contains PBO's which are ready to be used by an upload thread
	PBOQueue available;
	// The uploading PBO queue contains PBO's which were written to by an upload thread
	// and have not been copied into the texture
	PBOQueue uploading;
	// The copying list are the PBO's which are being copied on the GPU side from the PBO
	// into the volume and are not finished yet. Once they're done these are enqueue'd
	// back into the available queue for re-use.
	std::vector<std::shared_ptr<PBOMapping>> copying;
	// The workers responsible for copying from the Hana cache to PBOs
	std::vector<std::thread> pbo_workers;
	std::atomic<bool> pbo_workers_quit;
	glm::uvec3 next_page;

public:
	// TODO: make private, public only temporarily Min and max values in the data set
	// TODO: These will probably need to be atomic
	float vol_min, vol_max;

	SparseVolume(GLenum gl_format, std::shared_ptr<SparseVolumeCache> sparse_volume);
	~SparseVolume();
	SparseVolume(const SparseVolume&) = delete;
	SparseVolume& operator=(const SparseVolume&) = delete;
	// Translate the volume along some vector
	void translate(const glm::vec3 &v);
	// Scale the volume by some factor
	void scale(const glm::vec3 &v);
	// Rotate the volume
	void rotate(const glm::quat &r);
	// TODO: Remove, quick hack to demo isosurface & volume rendering at the same time
	inline GLuint shader_handle(){
		return shader;
	}
	void set_base_matrix(const glm::mat4 &m);
	/* Render the volume data, this will also upload the volume
	 * if this is the first time the data is being rendered,
	 * e.g. after loading from file or changing IDX level/field
	 */
	void render(std::shared_ptr<glt::BufferAllocator> &buf_allocator, const glm::mat4 &view_mat);

private:
	void pbo_worker(const size_t tid);
};

