#pragma once

#include <queue>
#include <atomic>
#include <thread>
#include <mutex>
#include <map>
#include <memory>
#include <future>
#include <unordered_map>

#include <glm/glm.hpp>
#include <hana/idx.h>
#include <hana/idx_file.h>
#include <vl/concurrent_priority_queue.h>
#include <vl/persistent_future.h>
#include "sparse_volume_cache.h"

struct CachedIdxPage {
	glm::uvec3 id;
	size_t last_used;
	FuturePage val;

	CachedIdxPage(const glm::uvec3 &id, const size_t time);
};

/* The SparseIdxVolume manages paging and caching from the IDX file on
 * disk using Hana. The desired page size can be set to mach whatever is needed,
 * e.g. GL sparse texture page size or something else. You then request pages
 * from the cache using "get_page".
 *
 * TODO: Must handle the case where threads are requesting the same pages at the
 * same time. A page can be in 3 states: requested, loading on a worker and loaded.
 */
class SparseIdxVolume : public SparseVolumeCache {
	hana::IdxFile idx_file;
	const int idx_quality;
	const int idx_time;
	const int idx_field;
	vl::ConcurrentPriorityQueue<VolumePageRequest, std::less<VolumePageRequest>> page_requests;

	std::vector<std::thread> loader_threads;
	std::atomic<bool> quit_workers;

	// Map of page ids to the page in the cache
	std::unordered_map<size_t, CachedIdxPage> cache;
	std::mutex cache_mutex;
	std::atomic<size_t> current_time;
	size_t max_cache_size;

public:
	// max_cache_mb specifies the max MB of cached data you want to store, the default is 2048MB
	SparseIdxVolume(hana::IdxFile idx_file, const int idx_quality,
			const int idx_time, const int idx_field, const glm::uvec3 &page_size,
			const size_t max_cache_mb = 2048, const size_t n_threads = 12);
	SparseIdxVolume(const SparseIdxVolume&) = delete;
	SparseIdxVolume& operator=(const SparseIdxVolume&) = delete;
	~SparseIdxVolume();
	/* Get the page. If the page is ready the shared future will have its state set,
	 * otherwise the request to load the page will be enqueue'd.
	 */
	FuturePage get_page(const VolumePageRequest &page) override;
	/* Get the page containing the voxel passed. Will throw if the voxel is out
	 * of bounds of the volume
	 */
	FuturePage get_containing_page(const glm::uvec3 &voxel,
			const float priority = 0) override;
	glm::uvec3 get_logical_dims() const override;

private:
	void worker_thread(const size_t tid);
	/* Look for pages in the cache we can clear out and remove them.
	 * cache lock must be held before calling this method
	 */
	void remove_old_pages();
};

