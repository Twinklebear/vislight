#include <iostream>
#include <algorithm>
#include <cmath>

#include <imgui.h>
#include <SDL.h>

#include <vl/plugin.h>
#include <vl/volume.h>
#include <vl/disjoint_set.h>
#include <vl/glt/util.h>

#include "sparse_idx_volume.h"
#include "sparse_tiff_volume.h"
#include "sparse_volume.h"
#include "../transfer_function/histogram_message.h"

/* Tracks information about the IDX dataset being shown
*/
struct IdxDataset {
	vl::FileName file_name;
	int max_hz_level = -1;
	int min_hz_level = -1;
	int min_idx_time = -1;
	int max_idx_time = -1;
	std::array<int, 3> dims = {{-1, -1, -1}};
	std::array<int, 3> logical_dims = {{-1, -1, -1}};
	std::array<float, 3> render_dims = {{-1, -1, -1}};
	GLenum internal_format, format;
	// Params set by the user through the UI
	int idx_quality = -1;
	int idx_time = -1;
	int idx_field = 0;

	hana::Grid grid;
	hana::IdxFile idx_file;
	std::vector<const char*> idx_field_names;

	std::shared_ptr<std::vector<char>> data;

	/* Load the IDX data from the file
	*/
	IdxDataset(const vl::FileName &fname);
};
IdxDataset::IdxDataset(const vl::FileName &fname) : file_name(fname) {
	auto error = hana::read_idx_file(fname.file_name.c_str(), &idx_file);
	if (error.code != hana::Error::NoError){
		std::cout << "Error loading IDX file '" << fname << "' - '" << error.get_error_msg() << "'\n";
		throw std::runtime_error("Error loading IDX file " + std::string(error.get_error_msg()));
	}
	max_hz_level = idx_file.get_max_hz_level();
	min_hz_level = idx_file.get_min_hz_level();
	min_idx_time = idx_file.get_min_time_step();
	max_idx_time = idx_file.get_max_time_step();
	grid.extent = idx_file.get_logical_extent();
	idx_quality = min_hz_level;
	idx_time = min_idx_time;

	idx_field_names = std::vector<const char*>(idx_file.num_fields, nullptr);
	for (int i = 0; i < idx_file.num_fields; ++i){
		idx_field_names[i] = idx_file.fields[i].name;
	}
}

static vl::MessageDispatcher *msg_dispatch = nullptr;
static std::vector<std::pair<std::unique_ptr<IdxDataset>, std::unique_ptr<vl::Volume>>> volumes;
// Rotation velocity along x and y axes
static glm::vec2 rotation_velocity = glm::vec2(0);

const static int RENDER_VOLUME = 0;
const static int RENDER_ISOSURFACE = 1;
static int active_render_mode = RENDER_VOLUME;
static bool render_mode_change = false;
static float isovalue = std::numeric_limits<float>::infinity();
static bool isovalue_change = false;

static std::shared_ptr<SparseVolumeCache> sparse_cache = nullptr;
static std::shared_ptr<SparseVolume> sparse_volume = nullptr;

static void draw_volume_ui(std::pair<std::unique_ptr<IdxDataset>, std::unique_ptr<vl::Volume>> &vol){
	ImGui::PushID(vol.first->file_name.c_str());
	ImGui::Text("File: %s\nDimensions {%d, %d, %d}\nFull Dimensions {%d, %d, %d}",
			vol.first->file_name.c_str(), vol.first->dims[0], vol.first->dims[1], vol.first->dims[2],
			vol.first->logical_dims[0], vol.first->logical_dims[1], vol.first->logical_dims[2]);

	bool update_idx = ImGui::SliderInt("IDX HZ Level", &vol.first->idx_quality, vol.first->min_hz_level,
			vol.first->max_hz_level);
	if (vol.first->min_idx_time != vol.first->max_idx_time){
		update_idx |= ImGui::SliderInt("IDX Time Step", &vol.first->idx_time, vol.first->min_idx_time,
				vol.first->max_idx_time);
	}

	update_idx |= ImGui::Combo("Field", &vol.first->idx_field, vol.first->idx_field_names.data(),
			vol.first->idx_field_names.size());

	if (vol.first->dims[0] > 0){
		ImGui::Text("Volume Size: %fMB", vol.first->grid.data.bytes * 1e-6);
	}

	int mode_selection = active_render_mode;
	ImGui::RadioButton("Volume", &mode_selection, RENDER_VOLUME); ImGui::SameLine();
	ImGui::RadioButton("Isosurface", &mode_selection, RENDER_ISOSURFACE);
	render_mode_change |= mode_selection != active_render_mode;
	active_render_mode = mode_selection;
	if (active_render_mode == RENDER_ISOSURFACE && vol.second){
		if (isovalue > vol.second->vol_max || isovalue < vol.second->vol_min){
			isovalue = vol.second->vol_min;
		}
		isovalue_change |= ImGui::SliderFloat("Isovalue", &isovalue, vol.second->vol_min, vol.second->vol_max);
	}

	// If we haven't loaded the volume display it now, since we default to showing the first field
	if (update_idx || vol.first->dims[0] == -1){
#define INCLUSIVE_VOLUME 1
#if INCLUSIVE_VOLUME
		vol.first->grid.data.bytes = vol.first->idx_file.get_size_inclusive(vol.first->grid.extent,
				vol.first->idx_field, vol.first->idx_quality);
#else
		vol.first->grid.data.bytes = vol.first->idx_file.get_size(vol.first->grid.extent,
				vol.first->idx_field, vol.first->idx_quality);
#endif
		vol.first->data = std::make_shared<std::vector<char>>(vol.first->grid.data.bytes, 0);
		vol.first->grid.data.ptr = vol.first->data->data();

		hana::Vector3i from, to, stride;
		// TODO: Handle errors
#if INCLUSIVE_VOLUME
		vol.first->idx_file.get_grid_inclusive(vol.first->grid.extent, vol.first->idx_quality, &from, &to, &stride);
		hana::read_idx_grid_inclusive(vol.first->idx_file, vol.first->idx_field,
				vol.first->idx_time, vol.first->idx_quality, &vol.first->grid);
#else
		vol.first->idx_file.get_grid(vol.first->grid.extent, vol.first->idx_quality, from, to, stride);
		hana::read_idx_grid(vol.first->idx_file, vol.first->idx_field,
				vol.first->idx_time, vol.first->idx_quality, &vol.first->grid);
#endif
		hana::Vector3i vol_dim = (to - from) / stride + 1;
		hana::Volume logical_ext = vol.first->idx_file.get_logical_extent();
		vol.first->logical_dims[0] = logical_ext.to.x - logical_ext.from.x + 1;
		vol.first->logical_dims[1] = logical_ext.to.y - logical_ext.from.y + 1;
		vol.first->logical_dims[2] = logical_ext.to.z - logical_ext.from.z + 1;
		float max_axis = static_cast<float>(std::max(vol.first->logical_dims[0],
					std::max(vol.first->logical_dims[1], vol.first->logical_dims[2])));
		for (size_t i = 0; i < 3; ++i){
			vol.first->render_dims[i] = vol.first->logical_dims[i] / max_axis;
		}
		vol.first->dims[0] = vol_dim.x;
		vol.first->dims[1] = vol_dim.y;
		vol.first->dims[2] = vol_dim.z;

		if (vol.first->grid.type.primitive_type == hana::IdxPrimitiveType::UInt8){
			vol.first->internal_format = GL_R8;
			vol.first->format = GL_FLOAT;
			// TODO: find min/max?
			std::vector<char> converted(vol.first->data->size() * 4, 0);
			float* data_ptr = reinterpret_cast<float*>(converted.data());
			std::transform(vol.first->data->begin(), vol.first->data->end(), data_ptr,
					[](const uint8_t &u){ return static_cast<float>(u) / 255.f; });
			vol.first->data = std::make_shared<std::vector<char>>(std::move(converted));
		} else if (vol.first->grid.type.primitive_type == hana::IdxPrimitiveType::UInt16){
			vol.first->internal_format = GL_R16;
			vol.first->format = GL_UNSIGNED_SHORT;
		} else if (vol.first->grid.type.primitive_type == hana::IdxPrimitiveType::UInt32){
			vol.first->internal_format = GL_R32F;
			vol.first->format = GL_FLOAT;
			// TODO: Support this format natively instead of lazily converting to float32
			std::vector<char> converted(vol.first->data->size(), 0);
			const uint32_t *vol_ptr = reinterpret_cast<const uint32_t*>(vol.first->data->data());
			float* data_ptr = reinterpret_cast<float*>(converted.data());
			std::transform(vol_ptr, vol_ptr + vol.first->data->size() / sizeof(uint32_t), data_ptr,
					[](const uint32_t &u){ return static_cast<float>(u); });
			vol.first->data = std::make_shared<std::vector<char>>(std::move(converted));
		} else if (vol.first->grid.type.primitive_type == hana::IdxPrimitiveType::Int32){
			vol.first->internal_format = GL_R32F;
			vol.first->format = GL_FLOAT;
			// TODO: Support this format natively instead of lazily converting to float32
			std::vector<char> converted(vol.first->data->size(), 0);
			const int32_t *vol_ptr = reinterpret_cast<const int32_t*>(vol.first->data->data());
			float* data_ptr = reinterpret_cast<float*>(converted.data());
			std::transform(vol_ptr, vol_ptr + vol.first->data->size() / sizeof(int32_t), data_ptr,
					[](const int32_t &u){ return static_cast<float>(u + 3024); });
			vol.first->data = std::make_shared<std::vector<char>>(std::move(converted));
		} else if (vol.first->grid.type.primitive_type == hana::IdxPrimitiveType::Float32){
			vol.first->internal_format = GL_R32F;
			vol.first->format = GL_FLOAT;
			// TODO: Find min/max?
		} else if (vol.first->grid.type.primitive_type == hana::IdxPrimitiveType::Float64){
			vol.first->internal_format = GL_R32F;
			vol.first->format = GL_FLOAT;
			// TODO: find min/max?
			double *ptr = reinterpret_cast<double*>(vol.first->data->data());
			float* data_ptr = reinterpret_cast<float*>(vol.first->data->data());
			std::transform(ptr, ptr + vol.first->data->size() / 8, data_ptr,
					[](const double &d){ return static_cast<float>(d); });
			// Remove the extra doubles we've converted down to floats
			vol.first->data->resize(vol.first->data->size() / 2);
		} else {
			throw std::runtime_error("Unsupported format");
		}
	}
	ImGui::PopID();
}
static void ui_fn(){
	if (volumes.empty()){
		return;
	}
	// Draw info for each volume in the IDX info panel
	if (ImGui::Begin("IDX Volume Info")){
		for (auto &v : volumes){
			draw_volume_ui(v);
		}
		if (ImGui::Button("Clear Volumes")){
			volumes.clear();
		}
	}
	ImGui::End();
}
static void render_fn(std::shared_ptr<glt::BufferAllocator> &allocator, const glm::mat4 &view,
		const glm::mat4 &proj, const float elapsed)
{
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	for (auto &v : volumes){
		if (v.first->data){
			if (!v.second){
				v.second = std::make_unique<vl::Volume>(v.first->format, v.first->internal_format,
						v.first->data, v.first->dims, v.first->render_dims);
				float *mat = v.first->idx_file.logic_to_physic;
				// TODO: this is assuming it's been stored row-major (row major looks correct)
				glm::mat4 logic_mat = glm::mat4();
				for (size_t i = 0; i < 4; ++i){
					for (size_t j = 0; j < 4; ++j){
						logic_mat[i][j] = mat[i * 4 + j];
					}
				}
				std::cout << "logic_mat (w/ translation) for " << v.first->file_name
					<< " = " << glm::to_string(logic_mat) << "\n";
				// Replace the translation to center the cube at origin.
				logic_mat[3][0] = -1.5;
				logic_mat[3][1] = -0.5;
				logic_mat[3][2] = -0.5;
				v.second->set_base_matrix(logic_mat);
			} else {
				v.second->set_volume(v.first->format, v.first->internal_format,
						v.first->data, v.first->dims, v.first->render_dims);
			}
			// TODO: How to show multiple histograms on the transfer function editor? Should
			// each volume have its own transfer function? (probably yes)
			if (v.second && !v.second->histogram.empty()){
				msg_dispatch->send(std::make_shared<tfcn::HistogramMessage>("volume_idx",
							"transfer_function", v.second->histogram));
			}
			v.first->data = nullptr;
		}
		if (v.second){
			v.second->render(allocator, view);

			// We need to do it here b/c the volume shader may not have been created yet!
			glUseProgram(v.second->shader_handle());
			if (render_mode_change){
				glUniform1i(glGetUniformLocation(v.second->shader_handle(), "isosurface"), active_render_mode);
			}
			if (active_render_mode == RENDER_ISOSURFACE && (isovalue_change || render_mode_change)){
				glUniform1f(glGetUniformLocation(v.second->shader_handle(), "isovalue"), isovalue);
			}
		}
	}
	if (sparse_volume) {
		sparse_volume->render(allocator, view);
	}

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);

	isovalue_change = false;
	render_mode_change = false;
}
static bool loader_fn(const vl::FileName &file_name){
	std::cout << "volume_idx plugin loading file: " << file_name << "\n";

	// TODO: Figure out page size based on the underlying data type we're really uploading. For now
	// this is hardcoded to float32
	const GLenum internal_format = GL_R32F;
	glm::uvec3 page_size(16, 16, 8);
	if (ogl_ext_ARB_sparse_texture) {
		for (int i = 0; i < 3; ++i) {
			GLint tmp;
			glGetInternalformativ(GL_TEXTURE_3D, internal_format, GL_VIRTUAL_PAGE_SIZE_X_ARB + i,
					sizeof(tmp), &tmp);
			page_size[i] = tmp;
		}
	} else {
		std::cout << "No ARB_sparse_texture support, picking default page size for cpu paging\n";
	}
	const glm::uvec3 use_page_size = glm::uvec3(2) * page_size;
	std::cout << "min page size = " << glm::to_string(page_size)
		<< ", using page size = " << glm::to_string(use_page_size) << "\n";

	if (file_name.extension() == "tif") {
		sparse_cache = std::dynamic_pointer_cast<SparseVolumeCache>(
				std::make_shared<SparseTiffVolume>(file_name.file_name, use_page_size, 2048, 2));
	} else {
		volumes.emplace_back(std::make_unique<IdxDataset>(file_name), nullptr);
		hana::IdxFile idx_file;
		auto error = hana::read_idx_file(file_name.file_name.c_str(), &idx_file);
		if (error.code != hana::Error::NoError){
			std::cout << "Error loading IDX file '" << file_name << "' - '" << error.get_error_msg() << "'\n";
			throw std::runtime_error("Error loading IDX file " + std::string(error.get_error_msg()));
		}
#if 0
		sparse_cache = std::dynamic_pointer_cast<SparseVolumeCache>(
				std::make_shared<SparseIdxVolume>(idx_file, idx_file.get_max_hz_level(),
					0, 0, use_page_size));
#endif
	}
	if (ogl_ext_ARB_sparse_texture && sparse_cache) {
		sparse_volume = std::make_shared<SparseVolume>(internal_format, sparse_cache);
	}

	return true;
}
static bool vislight_plugin_volume_idx_init(const std::vector<std::string> &args,
		vl::PluginFunctionTable &fcns, vl::MessageDispatcher &dispatcher)
{
	std::cout << "volume_idx plugin args = {\n";
	for (const auto &a : args){
		std::cout << "\t" << a << "\n";
	}
	std::cout << "}\n";
	msg_dispatch = &dispatcher;

	fcns.plugin_type = vl::PluginType::UI_ELEMENT | vl::PluginType::RENDER
		| vl::PluginType::LOADER;
	fcns.ui_fn = ui_fn;
	fcns.render_fn = render_fn;
	fcns.loader_fn = loader_fn;
	fcns.file_extensions = "idx,tif";
	if (args.size() > 1){
#if 0
		const GLenum internal_format = GL_R32F;
		glm::uvec3 page_size(16, 16, 8);
		if (ogl_ext_ARB_sparse_texture) {
			for (int i = 0; i < 3; ++i) {
				GLint tmp;
				glGetInternalformativ(GL_TEXTURE_3D, internal_format, GL_VIRTUAL_PAGE_SIZE_X_ARB + i,
						sizeof(tmp), &tmp);
				page_size[i] = tmp;
			}
		} else {
			std::cout << "No ARB_sparse_texture support, picking default page size for cpu paging\n";
		}
		const glm::uvec3 use_page_size = glm::uvec3(2) * page_size;
		std::cout << "min page size = " << glm::to_string(page_size)
			<< ", using page size = " << glm::to_string(use_page_size) << "\n";

		std::vector<std::string> tiff_stack(args.begin() + 1, args.end());
		sparse_cache = std::dynamic_pointer_cast<SparseVolumeCache>(
				std::make_shared<SparseTiffVolume>(tiff_stack, use_page_size, 2048, 2));
		sparse_volume = std::make_shared<SparseVolume>(internal_format, sparse_cache);
#else
		for (auto it = args.begin() + 1; it != args.end(); ++it){
			loader_fn(*it);
		}
#endif
	}
	return true;
}
static void unload_fn(){
	msg_dispatch = nullptr;
	volumes.clear();
	sparse_cache = nullptr;
	sparse_volume = nullptr;
}

PLUGIN_LOAD_FN(volume_idx)
PLUGIN_UNLOAD_FN(volume_idx, unload_fn)

