#include <chrono>
#include <algorithm>
#include <cmath>
#include <iostream>

#include <glm/ext.hpp>
#include <vl/glt/util.h>

#include "sparse_idx_volume.h"

VolumeDType hana_to_dtype(const hana::IdxPrimitiveType type) {
	if (type == hana::IdxPrimitiveType::UInt8) {
		return VolumeDType::UInt8;
	} else if (type == hana::IdxPrimitiveType::UInt16) {
		return VolumeDType::UInt16;
	} else if (type == hana::IdxPrimitiveType::UInt32) {
		return VolumeDType::UInt32;
	} else if (type == hana::IdxPrimitiveType::UInt64) {
		return VolumeDType::UInt64;
	} else if (type == hana::IdxPrimitiveType::Int8) {
		return VolumeDType::Int8;
	} else if (type == hana::IdxPrimitiveType::Int16) {
		return VolumeDType::Int16;
	} else if (type == hana::IdxPrimitiveType::Int32) {
		return VolumeDType::Int32;
	} else if (type == hana::IdxPrimitiveType::Int64) {
		return VolumeDType::Int64;
	} else if (type == hana::IdxPrimitiveType::Float32) {
		return VolumeDType::Float32;
	} else if (type == hana::IdxPrimitiveType::Float64) {
		return VolumeDType::Float64;
	} else {
		return VolumeDType::Invalid;
	}
}

CachedIdxPage::CachedIdxPage(const glm::uvec3 &id, const size_t time) : id(id), last_used(time) {}

SparseIdxVolume::SparseIdxVolume(hana::IdxFile idx_file, const int idx_quality,
		const int idx_time, const int idx_field, const glm::uvec3 &page_size,
		const size_t max_cache_mb, const size_t n_threads)
	: SparseVolumeCache(page_size), idx_file(idx_file), idx_quality(idx_quality),
	idx_time(idx_time), idx_field(idx_field), quit_workers(false), current_time(0), max_cache_size(0)
{
	const size_t page_bytes = page_size.x * page_size.y * page_size.z * idx_file.fields[idx_field].type.bytes();
	max_cache_size = max_cache_mb / (page_bytes * 1e-6);
	cache.reserve(max_cache_size);

	const glm::uvec3 dims = get_logical_dims();
	num_pages = glm::uvec3(std::ceil(float(dims.x) / page_size.x),
		std::ceil(float(dims.y) / page_size.y), std::ceil(float(dims.z) / page_size.z));

	for (size_t i = 0; i < n_threads; ++i) {
		loader_threads.push_back(std::thread([&](){ worker_thread(i); }));
		glt::set_thread_name(loader_threads.back(), "sparse_idx_cache_worker");
	}
}
SparseIdxVolume::~SparseIdxVolume() {
	quit_workers = true;
	for (auto &t : loader_threads) {
		t.join();
	}
}
FuturePage SparseIdxVolume::get_page(const VolumePageRequest &page) {
	const glm::uvec3 num_pages = get_num_pages();
	if (page.id.x >= num_pages.x || page.id.y >= num_pages.y || page.id.z >= num_pages.z) {
		throw std::out_of_range("Page id out of bounds");
	}

	std::lock_guard<std::mutex> lock(cache_mutex);
	++current_time;

	const size_t pid = compute_page_id(page.id);
	auto fnd = cache.find(pid);
	if (fnd != cache.end()) {
		fnd->second.last_used = current_time.load();
		return fnd->second.val;
	}
	// Enqueue a request to load this page and return nullptr to indicate it's not avaliable currently
	auto inserted = cache.emplace(std::make_pair(pid, CachedIdxPage(page.id, current_time.load())));
	page_requests.push(page);
	return inserted.first->second.val;
}
FuturePage SparseIdxVolume::get_containing_page(const glm::uvec3 &voxel, const float priority) {
	const glm::uvec3 dims = get_logical_dims();
	if (voxel.x >= dims.x && voxel.y >= dims.y && voxel.z >= dims.z) {
		throw std::runtime_error("Voxel index out of bounds of volume!");
	}
	const glm::uvec3 page_id = voxel / page_size;
	return get_page(VolumePageRequest(page_id, priority));
}
glm::uvec3 SparseIdxVolume::get_logical_dims() const {
	hana::Volume ext = idx_file.get_logical_extent();
	return glm::uvec3(ext.to.x - ext.from.x + 1, ext.to.y - ext.from.y + 1, ext.to.z - ext.from.z + 1);
}
void SparseIdxVolume::worker_thread(const size_t) {
	VolumePageRequest to_load;
	while (!quit_workers.load()) {
		if (page_requests.try_pop_for(to_load, std::chrono::nanoseconds(250))) {
			// Load the page using Hana
			const glm::uvec3 vfrom = to_load.id * page_size;
			// TODO: Clamp to the level-specific data dimensions
			const glm::uvec3 logical_dims = get_logical_dims();
			const glm::uvec3 actual_size(vfrom.x + page_size.x > logical_dims.x ? logical_dims.x % page_size.x : page_size.x,
					vfrom.y + page_size.y > logical_dims.y ? logical_dims.y % page_size.y : page_size.y,
					vfrom.z + page_size.z > logical_dims.z ? logical_dims.z % page_size.z : page_size.z);
			const glm::uvec3 vto = vfrom + actual_size;
			hana::Vector3i from(vfrom.x, vfrom.y, vfrom.z);
			hana::Vector3i to(vto.x - 1, vto.y - 1, vto.z - 1);
			hana::Vector3i stride(1, 1, 1);
			hana::Grid grid;
			grid.extent.from = from;
			grid.extent.to = to;
			grid.data.bytes = idx_file.get_size_inclusive(grid.extent, idx_field, idx_quality);

			grid.data.ptr = new char[grid.data.bytes];
			// Hana does not support default value so zero the page before loading
			std::memset(grid.data.ptr, 0, grid.data.bytes);
			idx_file.get_grid_inclusive(grid.extent, idx_file.get_max_hz_level(), &from, &to, &stride);
			hana::read_idx_grid_inclusive(idx_file, idx_field, idx_time, idx_quality, &grid);

			auto page = std::make_shared<const VolumePage>(to_load.id, page_size, actual_size,
					reinterpret_cast<uint8_t*>(grid.data.ptr),
					hana_to_dtype(grid.type.primitive_type));

			// Set the promise for the page in the cache
			std::lock_guard<std::mutex> lock(cache_mutex);
			const size_t pid = compute_page_id(to_load.id);
			auto fnd = cache.find(pid);
			if (fnd != cache.end()) {
				fnd->second.last_used = current_time.load();
				fnd->second.val.set(page);
			} else {
				throw std::runtime_error("Bad cache eviction! Actively loading item was evicted!");
			}
			if (cache.size() >= max_cache_size) {
				remove_old_pages();
			}
		}
	}
}
void SparseIdxVolume::remove_old_pages() {
	size_t n_removed = 0;
	// Go through the cache and find all the old pages we can remove
	for (auto it = cache.begin(); it != cache.end();) {
		bool is_old = false;
		if (it->second.val.is_set()) {
			// Optional: don't evict ref'd pages by checking use_count = 2 after try_get
			is_old = current_time - it->second.last_used >= 8;
		}

		if (is_old) {
			++n_removed;
			it = cache.erase(it);
		} else {
			++it;
		}
	}
	std::cout << "removed " << n_removed << " pages\n";
}

