#include <iostream>
#include <algorithm>
#include <cstring>

#include <vl/glt/util.h>
#include "sparse_tiff_volume.h"

void tiff_warning_handler(const char *module, const char *fmt, va_list ap) {
#ifndef NDEBUG
	printf("TIFF Warning from %s:", module);
	printf(fmt, ap);
	printf("\n");
#endif
}

TiffImage::TiffImage(const std::string &file)
	: tiff(TIFFOpen(file.c_str(), "r"))
{}
TiffImage::~TiffImage() {
	TIFFClose(tiff);
}

CachedTiffImage::CachedTiffImage(const std::string &file)
	: filename(file), tiff_dir(0)
{}
CachedTiffImage::CachedTiffImage(const std::string &file, size_t tiff_dir)
	: filename(file), tiff_dir(tiff_dir)
{}
std::unique_ptr<TiffImage> CachedTiffImage::open() {
	auto tiff = std::make_unique<TiffImage>(filename);
	TIFFSetDirectory(tiff->tiff, tiff_dir);
	return tiff;
}

CachedTiffPage::CachedTiffPage(const glm::uvec3 &id, const size_t time)
	: id(id), last_used(time)
{}

SparseTiffVolume::SparseTiffVolume(TIFF *tiff, const glm::uvec3 &page_size,
			const size_t max_cache_mb, const size_t n_threads)
	: SparseVolumeCache(page_size), open_files(0), quit_workers(false), current_time(0)
{
	TIFFSetWarningHandler(tiff_warning_handler);

	TIFFGetField(tiff, TIFFTAG_IMAGEWIDTH, &volume_dims.x);
	TIFFGetField(tiff, TIFFTAG_IMAGELENGTH, &volume_dims.y);
	TIFFGetField(tiff, TIFFTAG_ROWSPERSTRIP, &rows_per_strip);
	tiff_strip_size = TIFFStripSize(tiff);

	uint16_t bits_per_sample = 0, sample_format = 0;
	TIFFGetField(tiff, TIFFTAG_BITSPERSAMPLE, &bits_per_sample);
	TIFFGetField(tiff, TIFFTAG_SAMPLEFORMAT, &sample_format);
	if (sample_format == 0 || sample_format == SAMPLEFORMAT_UINT) {
		switch (bits_per_sample) {
			case 8: data_type = VolumeDType::UInt8; break;
			case 16: data_type = VolumeDType::UInt16; break;
			case 32: data_type = VolumeDType::UInt32; break;
			case 64: data_type = VolumeDType::UInt64; break;
			default: throw std::runtime_error("Unsupported bits per sample for uint tiff data");
		}
	} else if (sample_format == SAMPLEFORMAT_INT) {
		switch (bits_per_sample) {
			case 8: data_type = VolumeDType::Int8; break;
			case 16: data_type = VolumeDType::Int16; break;
			case 32: data_type = VolumeDType::Int32; break;
			case 64: data_type = VolumeDType::Int64; break;
			default: throw std::runtime_error("Unsupported bits per sample for int tiff data");
		}
	} else if (sample_format == SAMPLEFORMAT_IEEEFP) {
		switch (bits_per_sample) {
			case 32: data_type = VolumeDType::Float32; break;
			case 64: data_type = VolumeDType::Float64; break;
			default: throw std::runtime_error("Unsupported bits per sample for float tiff data");
		}
	} else {
		throw std::runtime_error("Unsupported TIFF sample format");
	}
	TIFFClose(tiff);

	const size_t page_bytes = page_size.x * page_size.y * page_size.z * (bits_per_sample / 8);
	max_cache_size = max_cache_mb / (page_bytes * 1e-6);
	max_files_open = page_size.z * 2;
	cache.reserve(max_cache_size);

	for (size_t i = 0; i < n_threads; ++i) {
		loader_threads.push_back(std::thread([&](){ worker_thread(i); }));
		glt::set_thread_name(loader_threads.back(), "sparse_tiff_cache_worker");
	}
}
SparseTiffVolume::SparseTiffVolume(const std::vector<std::string> &z_slice_files, const glm::uvec3 &page_size,
		const size_t max_cache_mb, const size_t n_threads)
	: SparseTiffVolume(TIFFOpen(z_slice_files[0].c_str(), "r"), page_size, max_cache_mb, n_threads)
{
	volume_dims.z = z_slice_files.size();
	num_pages = glm::uvec3(std::ceil(float(volume_dims.x) / page_size.x),
		std::ceil(float(volume_dims.y) / page_size.y), std::ceil(float(volume_dims.z) / page_size.z));

	for (auto &f : z_slice_files) {
		zslices.emplace_back(f);
	}
}
/* Open a TIFF directory for the volume.
 * max_cache_mb specifies the max MB of cached data you want to store, the default is 2048MB
 */
SparseTiffVolume::SparseTiffVolume(const std::string &tiff_file, const glm::uvec3 &page_size,
		const size_t max_cache_mb, const size_t n_threads)
	: SparseTiffVolume(TIFFOpen(tiff_file.c_str(), "r"), page_size, max_cache_mb, n_threads)
{
	TIFF *tiff = TIFFOpen(tiff_file.c_str(), "r");
	volume_dims.z = 0;
	do {
		++volume_dims.z;
	} while (TIFFReadDirectory(tiff));
	TIFFClose(tiff);

	num_pages = glm::uvec3(std::ceil(float(volume_dims.x) / page_size.x),
		std::ceil(float(volume_dims.y) / page_size.y), std::ceil(float(volume_dims.z) / page_size.z));

	for (size_t i = 0; i < volume_dims.z; ++i) {
		zslices.emplace_back(tiff_file, i);
	}
}
SparseTiffVolume::~SparseTiffVolume() {
	quit_workers = true;
	for (auto &t : loader_threads) {
		t.join();
	}
}
FuturePage SparseTiffVolume::get_page(const VolumePageRequest &page) {
	const glm::uvec3 num_pages = get_num_pages();
	if (page.id.x >= num_pages.x || page.id.y >= num_pages.y || page.id.z >= num_pages.z) {
		throw std::out_of_range("Page id out of bounds");
	}

	std::lock_guard<std::mutex> lock(cache_mutex);
	++current_time;

	const size_t pid = compute_page_id(page.id);
	auto fnd = cache.find(pid);
	if (fnd != cache.end()) {
		fnd->second.last_used = current_time.load();
		return fnd->second.val;
	}
	// Enqueue a request to load this page and return nullptr to indicate it's not avaliable currently
	auto inserted = cache.emplace(std::make_pair(pid, CachedTiffPage(page.id, current_time.load())));
	page_requests.push(page);
	return inserted.first->second.val;
}
FuturePage SparseTiffVolume::get_containing_page(const glm::uvec3 &voxel, const float priority){
	const glm::uvec3 dims = get_logical_dims();
	if (voxel.x >= dims.x && voxel.y >= dims.y && voxel.z >= dims.z) {
		throw std::runtime_error("Voxel index out of bounds of volume!");
	}
	const glm::uvec3 page_id = voxel / page_size;
	return get_page(VolumePageRequest(page_id, priority));
}
glm::uvec3 SparseTiffVolume::get_logical_dims() const {
	return volume_dims;
}
void SparseTiffVolume::worker_thread(const size_t tid) {
	VolumePageRequest to_load;
	tdata_t read_buf = _TIFFmalloc(tiff_strip_size);
	while (!quit_workers.load()) {
		if (page_requests.try_pop_for(to_load, std::chrono::nanoseconds(250))) {
			const glm::uvec3 vfrom = to_load.id * page_size;
			// Clamp to the level-specific data dimensions
			const glm::uvec3 logical_dims = get_logical_dims();
			const glm::uvec3 actual_size(
					vfrom.x + page_size.x > logical_dims.x ? logical_dims.x % page_size.x : page_size.x,
					vfrom.y + page_size.y > logical_dims.y ? logical_dims.y % page_size.y : page_size.y,
					vfrom.z + page_size.z > logical_dims.z ? logical_dims.z % page_size.z : page_size.z);

			uint8_t *data = read_box(vfrom, actual_size, read_buf);
			auto page = std::make_shared<const VolumePage>(to_load.id, page_size, actual_size, data, data_type);

			// Set the promise for the page in the cache
			std::lock_guard<std::mutex> lock(cache_mutex);
			const size_t pid = compute_page_id(to_load.id);
			auto fnd = cache.find(pid);
			if (fnd != cache.end()) {
				fnd->second.last_used = current_time.load();
				fnd->second.val.set(page);
			} else {
				throw std::runtime_error("Bad cache eviction! Actively loading item was evicted!");
			}
			if (cache.size() >= max_cache_size) {
				remove_old_pages();
			}
		}
	}
	_TIFFfree(read_buf);
}
uint8_t* SparseTiffVolume::read_box(const glm::uvec3 &start, const glm::uvec3 &size, tdata_t read_buf) {
	const size_t dtype_sz = dtype_size(data_type);
	uint8_t *data = new uint8_t[size.x * size.y * size.z * dtype_sz];
	std::memset(data, 0, size.x * size.y * size.z * dtype_sz);
#if 1
	// TODO: Load the TIFF page into this page. This will be a pain b/c we will have to do a
	// slice-based read, with multiple slices (reading sub-regions from them) and across
	// multiple z-slice files. Really a hassle.
	for (size_t z = 0; z < size.z; ++z) {
		size_t y = 0;
		// It's annoying we have to open/close the files so much, but we can't do multiple reads
		// on the same handle in parallel so I think this will get us better throughput in the end
		// if we do it on-demand, and it's simpler to implement.
		auto img = zslices[z + start.z].open();
		for (size_t strip = start.y / rows_per_strip; strip < std::ceil((start.y + size.y) / rows_per_strip); ++strip) {
			const size_t bytes_read = TIFFReadEncodedStrip(img->tiff, strip, read_buf, -1);
			if (bytes_read == size_t(-1)) {
				throw std::runtime_error("TIFFReadEncodedStrip failed!");
			}

#if 1
			// Now copy the subregion in x that we want into our page
			for (size_t row = 0; row < rows_per_strip && y < size.y; ++row, ++y) {
				const size_t page_write = z * size.y * size.x * dtype_sz
					+ y * size.x * dtype_sz;
				const size_t img_read = row * volume_dims.x * dtype_sz + start.x * dtype_sz;
				std::memcpy(&data[page_write],
						reinterpret_cast<uint8_t*>(read_buf) + img_read,
						size.x * dtype_sz);
			}
#endif
		}
	}
#endif
	return data;
}
void SparseTiffVolume::remove_old_pages() {
	size_t n_removed = 0;
	// Go through the cache and find all the old pages we can remove
	for (auto it = cache.begin(); it != cache.end();) {
		bool is_old = false;
		if (it->second.val.is_set()) {
			// Optional: don't evict ref'd pages by checking use_count = 2 after try_get
			is_old = current_time - it->second.last_used >= 8;
		}

		if (is_old) {
			++n_removed;
			it = cache.erase(it);
		} else {
			++it;
		}
	}
	std::cout << "SparseTiffVolume: removed " << n_removed << " pages\n";
}

