#pragma once

#include <queue>
#include <atomic>
#include <thread>
#include <mutex>
#include <map>
#include <memory>
#include <future>
#include <unordered_map>

#include <glm/glm.hpp>
#include <vl/concurrent_priority_queue.h>
#include <vl/persistent_future.h>

/* The SparseVolumeCache is an interface to a cache which manages paging and
 * caching a volume dataset off disk.
 * The desired page size can be set to mach whatever is needed,
 * e.g. GL sparse texture page size or something else. You then request pages
 * from the cache using "get_page".
 */

enum class VolumeDType {
	Invalid,
	UInt8,
	UInt16,
	UInt32,
	UInt64,
	Int8,
	Int16,
	Int32,
	Int64,
	Float32,
	Float64
};
inline size_t dtype_size(const VolumeDType &dtype) {
	switch (dtype) {
		case VolumeDType::Invalid: return -1;
		case VolumeDType::UInt8:
		case VolumeDType::Int8: return 1;
		case VolumeDType::UInt16:
		case VolumeDType::Int16: return 2;
		case VolumeDType::UInt32:
		case VolumeDType::Int32:
		case VolumeDType::Float32: return 4;
		case VolumeDType::UInt64:
		case VolumeDType::Int64:
		case VolumeDType::Float64: return 8;
	};
	return -1;
}

// A loaded volume page
struct VolumePage {
	// The page size is the base page size that we page in with,
	// the actual size is the actual data size of the page. These can
	// differ in the case that the volume size is not a multiple of the
	// page size.
	glm::uvec3 id, page_size, actual_size;
	uint8_t *data;
	VolumeDType data_type;

	VolumePage(const glm::uvec3 &id, const glm::uvec3 &page_size,
			const glm::uvec3 &actual_size, uint8_t *data,
			VolumeDType data_type);
	~VolumePage();
	// Transform from global volume coordinates to this page's coordinates
	glm::uvec3 to_page(const glm::uvec3 &v) const;
	// Transform from this page's coordinates to global volume coordinates
	glm::uvec3 from_page(const glm::uvec3 &v) const;
	VolumePage(const VolumePage&) = delete;
	VolumePage& operator=(const VolumePage&) = delete;
};

// A page to be loaded in our page-queue
struct VolumePageRequest {
	glm::uvec3 id;
	float priority;

	VolumePageRequest();
	VolumePageRequest(const glm::uvec3 &id, const float priority = 1);
};
bool operator<(const VolumePageRequest &a, const VolumePageRequest &b);
bool operator>(const VolumePageRequest &a, const VolumePageRequest &b);

struct PageIdLess {
	bool operator()(const glm::uvec3 &a, const glm::uvec3 &b);
};

using FuturePage = vl::PersistentFuture<std::shared_ptr<const VolumePage>>;
class SparseVolumeCache {
protected:
	glm::uvec3 page_size;
	glm::uvec3 num_pages;

public:
	SparseVolumeCache(const glm::uvec3 &page_size);
	SparseVolumeCache(const SparseVolumeCache&) = delete;
	SparseVolumeCache& operator=(const SparseVolumeCache&) = delete;
	virtual ~SparseVolumeCache();
	/* Get the page. If the page is ready the shared future will have its state set,
	 * otherwise the request to load the page will be enqueue'd.
	 */
	virtual FuturePage get_page(const VolumePageRequest &page) = 0;
	/* Get the page containing the voxel passed. Will throw if the voxel is out
	 * of bounds of the volume
	 */
	virtual FuturePage get_containing_page(const glm::uvec3 &voxel,
			const float priority = 0) = 0;
	// Get the page id containing the voxel passed.
	glm::uvec3 get_containing_page_id(const glm::uvec3 &voxel) const;
	glm::uvec3 get_num_pages() const;
	size_t get_total_num_pages() const;
	glm::uvec3 get_page_size() const;
	virtual glm::uvec3 get_logical_dims() const = 0;

protected:
	inline size_t compute_page_id(const glm::uvec3 &id) const {
		return id.x + num_pages.x * (id.y + num_pages.y * size_t{id.z});
	}
};

