#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#include <unordered_map>
#include <set>

#include <imgui.h>
#include <SDL.h>
#include <glm/ext.hpp>
#include <particle_lasso/import_xyz.h>
#include <particle_lasso/import_uintah.h>

#include <vl/plugin.h>
#include <vl/glt/buffer_allocator.h>
#include <vl/glt/util.h>

/* Supported file formats:
 * pts: A simple binary format, particles are expected to have float32 x,y,z positions
 * 		and a single float32 attribute. The format is as follows:
 * 		uint64_t: number of particles
 * 		[float32...]: x,y,z for each particle
 * 		[float32...]: attribute for each particle
 */

struct Grid {
	glm::ivec3 grid_dim;
	// Each grid cell is the number of particles with and the offset
	// to the first particle in the particles array
	std::vector<glm::ivec2> grid_cells;
	glt::SubBuffer data_buf;
};

struct ParticleData {
	uint64_t n_particles;
	float radius;
	glm::vec2 attrib_range;
	glm::vec3 box_min, box_max;
	std::vector<float> particles;
	std::vector<float> binned_particles;
	glm::vec3 translation, scaling;
	glm::quat rotation;
	glt::SubBuffer cube_buf, info_buf, data_buf;
	GLuint vao, shader;
	bool update_info, particles_updated;

	ParticleData() : vao(0), shader(0), update_info(false), particles_updated(false) {}
};

struct UiInfo {
	float radius = 0.0;
	int grid[3] = {0};
	int debug_cell_id = -1;
};

static const std::array<float, 42> CUBE_STRIP = {
	1, 1, -1,
	-1, 1, -1,
	1, 1, 1,
	-1, 1, 1,
	-1, -1, 1,
	-1, 1, -1,
	-1, -1, -1,
	1, 1, -1,
	1, -1, -1,
	1, 1, 1,
	1, -1, 1,
	-1, -1, 1,
	1, -1, -1,
	-1, -1, -1
};

static ParticleData particle_data;
static Grid particle_grid;
static UiInfo ui_info;
// Rotation velocity along x and y axes
static glm::vec2 rotation_velocity = glm::vec2(0);
static std::string uintah_attribute;

static void grid_particles();

static void ui_fn(){
	if (ImGui::Begin("Particle Info")){
		if (particle_data.n_particles > 0){
			ImGui::Text("Number of Particles: %lu", particle_data.n_particles);
		}
		if (ImGui::InputFloat("Radius", &ui_info.radius)){
			ui_info.radius = std::max(ui_info.radius, 0.f);
		}
		ImGui::InputInt3("Grid Dimensions", ui_info.grid);

		if (ImGui::Button("Re-grid")){
			grid_particles();
		}
	}
	ImGui::End();
}
static void render_fn(std::shared_ptr<glt::BufferAllocator> &allocator, const glm::mat4 &view,
		const glm::mat4 &proj, const float elapsed)
{
	// Check if we've uploaded our cube and do so if not
	if (particle_data.cube_buf.size == 0){
		glGenVertexArrays(1, &particle_data.vao);
		glBindVertexArray(particle_data.vao);
		// Setup our cube tri strip to draw the bounds of the volume to raycast against
		particle_data.cube_buf = allocator->alloc(sizeof(float) * CUBE_STRIP.size());
		{
			float *buf = reinterpret_cast<float*>(particle_data.cube_buf.map(GL_ARRAY_BUFFER,
						GL_MAP_INVALIDATE_RANGE_BIT | GL_MAP_WRITE_BIT));
			for (size_t i = 0; i < CUBE_STRIP.size(); ++i){
				buf[i] = CUBE_STRIP[i];
			}
			particle_data.cube_buf.unmap(GL_ARRAY_BUFFER);
		}
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)particle_data.cube_buf.offset);
		glBindVertexArray(0);

		const std::string resource_path = glt::get_resource_path("particles");
		particle_data.shader = glt::load_program({
				std::make_pair(GL_VERTEX_SHADER, resource_path + "particle_vert.glsl"),
				std::make_pair(GL_FRAGMENT_SHADER, resource_path + "particle_frag.glsl")});
		GLint palette_unif_loc = glGetUniformLocation(particle_data.shader, "palette");
		glUseProgram(particle_data.shader);
		glUniform1i(palette_unif_loc, 2);

		// Setup initial translation
		particle_data.translation = glm::vec3(0.f, 0.f, 0.f);
		// Setup the particle info ubo. Note that box_min/box_max are padded after attrib_range by an extra
		// 4 bytes
		particle_data.info_buf = allocator->alloc(2 * sizeof(glm::mat4) + sizeof(int) + 3 * sizeof(float)
				+ 2 * sizeof(glm::vec4), glt::BufAlignment::UNIFORM_BUFFER);
		glBindBufferRange(GL_UNIFORM_BUFFER, 2, particle_data.info_buf.buffer, particle_data.info_buf.offset,
				particle_data.info_buf.size);
		particle_data.update_info = true;
	}

	// Move the particles if desired
	if (rotation_velocity != glm::vec2(0.f)){
		particle_data.rotation = glm::quat_cast(glm::rotate(rotation_velocity.x * elapsed, glm::vec3(1, 0, 0)))
			* particle_data.rotation;
		particle_data.rotation = glm::quat_cast(glm::rotate(rotation_velocity.y * elapsed, glm::vec3(0, 1, 0)))
			* particle_data.rotation;
		particle_data.update_info = true;
	}

	// If we've got new particle data to upload, do so now
	if (particle_data.particles_updated && !particle_data.binned_particles.empty()){
		// Make sure we have enough room in a UBO for the particle data
		const size_t data_size = sizeof(float) * particle_data.binned_particles.size();
		assert(data_size <= 1.6e7);
		// Make sure we have enough room to hold everything
		if (particle_data.data_buf.size == 0){
			particle_data.data_buf = allocator->alloc(data_size, glt::BufAlignment::SHADER_STORAGE_BUFFER);
		} else if (particle_data.data_buf.size < data_size){
			allocator->realloc(particle_data.data_buf, data_size, glt::BufAlignment::SHADER_STORAGE_BUFFER);
		}
		// Make sure we have enough room for the grid info
		const size_t grid_data_size = sizeof(glm::ivec4) + sizeof(glm::ivec2) * particle_grid.grid_cells.size();
		if (particle_grid.data_buf.size == 0){
			particle_grid.data_buf = allocator->alloc(grid_data_size, glt::BufAlignment::SHADER_STORAGE_BUFFER);
		} else if (particle_grid.data_buf.size < grid_data_size){
			allocator->realloc(particle_grid.data_buf, grid_data_size, glt::BufAlignment::SHADER_STORAGE_BUFFER);
		}
		// Map the buffers and upload our data
		auto data = glt::map_multiple({particle_data.data_buf, particle_grid.data_buf}, GL_MAP_WRITE_BIT);
		std::memcpy(data[0], particle_data.binned_particles.data(),
				sizeof(float) * particle_data.binned_particles.size());
		{
			glm::ivec3 *dims = reinterpret_cast<glm::ivec3*>(data[1]);
			*dims = particle_grid.grid_dim;
			// Note that the ivec3 is padded to a vec4
			std::memcpy(data[1] + sizeof(glm::ivec4), particle_grid.grid_cells.data(),
					particle_grid.grid_cells.size() * sizeof(glm::ivec2));
		}

		glt::unmap_multiple({particle_data.data_buf, particle_grid.data_buf});

		glBindBufferRange(GL_SHADER_STORAGE_BUFFER, 3, particle_data.data_buf.buffer, particle_data.data_buf.offset,
				particle_data.data_buf.size);
		glBindBufferRange(GL_SHADER_STORAGE_BUFFER, 4, particle_grid.data_buf.buffer, particle_grid.data_buf.offset,
				particle_grid.data_buf.size);

		particle_data.particles_updated = false;
	}
	if (particle_data.update_info){
		char *buf = reinterpret_cast<char*>(particle_data.info_buf.map(GL_UNIFORM_BUFFER, GL_MAP_WRITE_BIT));
		glm::mat4 *mats = reinterpret_cast<glm::mat4*>(buf);
		int *n_particles = reinterpret_cast<int*>(buf + 2 * sizeof(glm::mat4));
		float *f = reinterpret_cast<float*>(buf + 2 * sizeof(glm::mat4) + sizeof(int));
		glm::vec4 *box_vecs = reinterpret_cast<glm::vec4*>(buf + 2 * sizeof(glm::mat4) +
				sizeof(int) + 3 * sizeof(float));
		mats[0] = glm::translate(particle_data.translation) * glm::mat4_cast(particle_data.rotation)
			* glm::scale(particle_data.scaling);
		mats[1] = glm::inverse(mats[0]);
		*n_particles = particle_data.n_particles;
		if (particle_data.radius > 0.0){
			f[0] = particle_data.radius;
		}
		f[1] = particle_data.attrib_range.y - particle_data.attrib_range.x;
		box_vecs[0] = glm::vec4(particle_data.box_min - particle_data.radius, 0);
		box_vecs[1] = glm::vec4(particle_data.box_max + particle_data.radius, 0);

		particle_data.info_buf.unmap(GL_UNIFORM_BUFFER);
		particle_data.update_info = false;
	}
	if (particle_data.n_particles > 0){
		// Now draw the cube and raycast the particle sphere glyphs within
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUseProgram(particle_data.shader);
		// Lazy debugging
		//glUniform1i(glGetUniformLocation(particle_data.shader, "debug_cell_id"), ui_info.debug_cell_id);
		glBindVertexArray(particle_data.vao);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, CUBE_STRIP.size() / 3);

		glCullFace(GL_BACK);
		glDisable(GL_CULL_FACE);
	}
}
void grid_particles(){
	if (ui_info.radius != 0.0){
		particle_data.radius = ui_info.radius;
	} else {
		particle_data.radius = 1.0;
		ui_info.radius = particle_data.radius;
	}
	if (ui_info.grid[0] == 0 || ui_info.grid[1] == 0 || ui_info.grid[2] == 0){
		particle_grid.grid_dim = glm::vec3(32, 32, 32);
		ui_info.grid[0] = particle_grid.grid_dim.x;
		ui_info.grid[1] = particle_grid.grid_dim.y;
		ui_info.grid[2] = particle_grid.grid_dim.z;
	} else {
		particle_grid.grid_dim = glm::vec3(ui_info.grid[0], ui_info.grid[1], ui_info.grid[2]);
	}

	// extend grid to account for particles within radius to the facets of grid
	const glm::vec3 box_min = particle_data.box_min - particle_data.radius;
	const glm::vec3 box_max = particle_data.box_max + particle_data.radius;

	// Construct a grid on the particles
	std::vector<std::set<size_t>> binned_particles(particle_grid.grid_dim.x * particle_grid.grid_dim.y
			* particle_grid.grid_dim.z, std::set<size_t>());
	for (size_t i = 0; i < particle_data.n_particles; ++i){
		const glm::vec3 p(particle_data.particles[i * 4], particle_data.particles[i * 4 + 1],
				particle_data.particles[i * 4 + 2]);
		glm::vec3 grid_p = p + particle_data.radius * glm::vec3(-1);
		grid_p = (grid_p - box_min) / (box_max - box_min);
		const glm::ivec3 range_min = glm::ivec3(grid_p * glm::vec3(particle_grid.grid_dim));

		grid_p = p + particle_data.radius * glm::vec3(1);
		grid_p = (grid_p - box_min) / (box_max - box_min);
		const glm::ivec3 range_max = glm::ivec3(grid_p * glm::vec3(particle_grid.grid_dim));

		// We need to find all bins the particle overlaps with its radius. Note that this also contains
		// the voxel that the particle is located in (0, 0, 0)
		for (int x = range_min.x; x <= range_max.x; ++x){
			// Don't add to bins that are not in the grid
			if (x < 0 || x >= particle_grid.grid_dim.x){
				continue;
			}
			for (int y = range_min.y; y <= range_max.y; ++y){
				if (y < 0 || y >= particle_grid.grid_dim.y){
					continue;
				}
				for (int z = range_min.z; z <= range_max.z; ++z){
					if (z < 0 || z >= particle_grid.grid_dim.z){
						continue;
					}
					// TODO: We need to add the particle to the neighboring cells that also contain it
					size_t cell_id = x + y * particle_grid.grid_dim.x
						+ z * particle_grid.grid_dim.x * particle_grid.grid_dim.y;
					binned_particles[cell_id].insert(i);
				}
			}
		}
	}
	// Re-sort the particles to be in grid order. TODO Must be a cleaner way to do this as well,
	// this is kind of nasty
	particle_data.binned_particles = std::vector<float>();
	particle_grid.grid_cells.resize(binned_particles.size());
	size_t offset = 0;
	for (size_t i = 0; i < binned_particles.size(); ++i){
		for (const auto &pid : binned_particles[i]){
			for (size_t k = 0; k < 4; ++k){
				particle_data.binned_particles.push_back(particle_data.particles[pid * 4 + k]);
			}
		}
		particle_grid.grid_cells[i] = glm::ivec2(binned_particles[i].size(), offset);
		offset += binned_particles[i].size();
	}
	std::cout << "Uploading " << particle_data.binned_particles.size() / 4 << " particles\n";
	// Setup scaling for the cube
	const glm::vec3 diagonal = box_max - box_min;
	std::cout << "Data box [" << glm::to_string(box_min)
		<< ", " << glm::to_string(box_max) << "]\n";
	const float max_axis = std::max(diagonal.x, std::max(diagonal.y, diagonal.z));
	glm::vec3 cube_scale = diagonal / max_axis;
	std::cout << "cube_scale = " << glm::to_string(cube_scale) << "\n";
	// We also scale down by 0.3 so it fits nicely in view in VR
	particle_data.scaling = cube_scale * 0.4f;

	particle_data.particles_updated = true;
	particle_data.update_info = true;
}
static bool loader_fn(const vl::FileName &file_name) {
	if (file_name.extension() == "xyz"){
		ParticleModel model;
		import_xyz(FileName(file_name.file_name), model);

		auto *pos = dynamic_cast<DataT<float>*>(model["positions"].get());
		auto *atom_type = dynamic_cast<DataT<int>*>(model["atom_type"].get());
		particle_data.n_particles = atom_type->data.size();
		particle_data.particles.resize(particle_data.n_particles * 4);
		for (size_t i = 0; i < particle_data.n_particles; ++i){
			for (size_t j = 0; j < 3; ++j) {
				particle_data.particles[i * 4 + j] = pos->data[i * 3 + j];
			}
			particle_data.particles[i * 4 + 3] = atom_type->data[i];
		}
	} else if (file_name.extension() == "xml") {
		ui_info.radius = 0.01;
		ParticleModel model;
		import_uintah(FileName(file_name.file_name), model);

		// TODO: Some way to pick the attributed you want.
		auto *pos = dynamic_cast<DataT<float>*>(model["p.x"].get());
		particle_data.n_particles = pos->data.size() / 3;
		particle_data.particles.resize(particle_data.n_particles * 4);
		for (size_t i = 0; i < particle_data.n_particles; ++i){
			for (size_t j = 0; j < 3; ++j) {
				particle_data.particles[i * 4 + j] = pos->data[i * 3 + j];
			}
			particle_data.particles[i * 4 + 3] = 0;
		}
		if (!uintah_attribute.empty()) {
			auto *attrib = model[uintah_attribute].get();
			if (!attrib) {
				throw std::runtime_error("No such attribute " + uintah_attribute);
			}
			for (size_t i = 0; i < particle_data.n_particles; ++i){
				particle_data.particles[i * 4 + 3] = attrib->get_float(i);
			}
		}
	}
	particle_data.box_max = glm::vec3(std::numeric_limits<float>::lowest());
	particle_data.box_min = glm::vec3(std::numeric_limits<float>::max());
	particle_data.attrib_range = glm::vec2(std::numeric_limits<float>::max(), std::numeric_limits<float>::lowest());
	for (size_t i = 0; i < particle_data.n_particles; ++i){
		for (size_t j = 0; j < 3; ++j){
			particle_data.box_max[j] = std::max(particle_data.particles[i * 4 + j], particle_data.box_max[j]);
			particle_data.box_min[j] = std::min(particle_data.particles[i * 4 + j], particle_data.box_min[j]);
		}
		particle_data.attrib_range.x = std::min(particle_data.particles[i * 4 + 3], particle_data.attrib_range.x);
		particle_data.attrib_range.y = std::max(particle_data.particles[i * 4 + 3], particle_data.attrib_range.y);
	}
	std::cout << "Attribute range: " << glm::to_string(particle_data.attrib_range) << "\n";
	grid_particles();
	return true;
}
static bool vislight_plugin_particles_init(const std::vector<std::string> &args,
		vl::PluginFunctionTable &fcns, vl::MessageDispatcher &dispatcher)
{
	std::cout << "particles plugin args = {\n";
	for (const auto &a : args){
		std::cout << "\t" << a << "\n";
	}
	std::cout << "}\n";
	for (size_t i = 0; i < args.size(); ++i) {
		if (args[i] == "-var") {
			uintah_attribute = args[++i];
		}
	}
	if (args.size() > 1){
		loader_fn(args[1]);
	}

	fcns.plugin_type = vl::PluginType::UI_ELEMENT | vl::PluginType::RENDER
		| vl::PluginType::LOADER;
	fcns.ui_fn = ui_fn;
	fcns.render_fn = render_fn;
	fcns.loader_fn = loader_fn;
	fcns.file_extensions = "ptsc,xyz";
	return true;
}
static void unload_fn(){
	// TODO: Tell the buf allocator to free our stuff
}

PLUGIN_LOAD_FN(particles)
PLUGIN_UNLOAD_FN(particles, unload_fn)

