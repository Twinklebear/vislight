#version 430 core

#include "particle_global.glsl"

uniform sampler1D palette;

//uniform int debug_cell_id;

in vec3 vray_dir;
flat in vec3 transformed_eye;

out vec4 color;

// Compute the intersection of the ray with the sphere, t_max is the current t_max
// of the ray and will be updated if the sphere is intersected before any previously
// found intersections
bool intersect_sphere(vec3 center, vec3 origin, vec3 dir, float t_min, inout float t_max){
	vec3 d = origin - center;
	float a = dot(dir, dir);
	float b = 2.0 * dot(dir, d);
	float c = dot(d, d) - radius * radius;
	float discrim = b * b - 4.0f * a * c;
	if (discrim < 0){
		return false;
	}
	discrim = sqrt(discrim);
	float t0 = (-b - discrim) / (2.0 * a);
	float t1 = (-b + discrim) / (2.0 * a);
	float t_hit = t0;
	if (t_hit < t_min){
		t_hit = t1;
	}
	if (t_hit > t_max){
		return false;
	} else {
		t_max = t_hit;
		return true;
	}
}
// Get the id of the cell containing the point passed. It's assumed the point
// is in the range [0, grid_dim - 1]
int get_cell_id(vec3 p){
	ivec3 c = ivec3(p);
	return c.x + c.y * grid_dim.x + c.z * grid_dim.x * grid_dim.y;
}
bool outside_grid(vec3 p){
	return any(lessThan(p, vec3(0))) || any(greaterThanEqual(p, vec3(grid_dim)));
}
// Get the min/max points of the cell containing the point
void get_cell_bounds(int id, out vec3 min_pt, out vec3 max_pt){
	min_pt = vec3(id % grid_dim.x, (id / grid_dim.x) % grid_dim.y,
				id / (grid_dim.x * grid_dim.y));
	max_pt = min_pt + vec3(1.0);
	min_pt /= grid_dim;
	max_pt /= grid_dim;
}
// Just raycast a specific cell in the grid, for debugging
int raycast_cell(vec3 origin, vec3 dir, int cell, float t0, inout float t1){
	int hit_id = -1;
	int start = grid_cells[cell].y;
	int end = grid_cells[cell].y + grid_cells[cell].x;
	for (int i = start; i < end; ++i){
		if (intersect_sphere(particles[i].xyz, origin, dir, t0, t1)){
			hit_id = i;
		}
	}
	return hit_id;
}
// Cast the ray through the grid and find the first intersection with a particle
// t1 will be returned with the hit t value, if any
int raycast_grid(vec3 origin, vec3 dir, vec3 inv_dir, float t0, inout float t1){
	// We must scale our ray on to the grid from the world bounds
	const vec3 grid_origin = (origin - box_min) / (box_max - box_min);
	const vec3 grid_ray = dir / (box_max - box_min);
	const vec3 grid_ray_inv_dir = 1.0 / grid_ray;
	const vec3 start_pos = grid_origin + t0 * grid_ray;
	// Grid cell position
	vec3 pos = clamp(start_pos * vec3(grid_dim), vec3(0), vec3(grid_dim - 1));
	int cell_id = get_cell_id(pos);
	vec3 cell_min, cell_max;
	get_cell_bounds(cell_id, cell_min, cell_max);
	// Setup for DDA traversal through the grid
	const bvec3 neg_dir = lessThan(dir, vec3(0.0));
	const vec3 t_max_neg = (cell_min - start_pos) * grid_ray_inv_dir;
	const vec3 t_max_pos = (cell_max - start_pos) * grid_ray_inv_dir;
	// It seems like the results of this test are flipped?
	// vs. what the docs say?
	vec3 t_max = mix(t_max_pos, t_max_neg, neg_dir);

	const ivec3 ray_step = ivec3(sign(dir));
	const vec3 t_delta = abs(grid_ray_inv_dir / grid_dim);

	int hit_id = -1;
	int steps = 0;
	while (!outside_grid(pos) && hit_id == -1 && steps < 100){
		++steps;
		// Test particles in the cell for intersection
		cell_id = get_cell_id(pos);
		get_cell_bounds(cell_id, cell_min, cell_max);
		int start = grid_cells[cell_id].y;
		int end = grid_cells[cell_id].y + grid_cells[cell_id].x;
		for (int i = start; i < end; ++i){
			float t_tmp = t1;
			// TODO it makes no sense that t_next is not valid to be our max t here!?
			if (intersect_sphere(particles[i].xyz, origin, dir, t0, t_tmp)){
				vec3 p = grid_origin + t_tmp * grid_ray;
				if (!any(lessThan(p, cell_min)) || !any(greaterThan(p, cell_max))){
					t1 = t_tmp;
					hit_id = i;
				}
			}
		}
		float t_next = min(t_max.x, min(t_max.y, t_max.z));
		if (t_next == t_max.x){
			pos.x += ray_step.x;
			t_max.x += t_delta.x;
		} else if (t_next == t_max.y){
			pos.y += ray_step.y;
			t_max.y += t_delta.y;
		} else {
			pos.z += ray_step.z;
			t_max.z += t_delta.z;
		}
	}
	return hit_id;
}

void main(void){
	vec3 ray_dir = normalize(vray_dir);
	const vec3 light_dir = -ray_dir;
	vec3 inv_dir = 1.0 / ray_dir;
	// Check for intersection against the bounding box of the brick
	vec3 tmin_tmp = (box_min - transformed_eye) * inv_dir;
	vec3 tmax_tmp = (box_max - transformed_eye) * inv_dir;
	vec3 tmin = min(tmin_tmp, tmax_tmp);
	vec3 tmax = max(tmin_tmp, tmax_tmp);
	// This is where we enter and exit the box containing the entire domain
	float tenter = max(0, max(tmin.x, max(tmin.y, tmin.z)));
	float texit = min(tmax.x, min(tmax.y, tmax.z));
	if (tenter > texit){
		discard;
	}

	float t_hit = texit;
	int hit_id = raycast_grid(transformed_eye, ray_dir, inv_dir, tenter, t_hit);

	if (hit_id >= 0){
		vec3 p = transformed_eye + t_hit * ray_dir;
		vec3 n = p - particles[hit_id].xyz;
		n = normalize(n);
		const vec3 atom_color = texture(palette, particles[hit_id].w / attrib_range).rgb;
		vec3 half_vec = normalize(light_dir - ray_dir);
		color.xyz = atom_color * 0.25 + atom_color * dot(light_dir, n)
				+ vec3(0.2) * pow(dot(half_vec, n), 30.f);
		color.xyz *= dot(light_dir, n);
		color.a = 1;
	} else {
		discard;
	}
}

