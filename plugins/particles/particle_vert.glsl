#version 430 core

#include "particle_global.glsl"

layout(location = 0) in vec3 pos;

out vec3 vray_dir;
flat out vec3 transformed_eye;

void main(void){
	gl_Position = proj * view * model_mat * vec4(pos, 1);
	// Both transformed eye and vray dir are in the [-1, 1] cube space
	transformed_eye = (model_mat_inv * vec4(eye_pos, 1)).xyz;
	// Transform into the data space
	transformed_eye = (box_max - box_min) * (transformed_eye + 1.f) / 2.f + box_min;
	vec3 transformed_pos = (box_max - box_min) * (pos + 1.f) / 2.f + box_min;
	vray_dir = transformed_pos - transformed_eye;
}

