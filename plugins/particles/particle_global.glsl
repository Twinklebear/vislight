#include <view_info.glsl>

layout(std140, binding = 2) uniform ParticleInfo {
	mat4 model_mat;
	mat4 model_mat_inv;
	int n_particles;
	float radius;
	float attrib_range;
	// Note that the vec3's are padded and aligned like vec4's so
	// there's an extra byte of padding here before box_min
	// Particle world bounds
	vec3 box_min;
	vec3 box_max;
};

layout(std140, binding = 3) buffer ParticleData {
	// A particle is x,y,z,attribute
	vec4 particles[];
};

layout(std430, binding = 4) buffer ParticleGrid {
	ivec3 grid_dim;
	// Each grid cell is the number of particles with and the offset
	// to the first particle in the particles array
	ivec2 grid_cells[];
};

