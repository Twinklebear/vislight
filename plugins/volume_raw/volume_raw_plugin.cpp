#include <iostream>
#include <algorithm>

#include <imgui.h>
#include <SDL.h>

#include <vl/plugin.h>
#include <vl/volume.h>

#include "../transfer_function/histogram_message.h"

struct VolumeInfo {
	vl::FileName file = vl::FileName("");
	int ui_dims[3] = { -1, -1, -1 };
	int ui_dtype_selection = 0;
	std::array<int, 3> dims;
	GLenum internal_format, format;
	std::shared_ptr<std::vector<char>> data;
};

static vl::MessageDispatcher *msg_dispatch = nullptr;
static std::unique_ptr<vl::Volume> volume = nullptr;
static VolumeInfo vol_info;
// Rotation velocity along x and y axes
static glm::vec2 rotation_velocity = glm::vec2(0);

// T is the type of data stored in the volume file
// TODO: I guess this would kind of make sense to have in vislights utilities?
template<typename T>
static std::shared_ptr<std::vector<char>> import_raw(const vl::FileName &file_name, const std::array<int, 3> &dims){
	std::ifstream fin{file_name.c_str(), std::ios::binary};
	auto data = std::make_shared<std::vector<char>>(dims[0] * dims[1] * dims[2] * sizeof(T), 0);
	fin.read(reinterpret_cast<char*>(data->data()), sizeof(T) * dims[0] * dims[1] * dims[2]);
	return data;
}

static void ui_fn(){
	if (vol_info.file.empty()){
		return;
	}
	static const char *ui_dtype_options[] = { "...", "uchar", "ushort", "half", "float", "double" };
	if (ImGui::Begin("Raw Volume Info")){
		ImGui::Text("File: %s", vol_info.file.c_str());
		// If no volume is loaded show a panel where the user can specify the dimensions
		if (vol_info.dims[0] == -1){
			ImGui::Text("Enter volume dimensions and data type");
		}
		// Handling the input here is a bit awkward..
		ImGui::InputInt3("Dimensions", vol_info.ui_dims, ImGuiInputTextFlags_CharsDecimal
				| ImGuiInputTextFlags_CharsNoBlank);
		ImGui::Combo("Data type", &vol_info.ui_dtype_selection, ui_dtype_options, 6);
		// Check if the user has given us all the information we need to load the raw
		// volume correctlIF the usery
		if (ImGui::Button("Load")){
			bool dims_set = true;
			for (size_t i = 0; i < 3; ++i){
				if (vol_info.ui_dims[i] < 1){
					dims_set = false;
				}
			}
			if (dims_set && vol_info.ui_dtype_selection > 0){
				std::cout << "loading raw file\n";
				for (size_t i = 0; i < 3; ++i){
					vol_info.dims[i] = vol_info.ui_dims[i];
				}
				if (vol_info.ui_dtype_selection == 1){
					vol_info.internal_format = GL_R8;
					vol_info.format = GL_FLOAT;
					auto input = import_raw<uint8_t>(vol_info.file, vol_info.dims);
					// TODO: find min/max?
					vol_info.data = std::make_shared<std::vector<char>>(input->size() * 4, 0);
					float* data_ptr = reinterpret_cast<float*>(vol_info.data->data());
					std::transform(input->begin(), input->end(), data_ptr,
							[](const uint8_t &u){ return static_cast<float>(u) / 255.f; });
				} else if (vol_info.ui_dtype_selection == 2){
					vol_info.internal_format = GL_R16;
					vol_info.format = GL_UNSIGNED_SHORT;
					vol_info.data = import_raw<uint16_t>(vol_info.file, vol_info.dims);
				} else if (vol_info.ui_dtype_selection == 3){
					vol_info.internal_format = GL_R16F;
					vol_info.format = GL_HALF_FLOAT;
					vol_info.data = import_raw<uint16_t>(vol_info.file, vol_info.dims);
					// TODO: find min/max?
				} else if (vol_info.ui_dtype_selection == 4){
					vol_info.internal_format = GL_R32F;
					vol_info.format = GL_FLOAT;
					vol_info.data = import_raw<float>(vol_info.file, vol_info.dims);
					// TODO: find min/max?
				} else if (vol_info.ui_dtype_selection == 5){
					vol_info.internal_format = GL_R32F;
					vol_info.format = GL_FLOAT;
					vol_info.data = import_raw<double>(vol_info.file, vol_info.dims);
					// TODO: find min/max?
					double *ptr = reinterpret_cast<double*>(vol_info.data->data());
					float* data_ptr = reinterpret_cast<float*>(vol_info.data->data());
					std::transform(ptr, ptr + vol_info.data->size() / 8, data_ptr,
							[](const double &d){ return static_cast<float>(d); });
					// Remove the extra doubles we've converted down to floats
					vol_info.data->resize(vol_info.data->size() / 2);
				} else {
					throw std::runtime_error("Invalid Selection!?");
				}
			}
		}
	}
	ImGui::End();
}
static void event_fn(const SDL_Event &e){
	if (volume && e.type == SDL_CONTROLLERAXISMOTION){
		float axis_amt = static_cast<float>(e.caxis.value) / std::numeric_limits<int16_t>::max();
		if (std::abs(axis_amt) < 0.1f){
			axis_amt = 0.f;
		}
		if (e.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTX){
			rotation_velocity.y = axis_amt;
		}
		else if (e.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTY){
			rotation_velocity.x = axis_amt;
		}
	}
}
static void render_fn(std::shared_ptr<glt::BufferAllocator> &allocator, const glm::mat4 &view,
	   const glm::mat4 &proj, const float elapsed)
{
	if (vol_info.data){
		// TODO: I guess this ctor should take the data, instead of calling set_data
		// after?
		if (!volume){
			volume = std::make_unique<vl::Volume>(vol_info.format,
					vol_info.internal_format, vol_info.data, vol_info.dims);
			volume->translate(glm::vec3(0, 0.75, -1.25));
			volume->scale(glm::vec3(0.25));
		} else {
			volume->set_volume(vol_info.format, vol_info.internal_format, vol_info.data, vol_info.dims);
		}
		if (volume && !volume->histogram.empty()){
			msg_dispatch->send(std::make_shared<tfcn::HistogramMessage>("volume_raw",
						"transfer_function", volume->histogram));
		}
		vol_info.data = nullptr;
	}
	if (volume){
		volume->rotate(glm::quat_cast(glm::rotate(rotation_velocity.x * elapsed, glm::vec3(1, 0, 0))));
		volume->rotate(glm::quat_cast(glm::rotate(rotation_velocity.y * elapsed, glm::vec3(0, 1, 0))));
		volume->render(allocator, view);
	}
}
static bool loader_fn(const vl::FileName &file_name){
	std::cout << "volume_raw plugin loading file: " << file_name << "\n";
	vol_info.file = file_name;
	return true;
}
static bool vislight_plugin_volume_raw_init(const std::vector<std::string> &args,
		vl::PluginFunctionTable &fcns, vl::MessageDispatcher &dispatcher)
{
	std::cout << "volume raw plugin args = {\n";
	for (const auto &a : args){
		std::cout << "\t" << a << "\n";
	}
	std::cout << "}\n";
	msg_dispatch = &dispatcher;

	fcns.plugin_type = vl::PluginType::UI_ELEMENT | vl::PluginType::RENDER
		| vl::PluginType::LOADER;
	fcns.ui_fn = ui_fn;
	fcns.render_fn = render_fn;
	fcns.loader_fn = loader_fn;
	fcns.event_fn = event_fn;
	fcns.file_extensions = "raw";
	if (args.size() > 1){
		vol_info.file = args[1];
	}
	return true;
}
static void unload_fn(){
	msg_dispatch = nullptr;
	volume = nullptr;
}

PLUGIN_LOAD_FN(volume_raw)
PLUGIN_UNLOAD_FN(volume_raw, unload_fn)

