#include <iostream>
#include <vector>
#include <cassert>

#include <imgui.h>

#include <vl/glt/gl_core_4_5.h>
#include <vl/glt/buffer_allocator.h>
#include <vl/plugin.h>
#include <vl/volume.h>
#include <vl/file_name.h>

#include "transfer_function.h"
#include "histogram_message.h"

using namespace tfcn;

static vl::MessageDispatcher *msg_dispatch = nullptr;
static std::unique_ptr<TransferFunction> transfer_fcn;

static void ui_fn(){
	transfer_fcn->draw_ui();
}
static void render_fn(std::shared_ptr<glt::BufferAllocator> &allocator, const glm::mat4 &view,
	   const glm::mat4 &proj, const float elapsed)
{
	transfer_fcn->render();
}
static void msg_fn(const std::shared_ptr<vl::PluginMessage> &msg){
	if (msg->name() == "HistogramMessage"){
		const HistogramMessage *hist = dynamic_cast<const HistogramMessage*>(msg.get());
		assert(hist);
		transfer_fcn->histogram = hist->histogram;
	}
}
static bool loader_fn(const vl::FileName &file_name){
	return transfer_fcn->load_fcn(file_name);
}
static void unload_fn(){}
static bool vislight_plugin_transfer_function_init(const std::vector<std::string> &args,
		vl::PluginFunctionTable &fcns, vl::MessageDispatcher &dispatcher)
{
	std::cout << "transfer function plugin args = {\n";
	for (const auto &a : args){
		std::cout << "\t" << a << "\n";
	}
	std::cout << "}\n";
	msg_dispatch = &dispatcher;

	fcns.plugin_type = vl::PluginType::UI_ELEMENT | vl::PluginType::RENDER
		| vl::PluginType::LOADER;
	fcns.ui_fn = ui_fn;
	fcns.render_fn = render_fn;
	fcns.message_fn = msg_fn;
	fcns.loader_fn = loader_fn;
	fcns.file_extensions = "vlfn";
	transfer_fcn = std::make_unique<TransferFunction>();
	if (args.size() > 1){
		transfer_fcn->load_fcn(vl::FileName(args[1]));
	}
	return true;
}

// Setup externally callable loaders so the plugin loader can find our stuff
PLUGIN_LOAD_FN(transfer_function)
PLUGIN_UNLOAD_FN(transfer_function, unload_fn)

