#ifndef TRANSFER_FUNCTION_HISTOGRAM_MESSAGE_H
#define TRANSFER_FUNCTION_HISTOGRAM_MESSAGE_H

#include <string>

#include <vl/plugin_message.h>

namespace tfcn {

struct HistogramMessage : vl::PluginMessage {
	std::string from, to, type;
	std::vector<size_t> histogram;

	HistogramMessage(const std::string &from, const std::string &to,
			const std::vector<size_t> &histo)
		: from(from), to(to), type("HistogramMessage"), histogram(histo)
	{}
	const std::string& from_plugin() const override {
		return from;
	}
	const std::string& to_plugin() const override {
		return to;
	}
	const std::string& name() const override {
		return type;
	}
};

}

#endif

